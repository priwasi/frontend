const express = require("express");
const cors = require("cors");
const app = express();
const port = 3000;
const bodyParser = require("body-parser");

// Import Mock Data
const viewRequests = require("./viewRequests.json");
const readRequests = require("./readRequest.json");
const deleteRequests = require("./deleteRequest.json");
const allpermissions = require("./allpermissions.json");
const roleList = require("./rolePermission.json");
const graph = require("./graph.json");
const silosCount = require("./silos.json");
const expiredRequest = require("./expiredRequest.json");
const assignee = require("./assignee.json");

app.use(cors());
app.use(bodyParser());

app.get("/request/all", (req, res) => {
  return res.json(viewRequests);
});

app.get("/request/access", (req, res) => {
  return res.json(readRequests);
});
app.get("/request/delete", (req, res) => {
  return res.json(deleteRequests);
});

app.post("/request", (req, res) => {
  let result = {
    requestId: Math.floor(Math.random() * 1000000000)
  };

  return res.json(result);
});
app.get("/sec/client/allpermission", (req, res) => {
  return res.json(allpermissions);
});
app.get("/sec/client/roleandpermission", (req, res) => {
  return res.json(roleList);
});

app.post("/dashboard/graph", (req, res) => {
  return res.json(graph);
});

app.get("/dashboard/silos", (req, res) => {
  return res.json(silosCount);
});
app.post("/dashboard/requestdata", (req, res) => {
  return res.json(expiredRequest);
});

app.get("/admin/assignees", (req, res) => {
  return res.json(assignee);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
