import React, { Component } from 'react';
import s from './Header.module.scss';
import { hardwareBackPressed } from '../../../utils/webviewUtils';
import crossicon from '../../../public/svgfonts/ic-clear.svg';
/* eslint-disable */
export default class Header extends Component {
  handleBackClick = () => {
    if (this.props.headerBackClick) {
      this.props.headerBackClick();
    } else {
      hardwareBackPressed(this.props.history);
    }
  };
  
  moveToProfile = () => {
    // this.props.history.push('/profile');
  };

  render() {
    let headerClassName = s.header;
    if (this.props.white) {
      headerClassName = `${s.white} ${headerClassName}`;
    }
    if (this.props.noBorder) {
      headerClassName = `${s.noBorder} ${headerClassName}`;
    }
    return (
      <div
        style={{ position: this.props.position }}
        className={`${s.headerWrap} ${
          this.props.isPremium ? s.premium : s.normal
        }`}
      >
        <header className={headerClassName}>
          {/* <div classsName={s.action}>
            <i
              className="gvFontIcons icon-back"
              onClick={this.handleBackClick}
            ></i>
          </div> */}
          <div className={s.action}>
            {!this.props.crossIcon ? (
              <i
                className="gvFontIcons icon-back"
                onClick={this.handleBackClick}
              ></i>
            ) : (
              <img
                src={crossicon}
                style={this.props.styleObj}
                onClick={this.handleBackClick}
              ></img>
            )}
          </div>
          <h1>
            {this.props.centerHeader ? (
              <center>{this.props.headerTitle}</center>
            ) : (
              this.props.headerTitle
            )}
          </h1>
          {this.props.headerRightLink && (
            <div className={s.link}>
              <span onClick={this.props.handleLinkAction}>
                {this.props.headerRightLink}
              </span>
            </div>
          )}
          {this.props.headerRightDots && (
            <div className={s.action}>
              <i className="more"></i>
            </div>
          )}
          {this.props.headerRightLogo && (
            <div
              className={`${s.action} ${s.noPadding} `}
              onClick={this.props.handleDotsAction}
            >
              <img src={this.props.headerRightLogo} alt="" />
              {this.props.isPremium && (
                <span className={s.premiumTag}>PREMIUM</span>
              )}
            </div>
          )}
          {this.props.headerRightText && (
            <div
              className={`${s.text} ${this.props.fontWhite ? s.whiteText : ''}`}
              onClick={
                this.props.headerRightAction && this.props.headerRightAction
              }
            >
              <span>{this.props.headerRightText}</span>
            </div>
          )}
        </header>
        {this.props.headerChildren}
      </div>
    );
  }
}

Header.defaultProps = {
  headerTitle: null,
  headerRightLogo: null,
  headerRightDots: null,
  headerRightLink: null,
  headerBackClick: null,
  headerRightText: null,
  white: false,
  centerHeader: false,
  crossIcon: false,
  headerRightAction: () => {}
};
