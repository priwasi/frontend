import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PlatformLayoutComponent from "./PlatformLayoutComponent";

export default withRouter(connect()(PlatformLayoutComponent));
