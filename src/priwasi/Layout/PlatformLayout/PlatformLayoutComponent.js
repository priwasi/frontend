import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { bindActionCreators } from "redux";

// import Loadable from "react-loadable";
import Header from "../PlatformLayout/Header/HeaderContainer";
import SideNav from "./SideNav/SideNav";
// import Footer from '../components/Footer/FooterContainer';

// Styles Import
import s from "./PlatformLayout.module.scss";

// Actions Import
import {
  getUserInfo,
  getAllPermissions,
  savePermMap,
  getAssignee
} from "../../store/actions";
import DotLoader from "../../shared/DotLoader";

// Node Modules Import
import ReactTooltip from "react-tooltip";

class PlatformLayoutComponent extends Component {
  componentDidMount = () => {
    // User Info API for enabling Permission
    const { getUserInfo, userInfo, getAssignee } = this.props;
    if (!userInfo.data) {
      getUserInfo("admin");
    }
  };

  componentDidUpdate = (prevProps, prevState) => {
    const { userInfo, getAssignee, assignees } = this.props;
    if (
      userInfo &&
      !userInfo.loading &&
      userInfo.data &&
      !assignees.loading &&
      !assignees.data &&
      !assignees.error
    ) {
      let clientId =
        userInfo &&
        userInfo.data &&
        userInfo.data.data &&
        userInfo.data.data.userId &&
        userInfo.data.data.userId;
      console.log(clientId);
      getAssignee(clientId);
    }
  };

  sectionClass = () => {
    if (this.props.position === "fixed") {
      if (this.props.showTab) {
        return "midleConTab";
      }
      return "midleCon";
    } else {
      return "";
    }
  };

  render() {
    /*
      TO DO Optimize
    */
    // When All Permissions are loading
    if (this.props.allPermissions.loading) {
      return <DotLoader />;
    }
    return (
      <main className={`${s.sectionClass}`}>
        <ReactTooltip />
        <section>
          <Header {...this.props} />
          <div className={s.content}>
            <SideNav {...this.props} />
            {this.props.children}
          </div>
          {/* {footer && <Footer />} */}
        </section>
      </main>
    );
  }
}

const mapStateToProps = (state, prevProps) => {
  return {
    userInfo: state.app.userInfo,
    allPermissions: state.app.allPermissions,
    assignees: state.app.assignees
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getUserInfo,
      getAllPermissions,
      getAssignee
    },
    dispatch
  );
};

PlatformLayoutComponent.propTypes = {
  header: PropTypes.bool
  // footer: PropTypes.bool
};
PlatformLayoutComponent.defaultProps = {
  header: true,
  footer: false,
  position: "fixed",
  isPremium: false
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(PlatformLayoutComponent)
);
