import React, { Component } from "react";
import s from "./SideNav.module.scss";

import logo from "../../../public/priwasi/logo.svg";

// Menu Icons
import controlPanel from "../../../public/priwasi/menu/control-panel.svg";
import dashboard from "../../../public/priwasi/menu/dashboard.svg";
import requestManager from "../../../public/priwasi/menu/request-manager.svg";
import settings from "../../../public/priwasi/menu/settings.svg";
import privacyPortal from "../../../public/priwasi/menu/privacy-portal.svg";
import silos from "../../../public/priwasi/menu/silos.svg";

////---- Active ----///

import controlPanelActive from "../../../public/priwasi/menu/control-panel-active.svg";
import dashboardActive from "../../../public/priwasi/menu/dashboard-active.svg";
import requestManagerActive from "../../../public/priwasi/menu/request-manager-active.svg";
import settingsActive from "../../../public/priwasi/menu/settings-active.svg";
import privacyPortalActive from "../../../public/priwasi/menu/privacy-portal-active.svg";
import silosActive from "../../../public/priwasi/menu/silos-active.svg";

/* eslint-disable */

const menus = [
  {
    title: "Dashboard",
    config_key: "dashboard",
    icon: dashboard,
    activeIcon: dashboardActive,
    link: "/platform/"
  },
  {
    title: "Data Source",
    config_key: "data_source",
    icon: silos,
    activeIcon: silosActive,
    link: "/platform/silos"
  },
  {
    title: "Request Manager",
    config_key: "request_manager",
    icon: requestManager,
    activeIcon: requestManagerActive,
    link: "/platform/request"
  },
  {
    title: "Control Panel",
    config_key: "control_panel",
    icon: controlPanel,
    activeIcon: controlPanelActive,
    link: "/settings/controlpanel"
  },
  {
    title: "Settings",
    config_key: "settings",
    icon: settings,
    activeIcon: settingsActive,
    link: "/settings/users"
  },
  {
    title: "Privacy Portal",
    config_key: "privacy_portal",
    icon: privacyPortal,
    activeIcon: privacyPortalActive,
    link: "/settings/policy"
  }
];

export default class SideNav extends Component {
  state = {
    activeMenu: false
  };

  navigateToMenu = item => {
    const { history } = this.props;
    if (item.link) {
      history.push(item.link);
    }
  };

  toggleActive = config_key => {
    this.setState({ activeMenu: config_key });
  };

  removeActive = () => {
    this.setState({ activeMenu: false });
  };

  render() {
    const { activeMenu } = this.state;
    const { location } = this.props;
    return (
      <div className={s.sideNavParent}>
        <div className={s.title}>
          <img src={logo} alt={"logo"} />
        </div>
        <div className={s.menuHolder}>
          <ul>
            {menus.map((item, index) => {
              return (
                <span
                  className={
                    activeMenu === item.config_key ||
                    (!activeMenu && location.pathname === item.link)
                      ? `${s.listItemParent} ${s.active}`
                      : `${s.listItemParent}`
                  }
                  key={`${index}_listParent`}
                  onClick={() => this.navigateToMenu(item)}
                  onMouseOver={() => this.toggleActive(item.config_key)}
                  onMouseOut={() => this.removeActive(item.config_key)}
                >
                  <div className={s.iconPlaceholder}>
                    <img
                      src={
                        activeMenu === item.config_key ||
                        (!activeMenu && location.pathname === item.link)
                          ? item.activeIcon
                          : item.icon
                      }
                    />
                  </div>
                  <li key={`${index}_key`}>{item.title}</li>
                </span>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }
}
