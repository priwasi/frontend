import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import SideNav from "./SideNav";

export default withRouter(connect()(SideNav));
