import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import SiteLayoutComponent from "./SiteLayoutComponent";

export default withRouter(connect()(SiteLayoutComponent));
