/*
  @Module : CredBox
  @Component : Policy Info
  @Type : Screen
  @Description : Policy Info
  @Author : Kaustubh Saxena 
*/

// React Imports
import React, { useState, Component } from "react";

// Component Imports
import SiteLayout from "../../Layout/SiteLayout/SiteLayout";
import Button from "../../shared/Button/Button";
import Input from "../../shared/Input/Input";

// Stylesheet Imports
import s from "./ForgotPassword.module.scss";

// Asset & Utility Imports
import logo from "../../public/priwasi/logo.svg";

// Node Modules Import
//

// import { MdEdit } from "react-icons/md";

class ForgotPassword extends Component {
  state = {
    formData: {},
    accessDenied: false
  };

  componentDidMount = () => {
    //
  };

  componentDidUpdate = (prevProps, prevState) => {
    const { forgotPasswordData, history } = this.props;
    if (
      prevProps.forgotPasswordData.loading &&
      !forgotPasswordData.loading &&
      forgotPasswordData.data
    ) {
      history.push("/admin/login");
    }
  };

  handleChange = e => {
    let formData = { ...this.state.formData };
    formData[e.target.name] = e.target.value;
    this.setState({ formData: formData });
  };

  initiateForgotPassword = () => {
    const { formData } = this.state;
    const { forgotPassword } = this.props;
    if (formData.email) {
      forgotPassword({ emailId: formData.email });
    }
  };

  render() {
    const { forgotPasswordData } = this.props;
    return (
      <SiteLayout position="relative">
        <div className={s.loginContainer}>
          <div>
            <img src={logo} />
          </div>
          <div className={s.innerParent}>
            <div>
              {" "}
              <span className={s.bigText}>Recover Your Account</span>{" "}
            </div>
            <div>
              <Input
                className={s.loginInput}
                placeholder={"Email"}
                name={"email"}
                onChangeHandler={this.handleChange}
                type={"text"}
              />
            </div>
            <div>
              <Button
                btnText={forgotPasswordData.loading ? "Loading" : "Confirm"}
                onClickHandler={
                  !forgotPasswordData.loading
                    ? this.initiateForgotPassword
                    : () => {}
                }
              />
            </div>
          </div>
        </div>
      </SiteLayout>
    );
  }
}

export default ForgotPassword;
