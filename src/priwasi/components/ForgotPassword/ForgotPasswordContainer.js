import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

// Actions
import { forgotPassword } from "./actions";

import ForgotPassword from "./ForgotPassword";

import { bindActionCreators } from "redux";

const mapStateToProps = (state, prevProps) => {
  console.log(state);
  return {
    forgotPasswordData: state.forgotPasswordReducer.forgotPasswordData
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      forgotPassword
    },
    dispatch
  );
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ForgotPassword)
);
