/*
  @Module : CredBox
  @Component : Activate
  @Type : Action Creators
  @Description : Activate User
  @Author : Kaustubh Saxena
*/

/*
  Action Type Constants
*/
import {
  FORGOT_PASSWORD_LOADING,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_FAILURE,
  FORGOT_PASSWORD_CLEAR
} from "../../constants/actionTypes";

/*
  Fetch Wrapper used to hit API.
*/

import httpFetch from "../../utils/http";

/*
  Utility Imports
*/
import { getHeaders, getUrl } from "../../utils/utilities";

/*
    @Purpose : Forgot Password
    @System : CBB Backend
*/

export function forgotPassword(data) {
  let apiUrl = getUrl("forgotPassword");
  return {
    types: [
      FORGOT_PASSWORD_LOADING,
      FORGOT_PASSWORD_SUCCESS,
      FORGOT_PASSWORD_FAILURE
    ],
    promise: () =>
      httpFetch(apiUrl, {
        headers: getHeaders(),
        method: "POST",
        body: JSON.stringify(data)
      })
  };
}
