// Action Type Constants
import {
  FORGOT_PASSWORD_LOADING,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_FAILURE,
  FORGOT_PASSWORD_CLEAR
} from "../../constants/actionTypes";

import {
  getLoadingState,
  getSuccessState,
  getErrorState
} from "../../store/stateSettler";

const initialState = {
  forgotPasswordData: {
    loading: false,
    data: null,
    error: null
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FORGOT_PASSWORD_LOADING:
      return {
        ...state,
        forgotPasswordData: getLoadingState("forgotPasswordData", state)
      };
    case FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        forgotPasswordData: getSuccessState("forgotPasswordData", state, action)
      };
    case FORGOT_PASSWORD_FAILURE:
      return {
        ...state,
        forgotPasswordData: getErrorState("forgotPasswordData", state, action)
      };
    case FORGOT_PASSWORD_CLEAR:
      return {
        ...state,
        forgotPasswordData: initialState.forgotPasswordData
      };
    default:
      return state;
  }
}
