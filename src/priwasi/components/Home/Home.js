/*
  @Module : PaytmKhata
  @Component : Home
  @Type : Screen
  @Description : Entry Screen
  @Author : Kaustubh Saxena 
*/

// React Imports
import React, { useState } from 'react';

// Component Imports
import DefaultLayout from '../../Layout/DefaultLayout';
import Button from '../../shared/Button/Button';
import Footer from '../../shared/Footer/Footer';
import FloatingFormField from '../../shared/FloatingFormField';
// Stylesheet Imports
import s from './Home.module.scss';
import Modal from '../../shared/Modal/Modal';

const Home = props => {
  const [state] = useState({
    merchantName: 'Verma General Store',
    customerName: 'Ashish Gupta',
    dueDate: '2nd October 2019',
    lastDate: '2nd October 2019',
    netAmount: 3432,
    customerMobileNo: '9898989898',
    records: [
      {
        date: '20.10.19',
        description: 'Description goes here',
        payment: 3223,
        loan: 23232
      },
      {
        date: '20.10.19',
        description: 'Description goes here',
        payment: 3223,
        loan: 23232
      },
      {
        date: '20.10.19',
        description: 'Description goes here',
        payment: 3223,
        loan: 23232
      },
      {
        date: '20.10.19',
        description: 'Description goes here',
        payment: 3223,
        loan: 23232
      },
      {
        date: '20.10.19',
        description: 'Description goes here',
        payment: 3223,
        loan: 23232
      },
      {
        date: '20.10.19',
        description: 'Description goes here',
        payment: 3223,
        loan: 23232
      },
      {
        date: '20.10.19',
        description: 'Description goes here',
        payment: 3223,
        loan: 23232
      }
    ]
  });
  const { uuid } = props.match.params;
  const [modal, showModal] = useState(false);

  return (
    <DefaultLayout white position="relative">
      <div className={s.body}>
        <div className={s.container}>
          <div className={s.storeName}>{state.merchantName}</div>
          {/* <div className={s.storeAddress}>D10, Sector 125, Noida - 201201</div> */}
          <div className={s.ruler}></div>
          <div className={s.header}>Last updated on {state.lastDate}</div>
          <div className={s.flex}>
            <div>
              <div className={s.label}>Due Date</div>
              <div>{state.dueDate}</div>
            </div>
            <div>
              <div className={s.label}>Amount to be paid</div>
              <div className={s.amount}>&#x20b9;{state.netAmount}</div>
            </div>
          </div>
          <div className={s.ruler}></div>
          <div className={s.header}>Customer Details</div>
          <div className={s.flex}>
            <div>
              <div className={s.label}>Name</div>
              <div>{state.customerName}</div>
            </div>
            <div>
              <div className={s.label}>Mobile</div>
              <div>{state.customerMobileNo}</div>
            </div>
          </div>
          <div className={s.ruler}></div>
          <div className={s.header}>Transaction History</div>
          <div className={`${s.transactionRow} ${s.label}`}>
            <div className={s.date}>Date</div>
            <div className={s.description}>Description</div>
            <div className={s.paid}>Paid</div>
            <div className={s.due}>Due</div>
          </div>
          <div className={s.ruler} />
          {state.records.map((record, index) => (
            <div key={index}>
              <div
                onClick={() => {
                  props.history.push('/paytmkhata/transact');
                }}
                className={s.transactionRow}
              >
                <div className={s.date}>{record.date}</div>
                <div className={s.description}>{record.description}</div>
                <div className={s.paid}>&#x20b9;{record.payment}</div>
                <div className={s.due}>&#x20b9;{record.loan}</div>
              </div>
              <div className={s.ruler} />
            </div>
          ))}
          <div className={s.flex}>
            <div>Amount to be paid</div>
            <div className={s.amount}>&#x20b9;{state.netAmount}</div>
          </div>
        </div>
        <div className={s.footer}>
          <div className={s.footerItem}>Contact Merchant</div>
          <div className={s.footerItem}>Report Spam</div>
          <div className={s.payButton}>
            <Button
              onClickHandler={() => {
                showModal(true);
              }}
              btnText="Process to Pay"
            />
          </div>
        </div>
        {modal && (
          <Modal>
            <div className={s.modalBody}>
              <span className={s.modalTitle}>Pay Bill</span>
              <div className={s.modalMain}>
                <div className={s.modalOption}></div>
                <div className={s.modalOption}>Pay Custom Amount</div>
                <FloatingFormField
                  type="number"
                  handleChange={() => {}}
                  id="customAmount"
                  title="Enter Amount"
                />
                <Button
                  onClickHandler={() => props.initiateTransaction(uuid)}
                  btnText="Process to Pay"
                />
              </div>
            </div>
          </Modal>
        )}
      </div>
    </DefaultLayout>
  );
};

export default Home;
