import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { initiateTransaction } from './actions';

import Home from './Home';

import { bindActionCreators } from 'redux';

const mapStateToProps = (state, prevProps) => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      initiateTransaction
    },
    dispatch
  );
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Home)
);
