import httpFetch from '../../utils/http';

export function initiateTransaction(uuid) {
  return dispatch => {
    //set correct url
    //httpFetch("http://demo2186828.mockable.io/", {
    httpFetch('http://172.16.1.11:8080/payment/api/v1/initiate/transaction', {
      //httpFetch("http://172.16.1.11:8080/payment/api/v1/initiate/transaction", {
      method: 'POST',
      body: {
        amount: Math.floor(Math.random() * 100),
        uuid,
        callbackUrl: 'http://172.16.1.11:8080/payment/api/v1/order/result'
      }
    }).then(response => {
      const { mid, orderId, txnToken } = response.result;
      const path = `https://securegw-stage.paytm.in/theia/api/v1/showPaymentPage?mid=${mid}&orderId=${orderId}`;
      var form = document.createElement('form');

      form.method = 'POST';
      form.action = path;

      var element1 = document.createElement('input');
      element1.value = mid;
      element1.name = 'mid';
      element1.type = 'hidden';
      form.appendChild(element1);

      var element2 = document.createElement('input');
      element2.value = orderId;
      element2.name = 'orderId';
      element2.type = 'hidden';
      form.appendChild(element2);

      var element3 = document.createElement('input');
      element3.value = txnToken;
      element3.name = 'txnToken';
      element3.type = 'hidden';
      form.appendChild(element3);

      document.body.appendChild(form);
      form.submit();
    });
  };
}
