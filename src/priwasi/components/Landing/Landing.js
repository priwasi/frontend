/*
  @Module : Priwasi
  @Component : Landing
  @Type : Screen
  @Description : Website
  @Author : Kaustubh Saxena
*/

// React Imports
import React from "react";

// Component Imports
import DefaultLayout from "../../Layout/DefaultLayout";

// Stylesheet Imports
import s from "./Landing.module.scss";

// Asset & Utility Imports
import panelImg from "../../public/images/pic.svg";
import dsr from "../../public/priwasi/dsr.svg";
import automate from "../../public/priwasi/automate.svg";
import shield from "../../public/priwasi/shield.svg";
import identity from "../../public/priwasi/identity.png";
import logo from "../../public/priwasi/logo.svg";
import boxes from "../../public/priwasi/boxes.svg";
import Button from "../../shared/Button/Button";
import panel from "../../public/priwasi/panel.png";
import Modal from "../../shared/Modal/Modal";
import {
  SoloValidator,
  addValidationRules,
  checkForValidationError
} from "../../utils/validator";
import { formValidation } from "../../utils/schema";
import Footer from "../../shared/Footer/Footer";

export default class Landing extends React.PureComponent {
  // State Declaration
  state = {
    formData: {},
    showScheduleDemo: false,
    validation: {},
    validationRules: {},
    showCookieModal: false
  };

  // Lifecycle Methods
  componentDidMount() {
    this.setState({
      validationRules: { ...addValidationRules(formValidation) }
    });
    if (!localStorage.getItem("cookieAccept")) {
      this.setState({ showCookieModal: true });
    }
    var Tawk_API = Tawk_API || {},
      Tawk_LoadStart = new Date();
    (function() {
      var s1 = document.createElement("script"),
        s0 = document.getElementsByTagName("script")[0];
      s1.async = true;
      s1.src = "https://embed.tawk.to/5e25a6f2daaca76c6fceec6d/default";
      s1.charset = "UTF-8";
      s1.setAttribute("crossorigin", "*");
      s0.parentNode.insertBefore(s1, s0);
    })();
  }

  componentDidUpdate = (prevProps, prevState) => {
    const { emailInvite } = this.props;
    if (
      prevProps.emailInvite.loading &&
      !emailInvite.loading &&
      emailInvite.data
    ) {
      let formData_ = { ...this.state.formData };
      delete formData_["inviteEmail"];
      this.setState({ formData: formData_ });
    }
  };

  renderBlocks = () => {
    return (
      <div className={s.blockParent}>
        <div className={s.blockHolder}>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
        <div className={s.blockHolder}>
          <div></div>
          <div></div>
          <div></div>
        </div>
        <div className={s.blockHolder}>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    );
  };

  handleChange = e => {
    const { validationRules } = this.state;
    let formData_ = { ...this.state.formData };
    let validation_ = {};

    let fieldObj = {
      name: e.target.name,
      value: e.target.value
    };
    formData_[e.target.name] = e.target.value;
    validation_ = {
      ...this.state.validation,
      ...SoloValidator(
        fieldObj.value,
        validationRules[fieldObj.name],
        fieldObj.name
      )
    };
    this.setState({ formData: formData_, validation: validation_ });
  };

  sendInvite = () => {
    const { formData } = this.state;
    const { sendInvite } = this.props;
    if (formData["inviteEmail"]) {
      let body = {
        email: formData["inviteEmail"]
      };
      sendInvite(body);
    }
  };

  showScheduleDemo = () => {
    this.setState({
      showScheduleDemo: true
    });
  };

  closeScheduleDemo = () => {
    this.setState({ showScheduleDemo: false });
  };

  scheduleDemo = () => {
    const { scheduleDemo, validation } = this.props;
    const { formData } = this.state;
    let dataObj = { ...formData };
    delete dataObj.inviteEmail;
    if (!checkForValidationError(this.state.validation, 4)) {
      scheduleDemo(dataObj);
    }
  };

  renderScheduleDemo = () => {
    const { validation } = this.state;
    const { scheduleDemoData, emailInvite } = this.props;
    if (scheduleDemoData.data && scheduleDemoData.data.code === 200) {
      return (
        <div>
          <div className={s.soonCard}>
            <span>
              Welcome to Credbox. Your demo is scheduled successfully. We'll get
              back to you shortly.
            </span>
          </div>
        </div>
      );
    }
    return (
      <div className={s.innerParent}>
        <span>Schedule Demo</span>
        <span>
          Fill out the form below and someone from our team will reach out soon
          as possible.
        </span>
        <div className={s.inputHolder}>
          <div className={s.inputParent}>
            <input
              type="text"
              placeholder={"Company/Title"}
              name={"companyName"}
              onChange={this.handleChange}
            />
            {validation.companyName && (
              <span className={s.error}>{validation.companyName}</span>
            )}
          </div>
          <div className={s.inputParent}>
            <input
              type="text"
              placeholder={"Full Name"}
              onChange={this.handleChange}
              name={"fullName"}
            />
            {validation.fullName && (
              <span className={s.error}>{validation.fullName}</span>
            )}
          </div>
          <div className={s.inputParent}>
            <input
              type="text"
              placeholder={"Email"}
              onChange={this.handleChange}
              name={"email"}
            />
            {validation.email && (
              <span className={s.error}>{validation.email}</span>
            )}
          </div>
          <div className={s.inputParent}>
            <input
              type="number"
              placeholder={"Phone Number"}
              onChange={this.handleChange}
              name={"phone"}
            />
            {validation.phone && (
              <span className={s.error}>{validation.phone}</span>
            )}
          </div>
          <div className={s.inputParent}>
            <Button
              onClickHandler={
                !scheduleDemoData.loading ? this.scheduleDemo : () => {}
              }
              btnText={scheduleDemoData.loading ? "Loading" : "SUBMIT"}
            />
          </div>
        </div>
      </div>
    );
  };

  acceptCookie = () => {
    localStorage.setItem("cookieAccept", true);
    this.setState({ showCookieModal: false });
  };

  render() {
    const { showScheduleDemo, validation, showCookieModal } = this.state;
    const { emailInvite } = this.props;
    return (
      <div>
        <div className={s.logoContainer}>
          <img src={logo} />
        </div>
        <div className={s.container}>
          <div className={s.introText}>
            <span>Privacy management starts here</span>
            <span>
              Securely receive and fulfill your user’s data rights requests.
              Automate the request fulfillment process and keep it encrypted end
              to end.
            </span>
          </div>
          <div className={s.rightDemo}>{this.renderScheduleDemo()}</div>
          {showScheduleDemo && (
            <Modal
              path={"#scheduleDemo"}
              showCloseButton
              onCloseHandler={this.closeScheduleDemo}
              customStyle={{ height: "auto" }}
            >
              <div className={s.rightDemo} style={{ display: "block" }}>
                {this.renderScheduleDemo()}
              </div>
            </Modal>
          )}
          {!showScheduleDemo && (
            <div className={s.requestDemoBtn}>
              <button type="button" onClick={this.showScheduleDemo}>
                Request Demo
              </button>
            </div>
          )}
          <div className={s.panelContainer}>
            <img src={panel} />
          </div>
        </div>
        <div className={s.introPane}>
          <span>Helping businesses deliver privacy at scale.</span>
          <span>
            Manage your user’s data requests from start to end - customize your
            privacy portal, auto-verify the requester, integrate your
            third-party tools, and lead with numbers.
          </span>
          {/* <span>
            Download Whitepaper
          </span> */}
        </div>

        <div className={s.infoBlockWrapper}>
          <div className={s.imageHolder} data-aos="fade-right">
            <img
              src={dsr}
              alt={
                "Receive your user’s  privacy data request from privacy portal"
              }
            />
          </div>
          <div className={s.contentHolder} data-aos="fade-left">
            <span>Receive data requests.</span>
            <span>
              Customize your privacy portal according to your business needs,
              and let your users submit their personal data requests from your
              privacy page itself.
            </span>
          </div>
        </div>
        <div className={s.rightInfoBlockWrapper}>
          <div className={s.contentHolder} data-aos="fade-right">
            <span>Verify requesters identity</span>
            <span>
              Leverage our user validation tech and ensure you are sharing
              information with the right person and there is no chance of fraud
              or data misuse.
            </span>
          </div>
          <div className={s.imageHolder} data-aos="fade-left">
            <img
              src={identity}
              alt={"Validate your user to ensure that the request is genuine"}
            />
          </div>
        </div>
        <div className={s.infoBlockWrapper}>
          <div className={s.imageHolder} data-aos="fade-right">
            <img
              src={automate}
              alt={
                "Identify user data across your business and complete the request"
              }
            />
          </div>
          <div className={s.contentHolder} data-aos="fade-left">
            <span>Automate fulfilment request. </span>
            <span>
              Locate your user’s personal data across your entire system with
              our connectors, and automate request fulfillment across your
              entire data ecosystem.
            </span>
          </div>
        </div>
        <div className={s.description}>
          <div className={s.content}>
            <span>Plug and play.</span>
            <span>
              CredBox seamlessly integrates with your third-party tools via our
              pre-built connectors. Connect the tools you use and eliminate any
              chance of human error and save your engineering efforts.
            </span>
            {/* <span>
              Know More
            </span> */}
          </div>
          <div className={s.blockContent}>
            <img
              src={boxes}
              alt={
                "Integrate tools like Intercom, Salesforce, Amazon, Zendesk, Strive and more with CredBox"
              }
            />
          </div>
        </div>
        <div className={s.infoBlockWrapper}>
          <div className={s.imageHolder} data-aos="fade-right">
            <img src={shield} />
          </div>
          <div className={s.contentHolder} data-aos="fade-left">
            <span>Encrypted end to end.</span>
            <span>
              Your user’s data is encrypted at CredBox, to ensure privacy.
              Special data protection measures are taken to keep all your user’s
              personal information secure.
            </span>
          </div>
        </div>
        <div className={`${s.introPane} ${s.greyBlock}`}>
          <span className={s.title}>Ready to get started?</span>
          <span className={s.secondTitle}>
            Sign up for our beta program to get early access.
          </span>
          {/* <span>Sign up for our beta program to get early access.</span> */}
          <div className={s.inviteBtnHolder}>
            <label>
              <input
                type="text"
                placeholder={"E-mail"}
                name={"inviteEmail"}
                onChange={this.handleChange}
              />
              {/* {validation.inviteEmail !== undefined &&
                !validation["inviteEmail"] && (
                  <span className={s.applyNow} onClick={this.sendInvite}>
                    Apply Now
                  </span>
                )} */}
              <div className={s.applyNow}>
                <Button
                  onClickHandler={this.sendInvite}
                  btnText={"Apply Now"}
                  isDisabled={
                    validation.inviteEmail !== undefined &&
                    !validation["inviteEmail"]
                      ? false
                      : true
                  }
                  type={"small"}
                />
              </div>
            </label>
          </div>
          {!emailInvite.loading && emailInvite.data && (
            <span>Thanks for registering. We'll be in touch shortly.</span>
          )}
        </div>
        {showCookieModal && (
          <Footer>
            <div className={s.acceptCookie}>
              This website uses cookies to ensure you get the best experience on
              our website.
              <span className={s.acceptCta} onClick={this.acceptCookie}>
                Got it!
              </span>
            </div>
          </Footer>
        )}
        <div className={s.footer}>
          <div>
            <span>Credbox &#xA9; 2019. All Rights Reserved.</span>
          </div>

          {/* <div>
            <span>Company</span>
            <span>
              <a href="#">Works Releases</a>
            </span>
            <span>
              <a href="#">Mission</a>
            </span>
            <span>
              <a href="#">Strategy</a>
            </span>
          </div>
          <div>
            <span>
              <a href="#">Support</a>
            </span>
            <span>
              <a href="#">Developers</a>
            </span>
            <span>
              <a href="#">Customer Service</a>
            </span>
            <span>
              <a href="#">Get Started Guide</a>
            </span>
          </div> */}

          {/* <span>
            Download Whitepaper
          </span> */}
        </div>
      </div>
    );
  }
}
