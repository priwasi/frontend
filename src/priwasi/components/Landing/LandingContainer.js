import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Landing from "./Landing";

import { bindActionCreators } from "redux";

import { sendInvite, scheduleDemo } from "./actions";

const mapStateToProps = (state, prevProps) => {
  return {
    emailInvite: state.websiteReducer.emailInvite,
    scheduleDemoData: state.websiteReducer.scheduleDemoData
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      sendInvite,
      scheduleDemo
    },
    dispatch
  );
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Landing)
);
