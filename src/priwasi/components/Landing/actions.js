/*
  @Module : CredBox
  @Component : View Requests
  @Type : Action Creators
  @Description : View All Requests
  @Author : Kaustubh Saxena
*/

/*
  Action Type Constants
*/
import {
  EMAIL_INVITE_LOADING,
  EMAIL_INVITE_SUCCESS,
  EMAIL_INVITE_FAILURE,
  SCHEDULE_DEMO_LOADING,
  SCHEDULE_DEMO_SUCCESS,
  SCHEDULE_DEMO_FAILURE
} from "../../constants/actionTypes";

/*
  Fetch Wrapper used to hit API.
*/
import httpFetch from "../../utils/http";

/*
  Utility Imports
*/
import { getHeaders, getUrl } from "../../utils/utilities";

/*
  @Purpose : To send an Invite via Email
  @System : CBB Backend
*/

export function sendInvite(data) {
  let url = `${getUrl("email")}?email=${data.email}`;
  console.log(url);
  return {
    types: [EMAIL_INVITE_LOADING, EMAIL_INVITE_SUCCESS, EMAIL_INVITE_FAILURE],
    promise: () => httpFetch(url, { method: "POST", headers: getHeaders() })
  };
}
/*
  @Purpose : To schedule A Demo
  @System : CBB Backend
*/

export function scheduleDemo(data) {
  let url = getUrl("scheduledemo");

  return {
    types: [
      SCHEDULE_DEMO_LOADING,
      SCHEDULE_DEMO_SUCCESS,
      SCHEDULE_DEMO_FAILURE
    ],
    promise: () =>
      httpFetch(url, { method: "POST", headers: getHeaders(), body: data })
  };
}
