// Action Type Constants
import {
  EMAIL_INVITE_LOADING,
  EMAIL_INVITE_SUCCESS,
  EMAIL_INVITE_FAILURE,
  EMAIL_INVITE_CLEAR,
  SCHEDULE_DEMO_LOADING,
  SCHEDULE_DEMO_SUCCESS,
  SCHEDULE_DEMO_FAILURE,
  SCHEDULE_DEMO_CLEAR
} from "../../constants/actionTypes";

import {
  getLoadingState,
  getSuccessState,
  getErrorState
} from "../../store/stateSettler";

const initialState = {
  emailInvite: {
    loading: false,
    data: null,
    error: null
  },
  scheduleDemoData: {
    loading: false,
    data: null,
    error: null
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case EMAIL_INVITE_LOADING:
      return {
        ...state,
        emailInvite: getLoadingState("emailInvite", state)
      };
    case EMAIL_INVITE_SUCCESS:
      return {
        ...state,
        emailInvite: getSuccessState("emailInvite", state, action)
      };
    case EMAIL_INVITE_FAILURE:
      return {
        ...state,
        emailInvite: getErrorState("emailInvite", state, action)
      };
    case EMAIL_INVITE_CLEAR:
      return {
        ...state,
        emailInvite: initialState.emailInvite
      };
    case SCHEDULE_DEMO_LOADING:
      return {
        ...state,
        scheduleDemoData: getLoadingState("scheduleDemoData", state)
      };
    case SCHEDULE_DEMO_SUCCESS:
      return {
        ...state,
        scheduleDemoData: getSuccessState("scheduleDemoData", state, action)
      };
    case SCHEDULE_DEMO_FAILURE:
      return {
        ...state,
        scheduleDemoData: getErrorState("scheduleDemoData", state, action)
      };
    case SCHEDULE_DEMO_CLEAR:
      return {
        ...state,
        scheduleDemoData: initialState.scheduleDemoData
      };

    default:
      return state;
  }
}
