/*
  @Module : CredBox
  @Component : Login
  @Type : Screen
  @Description : Login Screen
  @Author : Kaustubh Saxena 
*/

// React Imports
import React, { Component } from "react";

// Component Imports
import SiteLayout from "../../Layout/SiteLayout/SiteLayout";
import Button from "../../shared/Button/Button";
import Footer from "../../shared/Footer/Footer";
import FloatingFormField from "../../shared/FloatingFormField/FloatingFormField";
import Input from "../../shared/Input/Input";

// Stylesheet Imports
import s from "./Login.module.scss";
import logo from "../../public/priwasi/logo.svg";

// Utility Imports
import { getCookie } from "../../storage/cookie";

class Login extends Component {
  state = {
    email: null,
    password: null,
    loginError: false
  };

  componentDidMount = () => {
    //
    const { history } = this.props;
    if (getCookie(process.env.REACT_APP_ADMIN_COOKIE_NAME)) {
      history.push("/platform/");
    }
  };

  initiateLogin = () => {
    // cbclient.credbox.io / api / login / dummy
    const { email, password } = this.state;
    const { location, login, history } = this.props;
    switch (location.pathname) {
      case "/admin/login":
        if (email && password) {
          login({ email: email, password: password })
            .then(res => {
              console.log(res);
              history.push("/platform/");
            })
            .catch(err => {
              console.log(err);
              if (getCookie(process.env.REACT_APP_ADMIN_COOKIE_NAME)) {
                history.push("/platform/");
              } else {
                this.setState({ loginError: true });
              }
            });
        }
        break;
      case "/login":
        history.push("/login");
        break;
    }
  };

  handleChange = e => {
    let currentState = { ...this.state };
    currentState[e.target.name] = e.target.value;
    this.setState({ ...currentState });
  };

  render() {
    const { loginError } = this.state;
    return (
      <SiteLayout position="relative">
        <div className={s.loginContainer}>
          <div>
            <img src={logo} />
          </div>
          <div className={s.innerParent}>
            <div>
              {" "}
              <span className={s.bigText}>Welcome Back</span>{" "}
            </div>
            <div>
              <Input
                className={s.loginInput}
                placeholder={"Email"}
                name={"email"}
                onChangeHandler={this.handleChange}
              />
            </div>
            <div>
              <Input
                className={s.loginInput}
                placeholder={"Password"}
                name={"password"}
                onChangeHandler={this.handleChange}
                type={"password"}
              />
            </div>
            <div>
              <Button
                onClickHandler={() => {}}
                btnText={"Login"}
                onClickHandler={this.initiateLogin}
              />
              {loginError && <span>Login Error.</span>}
            </div>
          </div>
        </div>
      </SiteLayout>
    );
  }
}

export default Login;
