import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { login } from "./actions";

import Login from "./Login";

import { bindActionCreators } from "redux";

const mapStateToProps = (state, prevProps) => {
  return {
    loginData: state.loginReducer.loginData
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      login
    },
    dispatch
  );
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
