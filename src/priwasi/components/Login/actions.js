/*
  @Module : CredBox
  @Component : Privacy Public
  @Type : Action Creators
  @Description : Privacy
  @Author : Kaustubh Saxena
*/

/*
  Action Type Constants
*/
import {
  LOGIN_LOADING,
  LOGIN_SUCCESS,
  LOGIN_FAILURE
} from "../../constants/actionTypes";

/*
  Fetch Wrapper used to hit API.
*/

import httpFetch from "../../utils/http";

/*
  Utility Imports
*/
import { getHeaders, getUrl } from "../../utils/utilities";
import { getCookie } from "../../storage/cookie";

/*
  @Purpose : To Initiate Login Process
  @System : CBB Backend
*/

export function login(data) {
  let apiUrl = getUrl("adminLogin");
  return {
    types: [LOGIN_LOADING, LOGIN_SUCCESS, LOGIN_FAILURE],
    promise: () =>
      httpFetch(apiUrl, {
        headers: getHeaders(),
        method: "POST",
        credentials: "same-origin",
        body: JSON.stringify(data)
      })
  };
}
