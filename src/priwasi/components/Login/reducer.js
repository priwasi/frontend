// Action Type Constants
import {
  LOGIN_LOADING,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGIN_CLEAR
} from "../../constants/actionTypes";

import {
  getLoadingState,
  getSuccessState,
  getErrorState
} from "../../store/stateSettler";

const initialState = {
  login: {
    loading: false,
    data: null,
    error: null
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case LOGIN_LOADING:
      return {
        ...state,
        login: getLoadingState("login", state)
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        login: getSuccessState("login", state, action)
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        login: getErrorState("login", state, action)
      };
    case LOGIN_CLEAR:
      return {
        ...state,
        login: initialState.login
      };
    default:
      return state;
  }
}
