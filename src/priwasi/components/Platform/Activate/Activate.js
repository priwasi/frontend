/*
  @Module : CredBox
  @Component : Policy Info
  @Type : Screen
  @Description : Policy Info
  @Author : Kaustubh Saxena 
*/

// React Imports
import React, { useState, Component } from "react";

// Component Imports
import SiteLayout from "../../../Layout/SiteLayout/SiteLayout";
import Button from "../../../shared/Button/Button";
import Input from "../../../shared/Input/Input";

// Stylesheet Imports
import s from "./Activate.module.scss";

// Asset & Utility Imports
import logo from "../../../public/priwasi/logo.svg";

// Node Modules Import
import queryString from "query-string";

// import { MdEdit } from "react-icons/md";

class Activate extends Component {
  state = {
    formData: {},
    accessDenied: false
  };

  componentDidMount = () => {
    const { confirmToken, location } = this.props;
    let token = location.search;
    const parsed = queryString.parse(token);
    if (parsed.token) {
      confirmToken(parsed.token);
    } else {
      this.setState({ accessDenied: true });
    }
  };

  componentDidUpdate = (prevProps, prevState) => {
    const { createPasswordData, history } = this.props;
    if (
      prevProps.createPasswordData.loading &&
      !createPasswordData.loading &&
      createPasswordData.data
    ) {
      history.push("/cbb/admin/login");
    }
  };

  handleChange = e => {
    let formData = { ...this.state.formData };
    formData[e.target.name] = e.target.value;
    this.setState({ formData: formData });
  };

  intiateConfirmPassword = () => {
    const { createPassword, confirmTokenData } = this.props;
    const { formData } = this.state;
    let userId =
      confirmTokenData.data &&
      confirmTokenData.data.data &&
      confirmTokenData.data.data.userId;
    if (
      formData.password &&
      userId &&
      formData.confirmPassword &&
      formData.password === formData.confirmPassword
    ) {
      createPassword({ userId: userId, newPassword: formData.password });
    }
  };

  render() {
    if (this.state.accessDenied) {
      return <div>Invalid Token.</div>;
    }
    return (
      <SiteLayout position="relative">
        <div className={s.loginContainer}>
          <div>
            <img src={logo} />
          </div>
          <div className={s.innerParent}>
            <div>
              {" "}
              <span className={s.bigText}>Activate Your Account</span>{" "}
            </div>
            <div>
              <Input
                className={s.loginInput}
                placeholder={"Password"}
                name={"password"}
                onChangeHandler={this.handleChange}
                type={"password"}
              />
            </div>
            <div>
              <Input
                className={s.loginInput}
                placeholder={"Confirm Password"}
                name={"confirmPassword"}
                onChangeHandler={this.handleChange}
                type={"password"}
              />
            </div>
            <div>
              <Button
                onClickHandler={() => {}}
                btnText={"Confirm"}
                onClickHandler={this.intiateConfirmPassword}
              />
            </div>
          </div>
        </div>
      </SiteLayout>
    );
  }
}

export default Activate;
