import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

// Actions
import { confirmToken, createPassword } from "./actions";

import Activate from "./Activate";

import { bindActionCreators } from "redux";

const mapStateToProps = (state, prevProps) => {
  return {
    confirmTokenData: state.activateReducer.confirmTokenData,
    createPasswordData: state.activateReducer.createPasswordData
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      confirmToken,
      createPassword
    },
    dispatch
  );
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Activate)
);
