/*
  @Module : CredBox
  @Component : Activate
  @Type : Action Creators
  @Description : Activate User
  @Author : Kaustubh Saxena
*/

/*
  Action Type Constants
*/
import {
  CONFIRM_TOKEN_LOADING,
  CONFIRM_TOKEN_SUCCESS,
  CONFIRM_TOKEN_FAILURE,
  CREATE_PASSWORD_LOADING,
  CREATE_PASSWORD_SUCCESS,
  CREATE_PASSWORD_FAILURE,
  CREATE_PASSWORD_CLEAR
} from "../../../constants/actionTypes";

/*
  Fetch Wrapper used to hit API.
*/

import httpFetch from "../../../utils/http";

/*
  Utility Imports
*/
import { getHeaders, getUrl } from "../../../utils/utilities";

/*
  @Purpose : Confirm Token
  @System : CBB Backend
*/

export function confirmToken(token) {
  let apiUrl = `${getUrl("confirmToken")}?token=${token}`;
  return {
    types: [
      CONFIRM_TOKEN_LOADING,
      CONFIRM_TOKEN_SUCCESS,
      CONFIRM_TOKEN_FAILURE
    ],
    promise: () =>
      httpFetch(apiUrl, {
        headers: getHeaders()
      })
  };
}

/*
    @Purpose : Create Password
    @System : CBB Backend
*/

export function createPassword(data) {
  let apiUrl = getUrl("createPassword");
  return {
    types: [
      CREATE_PASSWORD_LOADING,
      CREATE_PASSWORD_SUCCESS,
      CREATE_PASSWORD_FAILURE
    ],
    promise: () =>
      httpFetch(apiUrl, {
        headers: getHeaders(),
        method: "POST",
        body: JSON.stringify(data)
      })
  };
}
