/*
  @Module : CredBox
  @Component : Change Password
  @Type : Screen
  @Description : Change Password
  @Author : Kaustubh Saxena 
*/

// React Imports
import React, { useState, Component } from "react";

// Component Imports
import SiteLayout from "../../../Layout/SiteLayout/SiteLayout";
import Button from "../../../shared/Button/Button";
import Input from "../../../shared/Input/Input";

// Stylesheet Imports
import s from "./ChangePassword.module.scss";

// Asset & Utility Imports
import logo from "../../../public/priwasi/logo.svg";

// Node Modules Import
import queryString from "query-string";
import PlatformLayout from "../../../Layout/PlatformLayout/PlatformLayout";
import PlatformHeaderChildren from "../../../shared/PlatformHeaderChildren";

// import { MdEdit } from "react-icons/md";

class ChangePassword extends Component {
  state = {
    formData: {},
    accessDenied: false
  };

  componentDidMount = () => {
    //
  };

  componentDidUpdate = (prevProps, prevState) => {
    const { changePasswordData, history } = this.props;
    if (
      prevProps.changePasswordData.loading &&
      !changePasswordData.loading &&
      changePasswordData.data
    ) {
      alert("change password success");
    }
  };

  handleChange = e => {
    let formData = { ...this.state.formData };
    formData[e.target.name] = e.target.value;
    this.setState({ formData: formData });
  };

  intiateConfirmPassword = () => {
    const { changePassword, userInfo } = this.props;
    const { formData } = this.state;
    let userId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;
    if (formData.password && userId && formData.newPassword) {
      changePassword({
        userId: userId,
        oldPassword: formData.password,
        newPassword: formData.newPassword
      });
    }
  };

  render() {
    if (this.state.accessDenied) {
      return <div>Invalid Token.</div>;
    }
    return (
      <PlatformLayout
        white
        position="relative"
        headerChildren={<PlatformHeaderChildren />}
      >
        <div className={s.loginContainer}>
          <div className={s.innerParent}>
            <div>
              {" "}
              <span className={s.bigText}>Change Password</span>{" "}
            </div>
            <div>
              <Input
                className={s.loginInput}
                placeholder={"Old Password"}
                name={"password"}
                onChangeHandler={this.handleChange}
                type={"password"}
              />
            </div>
            <div>
              <Input
                className={s.loginInput}
                placeholder={"New Password"}
                name={"newPassword"}
                onChangeHandler={this.handleChange}
                type={"password"}
              />
            </div>
            <div>
              <Button
                btnText={"Confirm"}
                onClickHandler={this.intiateConfirmPassword}
              />
            </div>
          </div>
        </div>
      </PlatformLayout>
    );
  }
}

export default ChangePassword;
