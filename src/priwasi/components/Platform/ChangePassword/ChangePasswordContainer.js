import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

// Actions
import { changePassword } from "./actions";

import ChangePassword from "./ChangePassword";

import { bindActionCreators } from "redux";

const mapStateToProps = (state, prevProps) => {
  return {
    changePasswordData: state.changePasswordReducer.createPasswordData,
    userInfo: state.app.userInfo
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      changePassword
    },
    dispatch
  );
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ChangePassword)
);
