/*
  @Module : CredBox
  @Component : Activate
  @Type : Action Creators
  @Description : Activate User
  @Author : Kaustubh Saxena
*/

/*
  Action Type Constants
*/
import {
  CONFIRM_TOKEN_LOADING,
  CONFIRM_TOKEN_SUCCESS,
  CONFIRM_TOKEN_FAILURE,
  CHANGE_PASSWORD_LOADING,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_FAILURE,
  CHANGE_PASSWORD_CLEAR
} from "../../../constants/actionTypes";

/*
  Fetch Wrapper used to hit API.
*/

import httpFetch from "../../../utils/http";

/*
  Utility Imports
*/
import { getHeaders, getUrl } from "../../../utils/utilities";

/*
  @Purpose : Confirm Token
  @System : CBB Backend
*/

export function confirmToken(token) {
  let apiUrl = `${getUrl("confirmToken")}?token=${token}`;
  return {
    types: [
      CONFIRM_TOKEN_LOADING,
      CONFIRM_TOKEN_SUCCESS,
      CONFIRM_TOKEN_FAILURE
    ],
    promise: () =>
      httpFetch(apiUrl, {
        headers: getHeaders()
      })
  };
}

/*
    @Purpose : Create Password
    @System : CBB Backend
*/

export function changePassword(data) {
  let apiUrl = getUrl("changePassword");
  return {
    types: [
      CHANGE_PASSWORD_LOADING,
      CHANGE_PASSWORD_SUCCESS,
      CHANGE_PASSWORD_FAILURE
    ],
    promise: () =>
      httpFetch(apiUrl, {
        headers: getHeaders(true, true),
        method: "POST",
        body: JSON.stringify(data)
      })
  };
}
