// Action Type Constants
import {
  CONFIRM_TOKEN_LOADING,
  CONFIRM_TOKEN_SUCCESS,
  CONFIRM_TOKEN_FAILURE,
  CONFIRM_TOKEN_CLEAR,
  CREATE_PASSWORD_LOADING,
  CREATE_PASSWORD_SUCCESS,
  CREATE_PASSWORD_FAILURE,
  CREATE_PASSWORD_CLEAR
} from "../../../constants/actionTypes";

import {
  getLoadingState,
  getSuccessState,
  getErrorState
} from "../../../store/stateSettler";

const initialState = {
  confirmTokenData: {
    loading: false,
    data: null,
    error: null
  },
  createPasswordData: {
    loading: false,
    data: null,
    error: null
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CONFIRM_TOKEN_LOADING:
      return {
        ...state,
        confirmTokenData: getLoadingState("confirmTokenData", state)
      };
    case CONFIRM_TOKEN_SUCCESS:
      return {
        ...state,
        confirmTokenData: getSuccessState("confirmTokenData", state, action)
      };
    case CONFIRM_TOKEN_FAILURE:
      return {
        ...state,
        confirmTokenData: getErrorState("confirmTokenData", state, action)
      };
    case CONFIRM_TOKEN_CLEAR:
      return {
        ...state,
        confirmTokenData: initialState.confirmTokenData
      };
    case CREATE_PASSWORD_LOADING:
      return {
        ...state,
        createPasswordData: getLoadingState("createPasswordData", state)
      };
    case CREATE_PASSWORD_SUCCESS:
      return {
        ...state,
        createPasswordData: getSuccessState("createPasswordData", state, action)
      };
    case CREATE_PASSWORD_FAILURE:
      return {
        ...state,
        createPasswordData: getErrorState("createPasswordData", state, action)
      };
    case CREATE_PASSWORD_CLEAR:
      return {
        ...state,
        createPasswordData: initialState.createPasswordData
      };
    default:
      return state;
  }
}
