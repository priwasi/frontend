/*
  @Module : CredBox
  @Component : Policy Info
  @Type : Screen
  @Description : Policy Info
  @Author : Kaustubh Saxena 
*/

// React Imports
import React, { useState, Component } from "react";

// Component Imports
import PlatformLayout from "../../../Layout/PlatformLayout/PlatformLayout";
import PlatformHeaderChildren from "../../../shared/PlatformHeaderChildren";

// Stylesheet Imports
import s from "./ConnectorInstall.module.scss";

// Asset & Utility Imports
import bin from "../../../public/images/bin.svg";
import { policyMock, newPolicyObj } from "./schema";

// Node Modules Import
import EditorComponent from "../../../shared/EditorComponent/EditorComponent";
import EditableInput from "../../../shared/EditableInput/EditableInput";
import Button from "../../../shared/Button/Button";
import AccessControl from "../../../services/AccessControl/AccessControl";
import DotLoader from "../../../shared/DotLoader";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Checkbox from "../../../shared/Checkbox/Checkbox";

const toastMessageMap = {};

class ConnectorInstall extends Component {
  state = {};

  componentDidMount = () => {
    const { params } = this.props.match;
    const { submitInstallerCode, userInfo } = this.props;
    if (params.code) {
      let clientId =
        userInfo.data && userInfo.data.data && userInfo.data.data.userId;
      submitInstallerCode(clientId, params.code);
    }
  };

  componentDidUpdate = (prevProps, prevState) => {
    const { connectorInstallData } = this.props;
    if (
      prevProps.connectorInstallData.loading &&
      !connectorInstallData.loading &&
      connectorInstallData.data
    ) {
      window.close();
    }
  };

  /*
    Toast Trigger Method
  */
  toastTrigger = (key, prevProps) => {
    const { getPolicySections, userInfo } = this.props;
    let clientId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;
    if (
      prevProps[key].loading &&
      !this.props[key].loading &&
      this.props[key].data
    ) {
      toast.success(toastMessageMap[key]);
    }
  };

  render() {
    const { params } = this.props.match;
    return (
      <PlatformLayout
        white
        position="relative"
        headerChildren={<PlatformHeaderChildren />}
      >
        {
          <div className={s.contentPanel}>
            <div className={s.upperPanel}>
              <span className={s.title}>Installing {params.name}</span>
            </div>
            <div>
              <DotLoader />
            </div>
          </div>
        }
      </PlatformLayout>
    );
  }
}

export default ConnectorInstall;
