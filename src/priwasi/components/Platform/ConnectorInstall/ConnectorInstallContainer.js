import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { submitInstallerCode } from "./actions";

import ConnectorInstall from "./ConnectorInstall";

import { bindActionCreators } from "redux";

const mapStateToProps = (state, prevProps) => {
  return {
    connectorInstallData: state.connectorReducer.connectorInstallData,
    userInfo: state.app.userInfo
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      submitInstallerCode
    },
    dispatch
  );
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ConnectorInstall)
);
