/*
  @Module : CredBox
  @Component : View Requests
  @Type : Action Creators
  @Description : View All Requests
  @Author : Kaustubh Saxena
*/

/*
  Action Type Constants
*/
import {
  INSTALL_CONNECTOR_LOADING,
  INSTALL_CONNECTOR_SUCCESS,
  INSTALL_CONNECTOR_FAILURE,
  INSTALL_CONNECTOR_CLEAR
} from "../../../constants/actionTypes";

/*
  Fetch Wrapper used to hit API.
*/
import httpFetch from "../../../utils/http";

/*
  Utility Imports
*/
import { getHeaders, getUrl } from "../../../utils/utilities";

/*
  @Purpose : To Submit Installer Connector Code to Backend
  @System : CBB Backend
*/

export function submitInstallerCode(clientId, code) {
  let url = `${getUrl("connectorInstall")}?code=${code}`;
  let headers = getHeaders(true, true);
  headers["clientId"] = clientId;
  return {
    types: [
      INSTALL_CONNECTOR_LOADING,
      INSTALL_CONNECTOR_SUCCESS,
      INSTALL_CONNECTOR_FAILURE
    ],
    promise: () => httpFetch(url, { headers: headers })
  };
}
