// Action Type Constants
import {
  INSTALL_CONNECTOR_LOADING,
  INSTALL_CONNECTOR_SUCCESS,
  INSTALL_CONNECTOR_FAILURE,
  INSTALL_CONNECTOR_CLEAR
} from "../../../constants/actionTypes";

import {
  getLoadingState,
  getSuccessState,
  getErrorState
} from "../../../store/stateSettler";

const initialState = {
  connectorInstallData: {
    loading: false,
    data: null,
    error: null
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case INSTALL_CONNECTOR_LOADING:
      return {
        ...state,
        connectorInstallData: getLoadingState("connectorInstallData", state)
      };
    case INSTALL_CONNECTOR_SUCCESS:
      return {
        ...state,
        connectorInstallData: getSuccessState(
          "connectorInstallData",
          state,
          action
        )
      };
    case INSTALL_CONNECTOR_FAILURE:
      return {
        ...state,
        connectorInstallData: getErrorState(
          "connectorInstallData",
          state,
          action
        )
      };
    case INSTALL_CONNECTOR_CLEAR:
      return {
        ...state,
        connectorInstallData: initialState.connectorInstallData
      };

    default:
      return state;
  }
}
