/*
  @Module : CredBox
  @Component : Request
  @Type : Screen
  @Description : Request Listing
  @Author : Kaustubh Saxena 
*/

// React Imports
import React, { useState, Component } from "react";

// Component Imports
import PlatformLayout from "../../../Layout/PlatformLayout/PlatformLayout";
import TableRow from "../../../shared/TableRow/TableRow";
import PlatformHeaderChildren from "../../../shared/PlatformHeaderChildren";
import { convertToFormat } from "../../../utils/dateUtils";

// Stylesheet Imports
import s from "./Dashboard.module.scss";
import "../../../commonstyles/reactMultiselectCheckbox.css";

// Asset Imports
import crossIcon from "../../../public/images/crossIcon.svg";

// Node Modules Import
import ReactMultiSelectCheckboxes from "react-multiselect-checkboxes";
import { Doughnut, Chart } from "react-chartjs-2";
import StatCard from "../../../shared/StatCard/StatCard";
import Select from "react-select";

// Utility Imports
import { getDashboardChartOptions } from "../../../utils/helper";
import NotFound from "../../../shared/NotFound/NotFound";
import Paginator from "../../../shared/PaginationComponent/Paginator";

/////////////////
var originalDoughnutDraw = Chart.controllers.doughnut.prototype.draw;
Chart.helpers.extend(Chart.controllers.doughnut.prototype, {
  draw: function() {
    originalDoughnutDraw.apply(this, arguments);

    var chart = this.chart;
    var width = chart.chart.width,
      height = chart.chart.height,
      ctx = chart.chart.ctx;

    var fontSize = (height / 114).toFixed(2);
    ctx.font = fontSize + "em sans-serif";
    ctx.textBaseline = "middle";

    var sum = 0;
    for (var i = 0; i < chart.config.data.datasets[0].data.length; i++) {
      sum += chart.config.data.datasets[0].data[i];
    }

    var text = sum,
      textX = Math.round((width - ctx.measureText(text).width) / 4.7),
      textY = height / 2;
    ctx.fillText(text, textX, textY);
  }
});
/////////////////

const options = [
  { label: "Last 7 days", value: 7, unit: "DAY" },
  { label: "Last 15 days", value: 15, unit: "DAY" },
  { label: "Last 20 days", value: 20, unit: "DAY" },
  { label: "Last 60 days", value: 60, unit: "DAY" },
  { label: "Last 90 days", value: 90, unit: "DAY" }
  // { label: "Last 1 month", value: 1, unit: "MONTH" },
  // { label: "Last 2 months", value: 2, unit: "MONTH" }
];

// Custom Styles for Select

const customStyles = {
  control: (styles, { isFocused }) => ({
    ...styles,
    backgroundColor: "#fff",
    borderColor: isFocused ? "#eaeaea" : "#eaeaea",
    borderRadius: "20.5px",
    border: "1px solid #dbdbdb",
    minWidth: "170px",
    width: "auto",
    fontSize: "12px",
    padding: "0px",
    marginLeft: "20px"
  }),
  option: (styles, { data, isDisabled, isFocused, isSelected }) => {
    return {
      ...styles,
      backgroundColor: isFocused ? "#efeef8" : "white",
      color: "#000",
      cursor: isDisabled ? "not-allowed" : "default",
      minWidth: "200px"
    };
  }
};

const tabsSchema = [
  {
    title: "Expiring Requests",
    config_key: "expiring",
    icon: false
  },
  {
    title: "Expired",
    config_key: "expired",
    icon: false
  }
];
// silos name, owner, source, identifier
const bodyHeaderSchema = [
  {
    title: "Request ID",
    config_key: "id",
    icon: false
  },
  {
    title: "Request Type",
    config_key: "requestType",
    icon: false
  },
  {
    title: "Date",
    config_key: "date",
    icon: false
  },
  {
    title: "Time Remaining",
    config_key: "timeRemaining",
    icon: false
  },
  {
    title: "User Identifier",
    config_key: "identifier",
    icon: false
  },
  {
    title: "Status",
    config_key: "status",
    icon: false
  },
  {
    title: "User Type",
    config_key: "userType",
    icon: false
  }
];

class Dashboard extends Component {
  state = {
    formData: {},
    currentTab: "expiring",
    duration: 7,
    durationUnit: "DAY",
    assignee: "ALL",
    assigneeOptions: []
  };

  componentDidMount = () => {
    const { getDashboardData, userInfo } = this.props;
    const { duration, durationUnit, assignee } = this.state;
    let clientId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;
    let data = {
      duration: duration,
      durationUnit: durationUnit,
      assignee: assignee
    };

    getDashboardData(data, clientId);
  };

  componentDidUpdate = (prevProps, prevState) => {
    const { assignees } = this.props;
    if (
      prevProps.assignees.loading &&
      !assignees.loading &&
      assignees.data &&
      assignees.data.data
    ) {
      this.generateAssigneeOptions();
    }
  };

  generateAssigneeOptions = () => {
    const { assignees } = this.props;
    let assigneeOptions = assignees.data.data.map((item, index) => {
      return {
        label: `${item.name ? item.name : item.emailId}`,
        value: item.emailId
      };
    });
    assigneeOptions.unshift({ label: "All", value: "ALL" });
    this.setState({ assigneeOptions: assigneeOptions });
  };

  /*
    Add Request Modal Toggle
    @params : none
  */

  getTabs = () => {
    const { currentTab } = this.state;
    return (
      <ul className={s.tabsHeader}>
        {tabsSchema.map((item, index) => {
          return (
            <li
              key={`${index}_header`}
              className={`${currentTab === item.config_key ? s.active : ""}`}
              onClick={() => this.switchTabs(item.config_key)}
            >
              {item.title}
            </li>
          );
        })}
      </ul>
    );
  };

  /*
    Handle Tab Switch Click
    @params : config_key=String
  */
  switchTabs = config_key => {
    this.setState({ currentTab: config_key });
  };

  /*
    Toggle Add Request
    @params :none
  */
  toggleAddRequest = () => {
    this.setState({ showAddRequest: !this.state.showAddRequest });
  };

  /*
    Close Add Request 
    params : none
  */
  closeAddRequest = () => {
    this.setState({ showAddRequest: false });
    window.history.back();
  };

  /*
    Show Calendar Handler
    params : none
  */
  toggleCalendar = () => {
    this.setState({ showCalendar: !this.state.showCalendar });
  };

  /*
    Manage Calendar Callback
    params : date =  Object;
  */
  manageCalendarCallback = date => {
    let formData = { ...this.state.formData };
    formData["eta"] = convertToFormat(date, "yyyy-mm-dd");
    this.setState({ formData: formData });
    this.toggleCalendar();
  };

  /*
    Push to Request Details
    params : request = Object
  */
  pushToRequestDetails = request => {
    const { history } = this.props;
    let id = request.request_id && request.request_id.split(" ").join("");
    history.push(`/platform/${id}/access`);
  };

  /*
    Get Tab Body
    @params : none
  */
  getTabBody = () => {
    const {
      dashboard,
      getMoreRequestData,
      userInfo,
      expiredRequests,
      expiringRequests
    } = this.props;
    const { currentTab } = this.state;
    let dashboardData = dashboard.data && dashboard.data.data;
    let clientId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;
    let currentFilters = {
      duration: 60,
      durationUnit: "DAY",
      assignee: "ALL"
    };
    let data = [];
    let count = null;
    if (currentTab === "expiring") {
      if (
        dashboardData &&
        dashboardData.requestDetails &&
        dashboardData.requestDetails.expiring
      ) {
        count = dashboardData.requestDetails.expiring.totalHits;
        data = expiringRequests.data && expiringRequests.data.data;
      }
    } else {
      if (
        dashboardData &&
        dashboardData.requestDetails &&
        dashboardData.requestDetails.expired
      ) {
        count = dashboardData.requestDetails.expired.totalHits;
        data = expiredRequests.data && expiredRequests.data.data;
      }
    }

    return (
      <div className={s.tabsBody}>
        <div className={s.bodyHeaders}>
          {bodyHeaderSchema.map((item, index) => {
            return <span key={`${index}_header`}>{item.title}</span>;
          })}
        </div>
        <Paginator
          type={"infinite"}
          total={count + 1}
          pageSize={10}
          action={getMoreRequestData}
          currentFilters={currentFilters}
          clientId={clientId}
          currentTab={currentTab}
        >
          <div className={`${s.rowWrapper}`}>
            {data.map((item, index) => {
              let rowItem = { ...item };
              rowItem["raisedAt"] = convertToFormat(
                new Date(rowItem.raisedAt),
                "yyyy-mm-dd"
              );
              return (
                <div
                  className={`${s.rowParent}`}
                  key={`${index}_tableParent`}
                  onClick={() => this.pushToRequestDetails(item)}
                >
                  <TableRow key={`${index}_tableRow`} data={rowItem} />
                </div>
              );
            })}
          </div>
        </Paginator>
        {data.length === 0 && <NotFound text={"No Requests Found"} />}
      </div>
    );
  };
  /*
    Get Graph Data
  */
  getGraphData = type => {
    const { dashboard } = this.props;
    let dashboardData = dashboard.data && dashboard.data.data;
    let open =
      dashboardData &&
      dashboardData.aggregatedRequest &&
      dashboardData.aggregatedRequest.open &&
      dashboardData.aggregatedRequest.open;
    let closed =
      dashboardData &&
      dashboardData.aggregatedRequest &&
      dashboardData.aggregatedRequest.open &&
      dashboardData.aggregatedRequest.open;
    let labels, dataPoints;

    switch (type) {
      case "open":
        labels = ["Access Requests", "Delete Requests"];
        dataPoints =
          open && open.length > 0 ? [open[0].count, open[1].count] : [];
        const data = {
          labels: labels,
          datasets: [
            {
              data: dataPoints,
              backgroundColor: ["#32a2ff", "#1accb0"],
              hoverBackgroundColor: ["#32a2ff", "#1accb0"]
            }
          ],
          text: "23%"
        };
        return data;
      case "closed":
        labels = ["Access Requests", "Delete Requests"];
        dataPoints =
          closed && closed.length > 0 ? [closed[0].count, closed[1].count] : [];
        const data2 = {
          labels: labels,
          datasets: [
            {
              data: dataPoints,
              backgroundColor: ["#4b5ffc", "#f08e23"],
              hoverBackgroundColor: ["#4b5ffc", "#f08e23"]
            }
          ]
        };
        return data2;
      default:
        break;
    }
  };

  /*
    Handle Filter Change
    @params : e = Object
  */
  changeFilter = e => {
    const { getDashboardData, userInfo } = this.props;
    // getDashboardData();
    let clientId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;

    /*
      Check which filter Applied
    */
    let filters = {
      duration: this.state.duration,
      durationUnit: this.state.durationUnit,
      assignee: this.state.assignee
    };

    if (e.unit) {
      filters["duration"] = e.value;
      filters["durationUnit"] = e.unit;
    } else {
      filters["assignee"] = e.value;
    }
    this.setState({ ...filters });
    getDashboardData(filters, clientId);
  };

  render() {
    const { dashboard } = this.props;
    console.log(typeof originalDoughnutDraw);
    let dashboardData = dashboard.data && dashboard.data.data;
    let internal =
      dashboardData &&
      dashboardData.silos &&
      dashboardData.silos.internal &&
      dashboardData.silos.internal.count
        ? dashboardData.silos.internal.count
        : 0;
    let external =
      dashboardData &&
      dashboardData.silos &&
      dashboardData.silos.external &&
      dashboardData.silos.external.count
        ? dashboardData.silos.external.count
        : 0;
    return (
      <PlatformLayout
        white
        position="relative"
        headerChildren={<PlatformHeaderChildren />}
      >
        <div className={s.contentPanel}>
          <div className={s.upperPanel}>
            <span className={s.title}>
              <span>Dashboard</span>
            </span>
          </div>
          <div className={s.statParent}>
            <div className={s.analytics}>
              <div className={s.header}>
                <span className={s.elements}>
                  Total Requests:{" "}
                  {dashboardData &&
                    dashboardData.aggregatedRequest &&
                    dashboardData.aggregatedRequest.totalCount}
                </span>
                <div className={s.elements}>
                  <span>In Last</span>
                  <Select
                    name="duration"
                    styles={customStyles}
                    cacheOptions
                    options={options}
                    onChange={this.changeFilter}
                    defaultValue={options.filter(option => option.value === 7)}
                  />
                </div>
                <div className={s.elements}>
                  <span>Assignee</span>
                  <Select
                    name="assignees"
                    styles={customStyles}
                    cacheOptions
                    options={this.state.assigneeOptions}
                    onChange={this.changeFilter}
                    defaultValue={this.state.assigneeOptions[0]}
                  />
                </div>
              </div>
              <div className={s.chartContainer}>
                <div>
                  <Doughnut
                    data={this.getGraphData("open")}
                    options={getDashboardChartOptions()}
                  />
                  <span className={s.typeLabel}>Open</span>
                </div>
                <div>
                  <div>
                    <Doughnut
                      data={this.getGraphData("closed")}
                      options={getDashboardChartOptions()}
                    />
                    <span className={s.typeLabel}>Closed</span>
                  </div>
                </div>
              </div>
            </div>
            <div className={s.count}>
              <StatCard title={"Internal Data Sources"} content={internal} />
              <StatCard title={"External Data Sources"} content={external} />
            </div>
          </div>
          <div className={s.tabsWrapper}>
            {/*-------------------- Tabs Data --------------------*/}
            <div className={s.headerWrapper}>{this.getTabs()}</div>
            {this.getTabBody()}
            {/*-------------------- Tabs Data --------------------*/}
          </div>
        </div>
      </PlatformLayout>
    );
  }
}

export default Dashboard;
