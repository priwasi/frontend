import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

// Actions
import { getDashboardData, getMoreRequestData } from "./actions";

import Dashboard from "./Dashboard";

import { bindActionCreators } from "redux";

const mapStateToProps = (state, prevProps) => {
  return {
    dashboard: state.dashboardReducer.dashboard,
    userInfo: state.app.userInfo,
    assignees: state.app.assignees,
    expiredRequests: state.dashboardReducer.expiredRequests,
    expiringRequests: state.dashboardReducer.expiringRequests
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getDashboardData,
      getMoreRequestData
    },
    dispatch
  );
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Dashboard)
);
