/*
  @Module : CredBox
  @Component : Dashboard
  @Type : Action Creators
  @Description : Dashboard
  @Author : Kaustubh Saxena
*/

/*
  Action Type Constants
*/
import {
  DASHBOARD_DATA_LOADING,
  DASHBOARD_DATA_SUCCESS,
  DASHBOARD_DATA_FAILURE,
  DASHBOARD_DATA_CLEAR,
  MORE_EXPIRING_REQUEST_LOADING,
  MORE_EXPIRING_REQUEST_SUCCESS,
  MORE_EXPIRING_REQUEST_FAILURE,
  MORE_EXPIRED_REQUEST_LOADING,
  MORE_EXPIRED_REQUEST_SUCCESS,
  MORE_EXPIRED_REQUEST_FAILURE
} from "../../../constants/actionTypes";

/*
  Fetch Wrapper used to hit API.
*/

import httpFetch from "../../../utils/http";

/*
  Utility Imports
*/
import { getHeaders, getUrl } from "../../../utils/utilities";
import { getCookie } from "../../../storage/cookie";

/*
  @Purpose : Get Dashboard Data
  @System : CBB Backend
*/

export function getDashboardData(data, clientId) {
  let apiUrl = getUrl("dashboard");
  let headers = getHeaders(true, true);
  headers["clientid"] = clientId;
  return {
    types: [
      DASHBOARD_DATA_LOADING,
      DASHBOARD_DATA_SUCCESS,
      DASHBOARD_DATA_FAILURE
    ],
    promise: () =>
      httpFetch(apiUrl, {
        headers: headers,
        method: "POST",
        body: JSON.stringify(data)
      })
  };
}

/*
  @Purpose : Get Paginated Request Data
  @System : CBB Backend
*/

export function getMoreRequestData(data, clientId, page) {
  let apiUrl = `${getUrl("moreRequests")}?page=${page}&isExpired=false`;
  let types = [
    MORE_EXPIRING_REQUEST_LOADING,
    MORE_EXPIRING_REQUEST_SUCCESS,
    MORE_EXPIRING_REQUEST_FAILURE
  ];
  if (data.type === "expired") {
    apiUrl = `${apiUrl}&isExpired=true`;
    types = [
      MORE_EXPIRED_REQUEST_LOADING,
      MORE_EXPIRED_REQUEST_SUCCESS,
      MORE_EXPIRED_REQUEST_FAILURE
    ];
  }
  let data_ = { ...data };
  delete data_.type;
  let headers = getHeaders(true, true);
  headers["clientid"] = clientId;
  return {
    types: types,
    promise: () =>
      httpFetch(apiUrl, {
        headers: headers,
        method: "POST",
        body: JSON.stringify(data_)
      })
  };
}
