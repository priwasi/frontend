// Action Type Constants
import {
  DASHBOARD_DATA_LOADING,
  DASHBOARD_DATA_SUCCESS,
  DASHBOARD_DATA_FAILURE,
  DASHBOARD_DATA_CLEAR,
  MORE_EXPIRING_REQUEST_LOADING,
  MORE_EXPIRING_REQUEST_SUCCESS,
  MORE_EXPIRING_REQUEST_FAILURE,
  MORE_EXPIRING_REQUEST_CLEAR,
  MORE_EXPIRED_REQUEST_LOADING,
  MORE_EXPIRED_REQUEST_SUCCESS,
  MORE_EXPIRED_REQUEST_FAILURE,
  MORE_EXPIRED_REQUEST_CLEAR
} from "../../../constants/actionTypes";

import {
  getLoadingState,
  getSuccessState,
  getErrorState
} from "../../../store/stateSettler";

const initialState = {
  dashboard: {
    loading: false,
    data: null,
    error: null
  },
  expiringRequests: {
    loading: false,
    data: null,
    error: null
  },
  expiredRequests: {
    loading: false,
    data: null,
    error: null
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case DASHBOARD_DATA_LOADING:
      return {
        ...state,
        dashboard: getLoadingState("dashboard", state)
      };
    case DASHBOARD_DATA_SUCCESS:
      let expiringRequest = {
        result: action.result.data.requestDetails.expiring
      };
      let expiredRequest = {
        result: action.result.data.requestDetails.expired
      };
      return {
        ...state,
        dashboard: getSuccessState("dashboard", state, action),
        expiringRequests: getSuccessState(
          "expiringRequests",
          state,
          expiringRequest
        ),
        expiredRequests: getSuccessState(
          "expiredRequests",
          state,
          expiredRequest
        )
      };
    case DASHBOARD_DATA_FAILURE:
      return {
        ...state,
        dashboard: getErrorState("dashboard", state, action)
      };
    case DASHBOARD_DATA_CLEAR:
      return {
        ...state,
        dashboard: initialState.dashboard
      };
    case MORE_EXPIRING_REQUEST_LOADING:
      return {
        ...state,
        expiringRequests: getLoadingState("expiringRequests", state)
      };
    case MORE_EXPIRING_REQUEST_SUCCESS:
      let dataObj = { ...state.expiringRequests };
      let prevData =
        state.expiringRequests.data && state.expiringRequests.data.data;
      let nextData = action.result.data && action.result.data;
      let updatedArr = prevData.concat(nextData);
      dataObj.data.data = updatedArr;
      return {
        ...state,
        expiringRequests: {
          loading: false,
          data: dataObj.data,
          error: null
        }
      };
    case MORE_EXPIRING_REQUEST_FAILURE:
      return {
        ...state,
        expiringRequests: getErrorState("expiringRequests", state, action)
      };
    case MORE_EXPIRING_REQUEST_CLEAR:
      return {
        ...state,
        expiringRequests: initialState.expiringRequests
      };
    case MORE_EXPIRED_REQUEST_LOADING:
      return {
        ...state,
        expiredRequests: getLoadingState("expiredRequests", state)
      };
    case MORE_EXPIRED_REQUEST_SUCCESS:
      let dataObj_ = { ...state.expiredRequests };
      let prevData_ =
        state.expiredRequests.data && state.expiredRequests.data.data;
      let nextData_ = action.result.data && action.result.data;
      let updatedArr_ = prevData_.concat(nextData_);
      dataObj_.data.data = updatedArr_;
      return {
        ...state,
        expiredRequests: {
          loading: false,
          data: dataObj_.data,
          error: null
        }
      };
    case MORE_EXPIRED_REQUEST_FAILURE:
      return {
        ...state,
        expiredRequests: getErrorState("expiredRequests", state, action)
      };
    case MORE_EXPIRED_REQUEST_CLEAR:
      return {
        ...state,
        expiredRequests: initialState.expiredRequests
      };

    default:
      return state;
  }
}
