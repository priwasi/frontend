/*
  @Module : CredBox
  @Component : Policy Info
  @Type : Screen
  @Description : Policy Info
  @Author : Kaustubh Saxena 
*/

// React Imports
import React, { useState, Component } from "react";

// Component Imports
import PlatformLayout from "../../../Layout/PlatformLayout/PlatformLayout";
import PlatformHeaderChildren from "../../../shared/PlatformHeaderChildren";

// Stylesheet Imports
import s from "./PolicyInfo.module.scss";

// Asset & Utility Imports
import bin from "../../../public/images/bin.svg";
import { policyMock, newPolicyObj } from "./schema";

// Node Modules Import
import EditorComponent from "../../../shared/EditorComponent/EditorComponent";
import EditableInput from "../../../shared/EditableInput/EditableInput";
import Button from "../../../shared/Button/Button";
import AccessControl from "../../../services/AccessControl/AccessControl";
import DotLoader from "../../../shared/DotLoader";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Checkbox from "../../../shared/Checkbox/Checkbox";

const toastMessageMap = {
  policyCreate: "Privacy Policy Content Created Successfully",
  policyDelete: "Privacy Policy Section Deleted Successfully"
};

class PolicyInfo extends Component {
  state = {
    descriptionEdit: false,
    titleEdit: false,
    titleValues: [],
    descriptionValues: [],
    policies: [],
    idArr: [],
    editHover: false,
    showBottomPane: false
  };

  componentDidMount = () => {
    const { getPolicySections, userInfo } = this.props;
    let clientId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;
    getPolicySections(clientId);
  };

  componentDidUpdate = (prevProps, prevState) => {
    const { policyList } = this.props;
    if (
      prevProps.policyList.loading &&
      !policyList.loading &&
      policyList.data &&
      policyList.data.data
    ) {
      let policies_ = policyList.data.data;
      let titleValues = [];
      let descriptionValues = [];
      let idArr = [];
      policies_.map((item, index) => {
        titleValues.push(item.title);
        descriptionValues.push(item.body);
        idArr.push(item.id);
      });

      this.setState({
        titleValues: titleValues,
        descriptionValues: descriptionValues,
        policies: policies_,
        idArr: idArr
      });
    }

    this.toastTrigger("policyCreate", prevProps);
    this.toastTrigger("policyDelete", prevProps);
  };

  /*
    Toast Trigger Method
  */
  toastTrigger = (key, prevProps) => {
    const { getPolicySections, userInfo } = this.props;
    let clientId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;
    if (
      prevProps[key].loading &&
      !this.props[key].loading &&
      this.props[key].data
    ) {
      if (key === "policyCreate" || key === "policyDelete") {
        getPolicySections(clientId);
      }
      toast.success(toastMessageMap[key]);
    }
  };

  /*
    Handle Edit Mode
    @params meta_key = String
  */
  handleEditMode = (meta_key, index) => {
    switch (meta_key) {
      case "title":
        this.setState({ titleEdit: index, descriptionEdit: false });
        break;
      case "description":
        this.setState({ descriptionEdit: index, titleEdit: false });
        break;
    }
  };

  /*
    Close Edit Input callback
  */

  closeEditInput = (identifier, value) => {
    let titleValues = [...this.state.titleValues];
    let policies = [...this.state.policies];
    policies[identifier] = {
      ...policies[identifier],
      title: value,
      body: policies[identifier].body
    };
    titleValues[identifier] = value;

    this.setState({
      titleEdit: false,
      titleValues: titleValues,
      policies: policies
    });
  };

  /*
    Close Editor Component
  */
  closeEditor = (identifier, value) => {
    let policies = [...this.state.policies];
    policies[identifier] = {
      ...policies[identifier],
      title: policies[identifier].title,
      body: value
    };
    let descriptionValues = [...this.state.descriptionValues];

    descriptionValues[identifier] = value;

    this.setState({
      descriptionEdit: false,
      policies: policies,
      descriptionValues: descriptionValues
    });
  };

  /*
    Handle Description Change
  */
  handleDescChange = (value, identifier) => {
    let descriptionValues = this.state.descriptionValues;
    descriptionValues[identifier] = value;
    this.setState({ descriptionValues: descriptionValues });
  };

  /*
    Handle Title Change
  */
  handleTitleChange = (value, index) => {
    let titleValues = this.state.titleValues;
    titleValues[index] = value;
    this.setState({ titleValues: titleValues });
  };

  /*
    Add New Section
  */
  addNewSection = () => {
    const { titleValues, descriptionValues, policies } = this.state;
    let titleValues_ = [...titleValues];
    let descriptionValues_ = [...descriptionValues];
    let policies_ = [...policies];
    let newObj = newPolicyObj();
    // Push into State Copy variables;
    titleValues_.push(newObj.title);
    descriptionValues_.push(newObj.description);
    policies_.push(newObj);
    this.setState({
      titleValues: titleValues_,
      descriptionValues: descriptionValues_,
      policies: policies_
    });
  };

  /*
    Toggle Edit Hover
  */
  toggleEditHover = () => {
    this.setState({ editHover: !this.state.editHover });
  };

  /*
    Publish Clicker
    @params : none
  */
  handleClick = () => {
    const { createUpdatePolicy, userInfo } = this.props;
    const { policies } = this.state;
    let clientId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;
    createUpdatePolicy(clientId, policies);
  };

  /*
    Toggle Bottom Pane
  */
  toggleBottomPane = (index = false) => {
    console.log(index);
    if (index !== false) {
      this.setState({ showBottomPane: index });
    } else {
      this.setState({ showBottomPane: false });
    }
  };

  /*
    Delete Section
  */
  deleteSection = (identifier, sectionData) => {
    const { deletePolicyDetail, userInfo } = this.props;
    let policies_ = [...this.state.policies];
    let titleValues_ = [...this.state.titleValues];
    let descriptionValues_ = [...this.state.descriptionValues];
    let clientId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;
    // Delete Values from Specific Indexes in State Arrays
    policies_.splice(identifier, 1);
    titleValues_.splice(identifier, 1);
    descriptionValues_.splice(identifier, 1);

    // if its an Old Entry. Also Make a Delete API call.
    if (sectionData.id) {
      deletePolicyDetail(clientId, sectionData.id);
    }
    this.setState({
      policies: policies_,
      titleValues: titleValues_,
      descriptionValues: descriptionValues_
    });
  };

  render() {
    const {
      titleEdit,
      descriptionEdit,
      descriptionValues,
      titleValues,
      policies,
      showBottomPane
    } = this.state;
    const { policyList, policyCreate, policyDelete } = this.props;
    return (
      <PlatformLayout
        white
        position="relative"
        headerChildren={<PlatformHeaderChildren />}
      >
        {
          <div className={s.contentPanel}>
            <div className={s.upperPanel}>
              <span className={s.title}>Terms & Policies</span>
            </div>
            <div className={s.tabsWrapper}>
              {policyList.loading && (
                <div>
                  <DotLoader />
                </div>
              )}
              {!policyList.loading &&
                policies.map((item, index) => {
                  return (
                    <div key={`${index}_paneWrapper`} className={s.paneWrapper}>
                      <div className={s.leftPane}>
                        {titleEdit !== index && (
                          <span
                            onClick={() =>
                              item.titleEditable
                                ? this.handleEditMode("title", index)
                                : undefined
                            }
                            onMouseOver={this.toggleEditHover}
                          >
                            {item.title}
                          </span>
                        )}
                        {titleEdit === index && (
                          <EditableInput
                            value={titleValues[index]}
                            index={index}
                            onChangeHandler={this.handleTitleChange}
                            closeEditInput={this.closeEditInput}
                          />
                        )}
                      </div>
                      <div
                        className={s.rightPane}
                        onMouseEnter={() => this.toggleBottomPane(index)}
                        onMouseLeave={() => this.toggleBottomPane()}
                      >
                        {descriptionEdit !== index && (
                          <div
                            className={s.contentHolder}
                            dangerouslySetInnerHTML={{
                              __html: descriptionValues[index]
                            }}
                            onClick={() =>
                              this.handleEditMode("description", index)
                            }
                          ></div>
                        )}

                        {descriptionEdit === index && (
                          <EditorComponent
                            value={descriptionValues[index]}
                            index={index}
                            onChangeHandler={this.handleDescChange}
                            closeEditor={this.closeEditor}
                          />
                        )}
                        {showBottomPane === index && (
                          <div
                            onClick={() => this.deleteSection(index, item)}
                            className={`${s.deletePane} ${
                              descriptionEdit === index
                                ? s.editorMarginBottom
                                : s.normalMarginBottom
                            }`}
                          >
                            <span>
                              <img src={bin} />
                            </span>
                          </div>
                        )}
                      </div>
                    </div>
                  );
                })}
              <div className={s.paneWrapper}>
                <div className={s.leftPane}></div>
                <div className={s.rightPane}>
                  <span className={s.addNew} onClick={this.addNewSection}>
                    + Add Section
                  </span>
                </div>
              </div>
            </div>
            <div className={s.actionPane}>
              <div>
                {/* <Button
                  onClickHandler={this.handleClick}
                  btnType={"button"}
                  type={`transparent`}
                  noBg
                  btnText={"Preview"}
                />*/}
              </div>
              <div>
                {!policyCreate.loading && !policyDelete.loading && (
                  <Button
                    onClickHandler={this.handleClick}
                    btnType={"button"}
                    btnText={"Publish"}
                  />
                )}
                {(policyCreate.loading || policyDelete.loading) && (
                  <span className={s.loaderParent}>
                    <DotLoader />
                  </span>
                )}
              </div>
            </div>
          </div>
        }
      </PlatformLayout>
    );
  }
}

export default PolicyInfo;
