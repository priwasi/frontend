import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  createUpdatePolicy,
  getPolicySections,
  deletePolicyDetail
} from "./actions";

import PolicyInfo from "./PolicyInfo";

import { bindActionCreators } from "redux";

const mapStateToProps = (state, prevProps) => {
  return {
    policyList: state.policyReducer.policyList,
    policyCreate: state.policyReducer.policyCreate,
    policyDelete: state.policyReducer.policyDelete,
    userInfo: state.app.userInfo
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      createUpdatePolicy,
      getPolicySections,
      deletePolicyDetail
    },
    dispatch
  );
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(PolicyInfo)
);
