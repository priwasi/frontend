/*
  @Module : CredBox
  @Component : View Requests
  @Type : Action Creators
  @Description : View All Requests
  @Author : Kaustubh Saxena
*/

/*
  Action Type Constants
*/
import {
  POLICY_LIST_LOADING,
  POLICY_LIST_SUCCESS,
  POLICY_LIST_FAILURE,
  POLICY_LIST_CLEAR,
  POLICY_CREATE_LOADING,
  POLICY_CREATE_SUCCESS,
  POLICY_CREATE_FAILURE,
  POLICY_CREATE_CLEAR,
  POLICY_DELETE_LOADING,
  POLICY_DELETE_SUCCESS,
  POLICY_DELETE_FAILURE,
  POLICY_DELETE_CLEAR
} from "../../../constants/actionTypes";

/*
  Fetch Wrapper used to hit API.
*/
import httpFetch from "../../../utils/http";

/*
  Utility Imports
*/
import { getHeaders, getUrl } from "../../../utils/utilities";

/*
  @Purpose : To Fetch all the Policy Sections
  @System : CBB Backend
*/

export function getPolicySections(clientId) {
  let url = getUrl("policyList");
  let headers = getHeaders(true, true);
  headers["clientId"] = clientId;
  return {
    types: [POLICY_LIST_LOADING, POLICY_LIST_SUCCESS, POLICY_LIST_FAILURE],
    promise: () => httpFetch(url, { headers: headers })
  };
}

/*
  @Purpose : To Create and Update Policy Sections
  @System : Self
*/

export function createUpdatePolicy(clientId, data) {
  let url = getUrl("createUpdatePolicy");
  let headers = getHeaders(true, true);
  headers["clientId"] = clientId;
  return {
    types: [
      POLICY_CREATE_LOADING,
      POLICY_CREATE_SUCCESS,
      POLICY_CREATE_FAILURE
    ],
    promise: () =>
      httpFetch(url, {
        headers: headers,
        method: "POST",
        body: JSON.stringify(data)
      })
  };
}

/*
  @Purpose : To Create and Update Policy Sections
  @System : Self
*/

export function deletePolicyDetail(clientId, id) {
  let url = `${getUrl("policyList")}?id=${id}`;
  let headers = getHeaders(true, true);
  headers["clientId"] = clientId;
  return {
    types: [
      POLICY_DELETE_LOADING,
      POLICY_DELETE_SUCCESS,
      POLICY_DELETE_FAILURE
    ],
    promise: () => httpFetch(url, { headers: headers, method: "DELETE" })
  };
}
