// Action Type Constants
import {
  POLICY_LIST_LOADING,
  POLICY_LIST_SUCCESS,
  POLICY_LIST_FAILURE,
  POLICY_LIST_CLEAR,
  POLICY_CREATE_LOADING,
  POLICY_CREATE_SUCCESS,
  POLICY_CREATE_FAILURE,
  POLICY_CREATE_CLEAR,
  POLICY_DELETE_LOADING,
  POLICY_DELETE_SUCCESS,
  POLICY_DELETE_FAILURE,
  POLICY_DELETE_CLEAR
} from "../../../constants/actionTypes";

import {
  getLoadingState,
  getSuccessState,
  getErrorState
} from "../../../store/stateSettler";

const initialState = {
  policyList: {
    loading: false,
    data: null,
    error: null
  },
  policyCreate: {
    loading: false,
    data: null,
    error: null
  },
  policyDelete: {
    loading: false,
    data: null,
    error: null
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case POLICY_LIST_LOADING:
      return {
        ...state,
        policyList: getLoadingState("policyList", state)
      };
    case POLICY_LIST_SUCCESS:
      return {
        ...state,
        policyList: getSuccessState("policyList", state, action)
      };
    case POLICY_LIST_FAILURE:
      return {
        ...state,
        policyList: getErrorState("policyList", state, action)
      };
    case POLICY_LIST_CLEAR:
      return {
        ...state,
        policyList: initialState.policyList
      };
    case POLICY_CREATE_LOADING:
      return {
        ...state,
        policyCreate: getLoadingState("policyCreate", state)
      };
    case POLICY_CREATE_SUCCESS:
      return {
        ...state,
        policyCreate: getSuccessState("policyCreate", state, action)
      };
    case POLICY_CREATE_FAILURE:
      return {
        ...state,
        policyCreate: getErrorState("policyCreate", state, action)
      };
    case POLICY_CREATE_CLEAR:
      return {
        ...state,
        policyCreate: initialState.policyCreate
      };
    case POLICY_DELETE_LOADING:
      return {
        ...state,
        policyDelete: getLoadingState("policyDelete", state)
      };
    case POLICY_DELETE_SUCCESS:
      return {
        ...state,
        policyDelete: getSuccessState("policyDelete", state, action)
      };
    case POLICY_DELETE_FAILURE:
      return {
        ...state,
        policyDelete: getErrorState("policyDelete", state, action)
      };
    case POLICY_DELETE_CLEAR:
      return {
        ...state,
        policyDelete: initialState.policyDelete
      };
    default:
      return state;
  }
}
