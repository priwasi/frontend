/*
  @Module : CredBox
  @Component : Request
  @Type : Screen
  @Description : Request Listing
  @Author : Kaustubh Saxena 
*/

// React Imports
import React, { useState, Component } from "react";

// Component Imports
import PlatformLayout from "../../../Layout/PlatformLayout/PlatformLayout";
import TableRow from "../../../shared/TableRow/TableRow";
import PlatformHeaderChildren from "../../../shared/PlatformHeaderChildren";
import Modal from "../../../shared/Modal";
import Button from "../../../shared/Button/Button";

// Stylesheet Imports
import s from "./Request.module.scss";
import "../../../commonstyles/reactMultiselectCheckbox.css";

// Asset Imports
import crossIcon from "../../../public/images/crossIcon.svg";

// Node Modules Import
import ReactMultiSelectCheckboxes from "react-multiselect-checkboxes";
import Select from "react-select";
import Calendar from "react-calendar";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import queryString from "query-string";

// Utility Imports
import { DELETION, ACCESS } from "../../../utils/enum";
import { filterSchema, validationSchema, validationMessages } from "./schema";
import {
  pushFilterToQuery,
  deleteFilterFromQuery
} from "../../../utils/utilities";

import { convertToFormat } from "../../../utils/dateUtils";

import { checkForValidationError } from "../../../utils/validator";
import AccessControl, {
  noActionAccessControl
} from "../../../services/AccessControl/AccessControl";
import { permissions } from "../../../constants/permissions";

const options = [
  { label: "Deletion", value: "DELETE" },
  { label: "Access", value: "ACCESS" }
];
const userTypes = [
  { label: "Customer", value: "CUSTOMER" },
  { label: "Employee", value: "EMPLOYEE" }
];

const initializationDays = [
  { label: "0 days", value: 0 },
  { label: "1 days", value: 1 },
  { label: "2 days", value: 2 },
  { label: "3 days", value: 3 },
  { label: "4 days", value: 4 },
  { label: "5 days", value: 5 },
  { label: "6 days", value: 6 },
  { label: "7 days", value: 7 },
  { label: "9 days", value: 8 },
  { label: "9 days", value: 9 },
  { label: "10 days", value: 10 }
];

const headerSchema = [
  {
    title: "Request ID",
    config_key: "request_id",
    icon: false
  },
  {
    title: "Request Type",
    config_key: "request_type",
    icon: false
  },
  {
    title: "Date",
    config_key: "date",
    icon: false
  },
  {
    title: "Time Remaining",
    config_key: "time_remaining",
    icon: false
  },
  {
    title: "User Identifier",
    config_key: "user_identifier",
    icon: false
  },
  {
    title: "Status",
    config_key: "status",
    icon: false
  }
];

// Custom Styles for Select

const customStyles = {
  control: (styles, { isFocused }) => ({
    ...styles,
    backgroundColor: "#efeef8",
    borderColor: isFocused ? "#eaeaea" : "#eaeaea"
  }),
  option: (styles, { data, isDisabled, isFocused, isSelected }) => {
    return {
      ...styles,
      backgroundColor: isFocused ? "#efeef8" : "white",
      color: "#000",
      cursor: isDisabled ? "not-allowed" : "default",
      borderRadius: "4px"
    };
  }
};

class Request extends Component {
  state = {
    showAddRequest: false,
    showCalendar: false,
    formData: {},
    filters: {},
    validation: {}
  };

  componentDidMount = () => {
    const { getAllRequests, location } = this.props;

    // Retain Filters From URL
    let filters = location.search;
    const parsed = queryString.parse(filters);
    this.setState({ filters: { ...parsed } });
    // Get All Requests Action Creator
    noActionAccessControl(
      permissions.VIEW_ALL_REQUESTS,
      getAllRequests,
      this.props.userInfo,
      [filters]
    );
  };

  componentDidUpdate = (prevProps, prevState) => {
    const { getAllRequests, location } = this.props;
    if (
      prevProps.addRequestResponse.loading &&
      !this.props.addRequestResponse.loading &&
      this.props.addRequestResponse.data
    ) {
      toast("Request Added Successfully");

      // Get All Request After Success
      // Retain Filters From URL
      let filters = location.search;
      noActionAccessControl(
        permissions.VIEW_ALL_REQUESTS,
        getAllRequests,
        this.props.userInfo,
        [filters]
      );
      this.setState({ showAddRequest: false });
    }
  };
  /*
    Add Request Modal Toggle
    @params : none
  */

  getTableHeaders = () => {
    return (
      <div className={s.tableHeadersParent}>
        {headerSchema.map((item, index) => {
          return <span key={`${index}_header`}>{item.title}</span>;
        })}
      </div>
    );
  };

  /*
    Toggle Add Request
    @params :none
  */
  toggleAddRequest = () => {
    this.setState({ showAddRequest: !this.state.showAddRequest });
  };

  /*
    Get No Records Found for Overall Assignee
    params : none
  */
  getNoRecordsFound = () => {
    return (
      <div className={s.noAssigneeFound}>
        <span>Invite</span>
      </div>
    );
  };

  /*
    Close Add Request 
    params : none
  */
  closeAddRequest = () => {
    this.setState({ showAddRequest: false });
    window.history.back();
  };

  /*
    Show Calendar Handler
    params : none
  */
  toggleCalendar = () => {
    this.setState({ showCalendar: !this.state.showCalendar });
  };

  /*
    Manage Calendar Callback
    params : date =  Object;
  */
  manageCalendarCallback = date => {
    let formData_ = { ...this.state.formData };
    formData_["eta"] = convertToFormat(date, "yyyy-mm-dd");
    this.setState({ formData: formData_ });
    this.toggleCalendar();
  };

  /*
    Record Change For Every Field
    @params : valueObj = Object
  */
  handleChange = valueObj => {
    let formData_ = { ...this.state.formData };
    formData_[valueObj.name] = valueObj.value;
    this.validate(valueObj);
    this.setState({ formData: formData_ });
  };

  validate = obj => {
    console.log(obj);
    let validationRules = validationSchema();
    let validationMsg = validationMessages();
    let validation_ = { ...this.state.validation };
    if (validationRules[obj.name].test(obj.value)) {
      validation_[obj.name] = validationMsg[obj.name];
    }
    this.setState({ validation: validation_ });
  };

  /*
    Add Request on Button Click
    @params : none
  */
  handleClick = () => {
    const { addRequest } = this.props;
    const { formData } = this.state;
    let body = { ...formData };
    if (Object.keys(formData).length > 0) {
      addRequest(body);
    }
  };
  /*
    Render Add Request
  */
  renderAddRequest = () => {
    const { showCalendar, formData, validation } = this.state;
    return (
      <Modal
        path={"#addRequest"}
        showCloseButton
        onCloseHandler={this.closeAddRequest}
        disableOuterClick
      >
        <div className={s.formContainer}>
          <span className={s.title}>Add Request</span>
          <div className={s.inputParent}>
            <div className={s.formElemWrapper}>
              <span className={s.label}>Request Type</span>
              <Select
                name="type"
                options={options}
                styles={customStyles}
                onChange={e =>
                  this.handleChange({ name: "type", value: e.value })
                }
              />
              {validation["type"] && <span>{validation["type"]}</span>}
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>User Identifiers</span>
              <input
                type={"text"}
                name={"userIdentifier"}
                onChange={e =>
                  this.handleChange({
                    name: e.target.name,
                    value: e.target.value
                  })
                }
              />
              {validation["userIdentifier"] && (
                <span>{validation["userIdentifier"]}</span>
              )}
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>User Email</span>
              <input
                type={"text"}
                name={"emailId"}
                onChange={e =>
                  this.handleChange({
                    name: e.target.name,
                    value: e.target.value
                  })
                }
              />

              {validation["emailId"] && <span>{validation["emailId"]}</span>}
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>User Type</span>
              <Select
                options={userTypes}
                styles={customStyles}
                onChange={e =>
                  this.handleChange({ name: "userType", value: e.value })
                }
              />
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>ETA</span>
              <input
                type={"text"}
                name={"eta"}
                readOnly
                onClick={this.toggleCalendar}
                defaultValue={formData["eta"]}
              />
              <span></span>

              {showCalendar && (
                <div className={s.calendarHolder}>
                  <span className={s.closeIcon} onClick={this.toggleCalendar}>
                    <img src={crossIcon} />
                  </span>
                  <Calendar
                    onChange={this.manageCalendarCallback}
                    value={formData["eta"] && new Date(formData["eta"])}
                  />
                </div>
              )}
              {validation["eta"] && <span>{validation["eta"]}</span>}
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>Time Before Initiating Request</span>
              <Select
                options={initializationDays}
                styles={customStyles}
                onChange={e =>
                  this.handleChange({
                    name: "initializeAfterInDay",
                    value: e.value
                  })
                }
              />
            </div>
            {/* <div className={s.formElemWrapper}>
              <span className={s.label}>Overall Assignee</span>
              <input
                type={"text"}
                name={"overallAssignee"}
                onChange={e =>
                  this.handleChange({
                    name: e.target.name,
                    value: e.target.value
                  })
                }
              />
              {formData["overallAssignee"] && (
                <div className={s.identifier} onClick={() => {}}>
                  <span>
                    <span>+</span> Invite {formData["overallAssignee"]}
                  </span>
                </div>
              )}
              {validation["overallAssignee"] && (
                <span>{validation["overallAssignee"]}</span>
              )}
            </div> */}
            <div className={s.btnParent}>
              <Button
                onClickHandler={this.handleClick}
                btnType={"button"}
                btnText={"SUBMIT"}
                // isDisabled={
                //   checkForValidationError(validation) ||
                //   Object.keys(formData).length === 0
                // }
              />
            </div>
          </div>
        </div>
      </Modal>
    );
  };

  /*
    Push to Request Details
    params : request = Object
  */
  pushToRequestDetails = request => {
    const { history } = this.props;
    let type = ACCESS;
    if (request.type && request.type.toLowerCase() === DELETION) {
      type = DELETION;
    }
    let id = request.requestId;
    history.push(`/platform/${id}/${type}`);
  };

  /*
    Handle Filter Change
  */
  handleFilterChange = (e, name = null) => {
    const { history, location } = this.props;
    const { filters } = this.state;

    let filterObj = { ...filters };

    const parsed = queryString.parse(location.search);
    if (e && e.length > 0) {
      let label = e[0].label;
      let values = [];
      e.map(item => {
        values.push(item.value);
        return null;
      });

      let newFilter = values.join(",");
      pushFilterToQuery(location, history, parsed, name, newFilter);
      filterObj[name] = e;
    } else {
      deleteFilterFromQuery(location, history, parsed, name);
      delete filterObj[name];
    }
    this.setState({ filters: filterObj });
  };

  sanitizeMultiSelect = (filterValue, filterSchema) => {
    console.log(filterValue);
    // if(filterValue){

    // }
    // let filterValArr = filterValue && filterValue.split(",");
    // console.log(filterValArr);
  };

  render() {
    const { showAddRequest, filters, formData } = this.state;
    const { requests } = this.props;
    console.log(formData);
    return (
      <PlatformLayout
        white
        position="relative"
        headerChildren={<PlatformHeaderChildren />}
      >
        <div className={s.contentPanel}>
          <div className={s.upperPanel}>
            <span className={s.title}>View Requests</span>
            {/*-------------------- Filter Panel --------------------*/}
            <div className={s.filterPanel}>
              <div className={s.searchPlaceholder}>
                <input
                  type="text"
                  className={s.search}
                  placeholder={"Search Requests from request ID/User ID"}
                />
              </div>
              {/* <div className={s.selectOptionParent}>
                <span>Assignee</span>
                <ReactMultiSelectCheckboxes
                  cacheOptions
                  options={options}
                  onChange={e =>
                    this.handleFilterChange(e, filterSchema.assignee)
                  }
                />
              </div> */}
              <div className={s.selectOptionParent}>
                <span>Silos</span>
                <ReactMultiSelectCheckboxes
                  defaultValue={filterSchema.silos && filterSchema.silos}
                  options={options}
                  onChange={e => this.handleFilterChange(e, filterSchema.silos)}
                />
              </div>
              <div className={`${s.selectOptionParent} ${s.addRequest}`}>
                <span
                  className={s.addRequestBtn}
                  onClick={this.toggleAddRequest}
                >
                  + Add Request
                </span>
              </div>
            </div>
            {/*-------------------- Filter Panel --------------------*/}
          </div>
          <AccessControl fullScreen={true} meta={permissions.VIEW_ALL_REQUESTS}>
            {!requests.loading && requests.data && requests.data.data && (
              <div className={s.tabularWrapper}>
                {/*-------------------- Tabular Data --------------------*/}
                <div className={s.headerWrapper}>{this.getTableHeaders()}</div>
                <div className={s.rowWrapper}>
                  {requests.data.data &&
                    requests.data.data &&
                    requests.data.data.map((item, index) => {
                      let item_ = { ...item };
                      item_.raisedAt = convertToFormat(
                        new Date(item_.raisedAt),
                        "dd-mm-yyyy"
                      );
                      return (
                        <div
                          className={s.rowParent}
                          key={`${index}_tableParent`}
                          onClick={() => this.pushToRequestDetails(item)}
                        >
                          <TableRow
                            key={`${index}_tableRow`}
                            data={item_}
                            formatData={"status"}
                          />
                        </div>
                      );
                    })}
                </div>
                {/*-------------------- Tabular Data --------------------*/}
              </div>
            )}
          </AccessControl>
          {showAddRequest && this.renderAddRequest()}
        </div>
      </PlatformLayout>
    );
  }
}

export default Request;
