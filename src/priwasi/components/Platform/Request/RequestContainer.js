import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { getAllRequests, addRequest } from "./actions";

import Request from "./Request";

import { bindActionCreators } from "redux";

const mapStateToProps = (state, prevProps) => {
  return {
    requests: state.requestReducer.requests,
    addRequestResponse: state.requestReducer.addRequestResponse,
    userInfo: state.app.userInfo
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getAllRequests,
      addRequest
    },
    dispatch
  );
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Request)
);
