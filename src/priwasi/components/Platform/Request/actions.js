/*
  @Module : CredBox
  @Component : View Requests
  @Type : Action Creators
  @Description : View All Requests
  @Author : Kaustubh Saxena
*/

/*
  Action Type Constants
*/
import {
  REQUEST_LIST_LOADING,
  REQUEST_LIST_SUCCESS,
  REQUEST_LIST_FAILURE,
  REQUEST_LIST_CLEAR,
  ADD_REQUEST_LOADING,
  ADD_REQUEST_SUCCESS,
  ADD_REQUEST_FAILURE,
  ADD_REQUEST_CLEAR
} from "../../../constants/actionTypes";

/*
  Fetch Wrapper used to hit API.
*/
import httpFetch from "../../../utils/http";

/*
  Utility Imports
*/
import { getHeaders, getUrl } from "../../../utils/utilities";

/*
  @Purpose : To Fetch all the Requests
  @System : CBB Backend
*/

export function getAllRequests(filters = null) {
  let url = filters
    ? `${getUrl("requestAll")}${filters}`
    : getUrl("requestAll");

  return {
    types: [REQUEST_LIST_LOADING, REQUEST_LIST_SUCCESS, REQUEST_LIST_FAILURE],
    promise: () => httpFetch(url, { headers: getHeaders(true, true) })
  };
}

/*
  @Purpose : To Clear Request List from Store
  @System : Self
*/

export function clearAllRequests() {
  return {
    type: REQUEST_LIST_CLEAR,
    result: {}
  };
}

/*
  @Purpose : To Add Request
  @System : CBB Backend
*/
export function addRequest(data = null) {
  let url = getUrl("addRequest");
  return {
    types: [ADD_REQUEST_LOADING, ADD_REQUEST_SUCCESS, ADD_REQUEST_FAILURE],
    promise: () =>
      httpFetch(url, {
        headers: getHeaders(true, true),
        method: "POST",
        body: JSON.stringify(data)
      })
  };
}

// /*
//   @Purpose : To Clear Customer khata summary  List From Store
//   @System : Self
// */
