// Action Type Constants
import {
    REQUEST_LIST_LOADING,
    REQUEST_LIST_SUCCESS,
    REQUEST_LIST_FAILURE,
    REQUEST_LIST_CLEAR,
    ADD_REQUEST_LOADING,
    ADD_REQUEST_SUCCESS,
    ADD_REQUEST_FAILURE,
    ADD_REQUEST_CLEAR
} from '../../../constants/actionTypes';

import {
    getLoadingState,
    getSuccessState,
    getErrorState
} from '../../../store/stateSettler';

const initialState = {
    requests: {
        loading: false,
        data: null,
        error: null
    },
    addRequestResponse:{
        loading:false,
        data:null,
        error:null
    }
};

export default function (state = initialState, action) {
    switch (action.type) {
      case REQUEST_LIST_LOADING:
        return {
          ...state,
          requests: getLoadingState("requests", state)
        };
      case REQUEST_LIST_SUCCESS:
        return {
          ...state,
          requests: getSuccessState("requests", state, action)
        };
      case REQUEST_LIST_FAILURE:
        return {
          ...state,
          requests: getErrorState("requests", state, action)
        };
      case REQUEST_LIST_CLEAR:
        return {
          ...state,
          requests: initialState.requests
        };
      case ADD_REQUEST_LOADING:
        return {
          ...state,
            addRequestResponse: getLoadingState("addRequestResponse", state)
        };
      case ADD_REQUEST_SUCCESS:
        return {
          ...state,
          addRequestResponse: getSuccessState(
            "addRequestResponse",
            state,
            action
          )
        };
        case ADD_REQUEST_FAILURE:
        return {
          ...state,
          addRequestResponse: getErrorState("addRequestResponse", state, action)
        };
        case ADD_REQUEST_CLEAR:
        return {
          ...state,
          addRequestResponse: initialState.addRequestResponse
        };
      default:
        return state;
    }
}
