/*
    Filter Name Schema
*/
export const filterSchema = {
  assignee: "assignee",
  silos: "silos"
};

/*
    Validation Object
*/

export const validationSchema = () => {
  return {
    type: /^\s+$/,
    userIdentifier: /^\s+$/,
    eta: /^\s+$/,
    emailId: /^\s+$/,
    userType: /^\s+$/,
    initializeAfterInDay: /^\s+$/
  };
};

export const validationMessages = () => {
  return {
    type: "Please Enter a Valid Value",
    userIdentifier: "Please Enter a Valid Value",
    eta: "Please Enter a Valid Value",
    emailId: "Please Enter a Valid Email",
    userType: "Please Enter a User Type",
    initializeAfterInDay: "Please Enter A Initialization Value"
    // overallAssignee: "Please Enter a Valid Value"
  };
};
