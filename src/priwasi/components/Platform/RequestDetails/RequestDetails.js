/*
  @Module : CredBox
  @Component : Request
  @Type : Screen
  @Description : Request Listing
  @Author : Kaustubh Saxena 
*/

// React Imports
import React, { useState, Component } from "react";

// Component Imports
import PlatformLayout from "../../../Layout/PlatformLayout/PlatformLayout";
import TableRow from "../../../shared/TableRow/TableRow";
import PlatformHeaderChildren from "../../../shared/PlatformHeaderChildren";
import Modal from "../../../shared/Modal/ModalContainer";
import Select from "react-select";
import Calendar from "react-calendar";
import { convertToFormat } from "../../../utils/dateUtils";

// Stylesheet Imports
import s from "./RequestDetails.module.scss";
import "../../../commonstyles/reactMultiselectCheckbox.css";

// Asset Imports
import crossIcon from "../../../public/images/crossIcon.svg";

// Node Modules Import
import ReactMultiSelectCheckboxes from "react-multiselect-checkboxes";
import Button from "../../../shared/Button/Button";
import { toast } from "react-toastify";

const options = [
  { label: "Thing 1", value: 1 },
  { label: "Thing 2", value: 2 }
];

const tabsSchema = [
  {
    title: "Request Map",
    config_key: "request_map",
    icon: false
  },
  {
    title: "Messages",
    config_key: "messages",
    icon: false
  },
  {
    title: "Approve",
    config_key: "approve",
    icon: false
  },
  {
    title: "Audit Log",
    config_key: "audit_log",
    icon: false
  }
];
const bodyHeaderSchema = [
  {
    title: "Silos Name",
    config_key: "silos_name",
    icon: false
  },
  {
    title: "Status",
    config_key: "status",
    icon: false
  },
  {
    title: "Owner",
    config_key: "owner",
    icon: false
  },
  {
    title: "Source",
    config_key: "source",
    icon: false
  },
  {
    title: "Flow Type",
    config_key: "flow_type",
    icon: false
  },
  {
    title: "Identifier",
    config_key: "identifier",
    icon: false
  },
  {
    title: "",
    config_key: "upload",
    icon: false
  }
];

// Custom Styles for Select

const customStyles = {
  control: (styles, { isFocused }) => ({
    ...styles,
    backgroundColor: "#efeef8",
    borderColor: isFocused ? "#eaeaea" : "#eaeaea"
  }),
  option: (styles, { data, isDisabled, isFocused, isSelected }) => {
    return {
      ...styles,
      backgroundColor: isFocused ? "#efeef8" : "white",
      color: "#000",
      cursor: isDisabled ? "not-allowed" : "default",
      borderRadius: "4px"
    };
  }
};

class RequestDetails extends Component {
  state = {
    formData: {},
    currentTab: "request_map"
  };

  componentDidMount = () => {
    const { getRequestDetails } = this.props;
    const { id, type } = this.props.match.params;
    getRequestDetails(id);
  };

  componentDidUpdate = (prevProps, prevState) => {
    const { uploadFileData, getRequestDetails } = this.props;
    const { id, type } = this.props.match.params;

    if (
      prevProps.uploadFileData.loading &&
      !uploadFileData.loading &&
      uploadFileData.data
    ) {
      toast.success("File Uploaded Successfully");
      getRequestDetails(id);
    }
  };
  /*
    Add Request Modal Toggle
    @params : none
  */

  getTabs = () => {
    const { currentTab } = this.state;
    switch (currentTab) {
      case "":
        break;
    }
    return (
      <ul className={s.tabsHeader}>
        {tabsSchema.map((item, index) => {
          return (
            <li
              key={`${index}_header`}
              className={`${currentTab === item.config_key ? s.active : ""}`}
              onClick={() => this.switchTabs(item.config_key)}
            >
              {item.title}
            </li>
          );
        })}
      </ul>
    );
  };

  /*
    Handle Tab Switch Click
    @params : config_key=String
  */
  switchTabs = config_key => {
    this.setState({ currentTab: config_key });
  };

  /*
    Toggle Add Request
    @params :none
  */
  toggleAddRequest = () => {
    this.setState({ showAddRequest: !this.state.showAddRequest });
  };

  /*
    Get No Records Found for Overall Assignee
    params : none
  */
  getNoRecordsFound = () => {
    return (
      <div className={s.noAssigneeFound}>
        <span>Invite</span>
      </div>
    );
  };

  /*
    Close Add Request 
    params : none
  */
  closeAddRequest = () => {
    this.setState({ showAddRequest: false });
    window.history.back();
  };

  /*
    Show Calendar Handler
    params : none
  */
  toggleCalendar = () => {
    this.setState({ showCalendar: !this.state.showCalendar });
  };

  /*
    Manage Calendar Callback
    params : date =  Object;
  */
  manageCalendarCallback = date => {
    let formData = { ...this.state.formData };
    formData["eta"] = convertToFormat(date, "yyyy-mm-dd");
    this.setState({ formData: formData });
    this.toggleCalendar();
  };

  /*
    Push to Request Details
    params : request = Object
  */
  pushToRequestDetails = request => {
    const { history } = this.props;
    let id = request.request_id && request.request_id.split(" ").join("");
    history.push(`/platform/${id}/access`);
  };

  /*
    Get Tab Body
    @params : none
  */
  getTabBody = () => {
    const { currentTab } = this.state;
    switch (currentTab) {
      case "request_map":
        return this.getRequestMapContent();
      default:
        return this.getRequestMapContent();
    }
  };

  onUpload = (e, data) => {
    const { uploadFile } = this.props;
    let formData = new FormData();
    formData.append("file", e.target.files[0]);
    let requestData = {
      file: formData,
      requestId: data.id,
      dsId: data.dsId
    };
    uploadFile(requestData, formData);
  };

  getRequestMapContent = () => {
    const { requestDetails } = this.props;
    let tableData =
      requestDetails.data &&
      requestDetails.data.data &&
      requestDetails.data.data.dsDetails &&
      requestDetails.data.data.dsDetails;
    return (
      <div className={s.tabsBody}>
        <div className={s.bodyHeaders}>
          {bodyHeaderSchema.map((item, index) => {
            return <span key={`${index}_header`}>{item.title}</span>;
          })}
        </div>
        <div className={s.rowWrapper}>
          {tableData &&
            tableData.map((item, index) => {
              return (
                <div className={s.rowParent} key={`${index}_tableParent`}>
                  <TableRow
                    key={`${index}_tableRow`}
                    data={item}
                    type={"requestMap"}
                    onUpload={this.onUpload}
                  />
                </div>
              );
            })}
        </div>
      </div>
    );
  };

  render() {
    const { requestDetails } = this.props;
    const { id, type } = this.props.match.params;
    let requestDetailData =
      requestDetails.data &&
      requestDetails.data.data &&
      requestDetails.data.data.dsDetails &&
      requestDetails.data.data.dsDetails;
    let overallReqData =
      requestDetails.data &&
      requestDetails.data.data &&
      requestDetails.data.data;
    // temp
    let firstRow =
      requestDetails.data &&
      requestDetails.data.data &&
      requestDetails.data.data.silos &&
      requestDetails.data.data.silos.length > 0 &&
      requestDetails.data.data.silos[0] &&
      requestDetails.data.data.silos[0].identifier &&
      requestDetails.data.data.silos[0].identifier;
    return (
      <PlatformLayout
        white
        position="relative"
        headerChildren={<PlatformHeaderChildren />}
      >
        <div className={s.contentPanel}>
          <div className={s.upperPanel}>
            <span className={s.title}>
              <span className={s.greyTitle}>Request ID:</span> {id}
              {type === "access" && (
                <span className={s.accessRequestType}>{type}</span>
              )}
              {type === "delete" && (
                <span className={s.deleteRequestType}>{type}</span>
              )}
            </span>
            {/*-------------------- Filter Panel --------------------*/}
            <div className={s.filterPanel}>
              <div className={s.requestInfo}>
                <div>
                  <span>
                    {overallReqData &&
                      overallReqData.status &&
                      overallReqData.status}
                  </span>
                  <span>
                    Raised at{" "}
                    {overallReqData &&
                      overallReqData.raisedAt &&
                      convertToFormat(new Date(overallReqData.raisedAt))}
                  </span>

                  <span>
                    {overallReqData &&
                      overallReqData.daysLeft &&
                      `${overallReqData.daysLeft} Left`}
                  </span>
                </div>
                <progress value="50" max="100"></progress>
              </div>
              <div className={s.selectOptionParent}>
                <span>Core Identifier:</span>
                <span className={s.identifier}>{firstRow && firstRow}</span>
              </div>
              <div className={s.selectOptionParent}>
                {/* <span>Assignee</span>
                <span className={s.selectDropdown}>All</span> */}
                <span>Assignee</span>
                <ReactMultiSelectCheckboxes options={options} />
              </div>
              <div className={s.selectOptionParent}>
                <span>Silos</span>
                <ReactMultiSelectCheckboxes options={options} />
              </div>
            </div>
            {/*-------------------- Filter Panel --------------------*/}
          </div>
          <div className={s.tabsWrapper}>
            {/*-------------------- Tabs Data --------------------*/}
            <div className={s.headerWrapper}>{this.getTabs()}</div>
            {this.getTabBody()}
            {/*-------------------- Tabs Data --------------------*/}
          </div>
        </div>
      </PlatformLayout>
    );
  }
}

export default RequestDetails;
