import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { getRequestDetails, uploadFile } from "./actions";

import RequestDetails from "./RequestDetails";

import { bindActionCreators } from "redux";

const mapStateToProps = (state, prevProps) => {
  return {
    requestDetails: state.requestDetailsReducer.requestDetails,
    uploadFileData: state.requestDetailsReducer.uploadFileData
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getRequestDetails,
      uploadFile
    },
    dispatch
  );
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(RequestDetails)
);
