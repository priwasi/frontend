/*
  @Module : CredBox
  @Component : View Request Detail
  @Type : Action Creators
  @Description : View Details Of Requests
  @Author : Kaustubh Saxena
*/

/*
  Action Type Constants
*/
import {
  REQUEST_DETAIL_LOADING,
  REQUEST_DETAIL_SUCCESS,
  REQUEST_DETAIL_FAILURE,
  REQUEST_DETAIL_CLEAR,
  UPLOAD_FILE_LOADING,
  UPLOAD_FILE_SUCCESS,
  UPLOAD_FILE_FAILURE,
  UPLOAD_FILE_CLEAR
} from "../../../constants/actionTypes";

/*
  Fetch Wrapper used to hit API.
*/
import httpFetch from "../../../utils/http";

/*
  Utility Imports
*/
import { getHeaders, getUrl } from "../../../utils/utilities";

/*
  @Purpose : To Fetch all the Requests
  @System : CBB Backend
*/

export function getRequestDetails(requestId, filters = null) {
  let url = filters
    ? `${getUrl("requestDetails")}?id=${requestId}${filters}`
    : `${getUrl("requestDetails")}?id=${requestId}`;

  return {
    types: [
      REQUEST_DETAIL_LOADING,
      REQUEST_DETAIL_SUCCESS,
      REQUEST_DETAIL_FAILURE
    ],
    promise: () => httpFetch(url, { headers: getHeaders(true, true) })
  };
}

/*
  @Purpose : To Clear Request List from Store
  @System : Self
*/

export function clearAllRequests() {
  return {
    type: REQUEST_DETAIL_CLEAR,
    result: {}
  };
}

/*
  @Purpose : To Upload the File
  @System : CBB Backend
*/

export function uploadFile(data, formData) {
  console.log(formData);
  let url = `${getUrl("uploadFile")}?dsId=${data.dsId}&requestId=${
    data.requestId
  }`;
  let headers = getHeaders(true, true);
  headers["Content-Type"] = "multipart/form-data";
  return {
    types: [UPLOAD_FILE_LOADING, UPLOAD_FILE_SUCCESS, UPLOAD_FILE_FAILURE],
    promise: () =>
      httpFetch(url, { method: "POST", headers: headers, body: formData })
  };
}
