// Action Type Constants
import {
  REQUEST_DETAIL_LOADING,
  REQUEST_DETAIL_SUCCESS,
  REQUEST_DETAIL_FAILURE,
  REQUEST_DETAIL_CLEAR,
  UPLOAD_FILE_LOADING,
  UPLOAD_FILE_SUCCESS,
  UPLOAD_FILE_FAILURE,
  UPLOAD_FILE_CLEAR
} from "../../../constants/actionTypes";

import {
  getLoadingState,
  getSuccessState,
  getErrorState
} from "../../../store/stateSettler";

const initialState = {
  requestDetails: {
    loading: false,
    data: null,
    error: null
  },
  uploadFileData: {
    loading: false,
    data: null,
    error: null
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case REQUEST_DETAIL_LOADING:
      return {
        ...state,
        requestDetails: getLoadingState("requestDetails", state)
      };
    case REQUEST_DETAIL_SUCCESS:
      return {
        ...state,
        requestDetails: getSuccessState("requestDetails", state, action)
      };
    case REQUEST_DETAIL_FAILURE:
      return {
        ...state,
        requestDetails: getErrorState("requestDetails", state, action)
      };
    case REQUEST_DETAIL_CLEAR:
      return {
        ...state,
        requestDetails: initialState.requestDetails
      };
    case UPLOAD_FILE_LOADING:
      return {
        ...state,
        uploadFileData: getLoadingState("uploadFileData", state)
      };
    case UPLOAD_FILE_SUCCESS:
      return {
        ...state,
        uploadFileData: getSuccessState("uploadFileData", state, action)
      };
    case UPLOAD_FILE_FAILURE:
      return {
        ...state,
        uploadFileData: getErrorState("uploadFileData", state, action)
      };
    case UPLOAD_FILE_CLEAR:
      return {
        ...state,
        uploadFileData: initialState.uploadFileData
      };
    default:
      return state;
  }
}
