/*
  @Module : CredBox
  @Component : Request Preferences
  @Type : Screen
  @Description : Request Preferences
  @Author : Kaustubh Saxena 
*/

// React Imports
import React, { useState, Component } from "react";

// Component Imports
import PlatformLayout from "../../../Layout/PlatformLayout/PlatformLayout";
import PlatformHeaderChildren from "../../../shared/PlatformHeaderChildren";
import Modal from "../../../shared/Modal/ModalContainer";
import Select from "react-select";
import Tabs from "../../../shared/Tabs/Tabs";

// Stylesheet Imports
import s from "./RequestPreferences.module.scss";
import "../../../commonstyles/reactMultiselectCheckbox.css";

// Asset & Utility Imports
import crossIcon from "../../../public/images/crossIcon.svg";
import { preferences } from "./schema";

// Node Modules Import
import Button from "../../../shared/Button/Button";
import Checkbox from "../../../shared/Checkbox/Checkbox";
import { toast } from "react-toastify";

// Custom Styles for Select

const customStyles = {
  control: (styles, { isFocused }) => ({
    ...styles,
    backgroundColor: "#efeef8",
    borderColor: isFocused ? "#eaeaea" : "#eaeaea"
  }),
  option: (styles, { data, isDisabled, isFocused, isSelected }) => {
    return {
      ...styles,
      backgroundColor: isFocused ? "#efeef8" : "white",
      color: "#000",
      cursor: isDisabled ? "not-allowed" : "default",
      borderRadius: "4px"
    };
  }
};

class RequestPreferences extends Component {
  state = {
    showAddSilos: false,
    showRequestSubmitForm: false,
    formData: {},
    silosList: [],
    dataFields: [],
    selectedDataFields: {},
    currentSelected: null
  };

  componentDidMount = () => {
    const { getSilosSettings } = this.props;
    getSilosSettings();
  };

  componentDidUpdate = (prevProps, prevState) => {
    const { dsFields, saveDsFields } = this.props;
    if (prevProps.dsFields.loading && !dsFields.loading && dsFields.data) {
      let external = dsFields.data.data.external;
      let internal = dsFields.data.data.internal;
      let formData_ = {};
      let external_options = external.map((item, index) => {
        formData_[item.silosId] = item.fields;
        return {
          label: item.silosName,
          value: item.silosId,
          group: "external"
        };
      });
      let internal_options = internal.map((item, index) => {
        formData_[item.silosId] = item.fields;
        return {
          label: item.silosName,
          value: item.silosId,
          group: "internal"
        };
      });

      // Sanitize Options To Groups
      const options = [
        {
          label: "external",
          options: external_options
        },
        {
          label: "internal",
          options: internal_options
        }
      ];
      this.setState({ silosList: options, formData: formData_ });
    }

    if (
      prevProps.saveDsFields.loading &&
      !saveDsFields.loading &&
      saveDsFields.data
    ) {
      toast.success("Control Panel Checklist Updated Successfully");
    }
  };

  recordCheckboxData = (e, type) => {
    const { currentSelected } = this.state;
    let formData = { ...this.state.formData };
    formData[currentSelected][type].push(e.target.name);
    this.setState({ formData: formData });
  };

  /*
    Handle Click
    @params : none
  */
  handleClick = () => {
    const { formData, currentSelected } = this.state;
    const { saveSilosSettings } = this.props;
    if (currentSelected) {
      let body = {
        silosId: currentSelected,
        fields: formData[currentSelected]
      };
      saveSilosSettings(body);
    }
  };
  getAccessContent = () => {
    const { formData, dataFields, currentSelected } = this.state;
    return (
      <div className={s.tab}>
        <div className={s.prefHeader}>
          <span>Data Fields</span>
          <span>Access</span>
        </div>
        <div className={s.prefBody}>
          {dataFields &&
            dataFields.map((item, index) => {
              return (
                <div
                  className={s.prefItemWrapper}
                  key={`${index}_pref_item_wrapper`}
                >
                  <span className={s.prefItem} key={`${index}_data_cat`}>
                    {item}
                  </span>
                  <span className={s.prefItem} key={`${index}_access`}>
                    <Checkbox
                      name={`${item}`}
                      handleChange={e => this.recordCheckboxData(e, "ACCESS")}
                      checked={formData[currentSelected].ACCESS.includes(item)}
                    />
                  </span>
                </div>
              );
            })}
        </div>
        <div className={s.btnParent}>
          <Button
            onClickHandler={this.handleClick}
            btnType={"button"}
            btnText={"APPLY"}
          />
        </div>
      </div>
    );
  };
  getDeleteContent = () => {
    const {
      formData,
      dataFields,
      selectedDataFields,
      currentSelected
    } = this.state;
    return (
      <div className={s.tab}>
        <div className={s.prefHeader}>
          <span>Data Fields</span>
          <span>Erasure</span>
        </div>
        <div className={s.prefBody}>
          {dataFields &&
            dataFields.map((item, index) => {
              return (
                <div
                  className={s.prefItemWrapper}
                  key={`${index}_pref_item_wrapper`}
                >
                  <span className={s.prefItem} key={`${index}_data_cat`}>
                    {item}
                  </span>
                  <span className={s.prefItem} key={`${index}_delete`}>
                    <Checkbox
                      name={`${item}`}
                      handleChange={e => this.recordCheckboxData(e, "DELETE")}
                      checked={formData[currentSelected].DELETE.includes(item)}
                    />
                  </span>
                </div>
              );
            })}
        </div>
        <div className={s.btnParent}>
          <Button
            onClickHandler={this.handleClick}
            btnType={"button"}
            btnText={"APPLY"}
          />
        </div>
      </div>
    );
  };

  handleChange = e => {
    const { dsFields } = this.props;
    if (dsFields.data && dsFields.data.data) {
      let dsfieldData = dsFields.data && dsFields.data.data;
      let dataFields = [];
      let currentSelected = null;
      if (e.group === "external") {
        let externalDataFields = dsfieldData.external.map((item, index) => {
          currentSelected = item.silosId;
          dataFields = [...item.allFields];
          return item.fields;
        });
        this.setState({
          selectedDataFields: externalDataFields,
          dataFields: dataFields,
          currentSelected: currentSelected
        });
      } else {
        let internalDataFields = dsfieldData.internal.map((item, index) => {
          currentSelected = item.silosId;
          dataFields = [...item.allFields];
          return item.fields;
        });
        this.setState({
          selectedDataFields: internalDataFields,
          dataFields: dataFields,
          currentSelected: currentSelected
        });
      }
    }
  };

  render() {
    const { formData, currentSelected } = this.state;
    const { dsFields } = this.props;
    return (
      <PlatformLayout
        white
        position="relative"
        headerChildren={<PlatformHeaderChildren />}
      >
        <div className={s.contentPanel}>
          <div className={s.upperPanel}>
            <span className={s.title}>Requests Preferences</span>
          </div>
          <div className={s.preferenceContainer}>
            <div className={s.silosParent}>
              <Select
                options={this.state.silosList}
                onChange={this.handleChange}
              />
            </div>
            {!dsFields.loading && dsFields.data && dsFields.data.data && (
              <Tabs
                data={[
                  {
                    config_key: "access",
                    content: this.getAccessContent(),
                    title: "Access"
                  },
                  {
                    config_key: "delete",
                    content: this.getDeleteContent(),
                    title: "Delete"
                  }
                ]}
              />
            )}
            {!currentSelected && (
              <div className={s.notice}>
                <span>Please Select Data Source from above dropdown.</span>
              </div>
            )}
          </div>
        </div>
      </PlatformLayout>
    );
  }
}

export default RequestPreferences;
