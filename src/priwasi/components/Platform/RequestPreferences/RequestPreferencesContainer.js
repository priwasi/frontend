import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { getSilosSettings, saveSilosSettings } from "./actions";

import RequestPreferences from "./RequestPreferences";

import { bindActionCreators } from "redux";

const mapStateToProps = (state, prevProps) => {
  return {
    dsFields: state.controlPanelReducer.dsFields,
    saveDsFields: state.controlPanelReducer.saveDsFields
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getSilosSettings,
      saveSilosSettings
    },
    dispatch
  );
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(RequestPreferences)
);
