/*
  @Module : CredBox
  @Component : View Request Detail
  @Type : Action Creators
  @Description : View Details Of Requests
  @Author : Kaustubh Saxena
*/

/*
  Action Type Constants
*/
import {
  DS_FIELDS_LOADING,
  DS_FIELDS_SUCCESS,
  DS_FIELDS_FAILURE,
  DS_FIELDS_CLEAR,
  SAVE_DS_FIELDS_LOADING,
  SAVE_DS_FIELDS_SUCCESS,
  SAVE_DS_FIELDS_FAILURE,
  SAVE_DS_FIELDS_CLEAR
} from "../../../constants/actionTypes";

/*
  Fetch Wrapper used to hit API.
*/
import httpFetch from "../../../utils/http";

/*
  Utility Imports
*/
import { getHeaders, getUrl } from "../../../utils/utilities";

/*
  @Purpose : To Fetch all the Requests
  @System : CBB Backend
*/

export function getSilosSettings() {
  let url = `${getUrl("controlPanel")}`;

  return {
    types: [DS_FIELDS_LOADING, DS_FIELDS_SUCCESS, DS_FIELDS_FAILURE],
    promise: () => httpFetch(url, { headers: getHeaders(true, true) })
  };
}

/*
  @Purpose : To Clear Request List from Store
  @System : Self
*/

export function clearSilosSettings() {
  return {
    type: DS_FIELDS_CLEAR,
    result: {}
  };
}

/*
  @Purpose : To Save Control Panel Preferences
  @System : CBB Backend
*/
export function saveSilosSettings(data) {
  let url = `${getUrl("controlPanel")}`;

  return {
    types: [
      SAVE_DS_FIELDS_LOADING,
      SAVE_DS_FIELDS_SUCCESS,
      SAVE_DS_FIELDS_FAILURE
    ],
    promise: () =>
      httpFetch(url, {
        headers: getHeaders(true, true),
        method: "POST",
        body: data
      })
  };
}

/*
  @Purpose : To Clear Request List from Store
  @System : Self
*/

export function clearSaveSilosSettings() {
  return {
    type: SAVE_DS_FIELDS_CLEAR,
    result: {}
  };
}
