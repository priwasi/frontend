// Action Type Constants
import {
  DS_FIELDS_LOADING,
  DS_FIELDS_SUCCESS,
  DS_FIELDS_FAILURE,
  DS_FIELDS_CLEAR,
  SAVE_DS_FIELDS_LOADING,
  SAVE_DS_FIELDS_SUCCESS,
  SAVE_DS_FIELDS_FAILURE,
  SAVE_DS_FIELDS_CLEAR
} from "../../../constants/actionTypes";

import {
  getLoadingState,
  getSuccessState,
  getErrorState
} from "../../../store/stateSettler";

const initialState = {
  dsFields: {
    loading: false,
    data: null,
    error: null
  },
  saveDsFields: {
    loading: false,
    data: null,
    error: null
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case DS_FIELDS_LOADING:
      return {
        ...state,
        dsFields: getLoadingState("dsFields", state)
      };
    case DS_FIELDS_SUCCESS:
      return {
        ...state,
        dsFields: getSuccessState("dsFields", state, action)
      };
    case DS_FIELDS_FAILURE:
      return {
        ...state,
        dsFields: getErrorState("dsFields", state, action)
      };
    case DS_FIELDS_CLEAR:
      return {
        ...state,
        dsFields: initialState.dsFields
      };
    case SAVE_DS_FIELDS_LOADING:
      return {
        ...state,
        saveDsFields: getLoadingState("saveDsFields", state)
      };
    case SAVE_DS_FIELDS_SUCCESS:
      return {
        ...state,
        saveDsFields: getSuccessState("saveDsFields", state, action)
      };
    case SAVE_DS_FIELDS_FAILURE:
      return {
        ...state,
        saveDsFields: getErrorState("saveDsFields", state, action)
      };
    case SAVE_DS_FIELDS_CLEAR:
      return {
        ...state,
        saveDsFields: initialState.saveDsFields
      };

    default:
      return state;
  }
}
