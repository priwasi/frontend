export const preferences = () => {
  return [
    {
      id: 1,
      data_category: "Address Details",
      erasure: true,
      access: false,
      silos_name: "User Profile",
      owner: "John Wayne"
    },
    {
      id: 2,
      data_category: "Full Name",
      erasure: true,
      access: true,
      silos_name: "User Profile",
      owner: "Nick Davis"
    },
    {
      id: 3,
      data_category: "Communication",
      erasure: false,
      access: true,
      silos_name: "Slack",
      owner: "Craig Marshall"
    },
    {
      id: 4,
      data_category: "Payment Details",
      erasure: false,
      access: false,
      silos_name: "Stripe",
      owner: "James Dorris"
    }
  ];
};
