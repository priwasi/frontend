/*
  @Module : CredBox
  @Component : Request
  @Type : Screen
  @Description : Request Listing
  @Author : Kaustubh Saxena 
*/

// React Imports
import React, { Component, Fragment } from "react";

// Component Imports
import PlatformLayout from "../../../Layout/PlatformLayout/PlatformLayout";
import TableRow from "../../../shared/TableRow/TableRow";
import PlatformHeaderChildren from "../../../shared/PlatformHeaderChildren";
import Modal from "../../../shared/Modal/ModalContainer";
import Select from "react-select";
import TitleThumb from "../../../shared/TitleThumb/TitleThumb";
import Button from "../../../shared/Button/Button";
import AccessControl, {
  noActionAccessControl
} from "../../../services/AccessControl/AccessControl";

// Stylesheet Imports
import s from "./Silos.module.scss";
import "../../../commonstyles/reactMultiselectCheckbox.css";

// Asset & Utility Imports
import { silosList } from "./schema";
import { permissions } from "../../../constants/permissions";
import StepProgressBar from "../../../shared/StepProgressBar";

// Node Modules Import
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import DotLoader from "../../../shared/DotLoader";
import { FaTrash } from "react-icons/fa";

// Custom Styles for Select
const customStyles = {
  control: (styles, { isFocused }) => ({
    ...styles,
    backgroundColor: "#efeef8",
    borderColor: isFocused ? "#eaeaea" : "#eaeaea"
  }),
  option: (styles, { data, isDisabled, isFocused, isSelected }) => {
    return {
      ...styles,
      backgroundColor: isFocused ? "#efeef8" : "white",
      color: "#000",
      cursor: isDisabled ? "not-allowed" : "default",
      borderRadius: "4px"
    };
  }
};

const flowTypeOptions = [
  { label: "Manual", value: "MANUAL" },
  { label: "Webhook", value: "WEBHOOK" }
];
const dsTypeOptions = [
  { label: "Internal", value: "internal" },
  { label: "External", value: "external" }
];
const methodOptions = [
  { label: "GET", value: "GET" },
  { label: "POST", value: "POST" },
  { label: "PUT", value: "PUT" },
  { label: "DELETE", value: "DELETE" }
];
const userTypeOptions = [
  { label: "Customer", value: "CUSTOMER" },
  { label: "Employee", value: "EMPLOYEE" }
];

const toastMessageMap = {
  updateSilosData: "Silos Updated SUccessfully",
  silosCreate: "Data Source Created Successfully",
  deleteSilosData: "Data Source Deleted Successfully"
};

class Silos extends Component {
  state = {
    showAddSilos: false,
    showRequestSubmitForm: false,
    formData: {
      access_processType: "WEBHOOK",
      delete_processType: "WEBHOOK",
      type: "webhook"
    },
    accessRequest: {},
    deleteRequest: {},
    idMapping: {},
    showBasicInfo: true, //,
    currentPane: "basic_info",
    editSilo: false
  };

  componentDidMount = () => {
    const { getSilos, userInfo } = this.props;
    let clientId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;
    getSilos(clientId);
  };

  componentDidUpdate = (prevProps, prevState) => {
    const { formData, showRequestSubmitForm } = this.state;
    // Toggle Request Submit Form
    if (
      (!prevState.formData.type || prevState.formData.type === "manual") &&
      formData.type === "webhook"
    ) {
      this.setState({ showRequestSubmitForm: true });
    } else if (
      (!prevState.formData.type || prevState.formData.type === "webhook") &&
      showRequestSubmitForm &&
      formData.type === "manual"
    ) {
      this.setState({ showRequestSubmitForm: false });
    }

    this.toastTrigger("silosCreate", prevProps);
    this.toastTrigger("updateSilosData", prevProps);
    this.toastTrigger("deleteSilosData", prevProps);
  };

  toastTrigger = (key, prevProps) => {
    const { userInfo, getSilos } = this.props;

    let clientId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;

    if (
      prevProps[key].loading &&
      !this.props[key].loading &&
      this.props[key].data
    ) {
      if (key === "updateSilosData" || key === "deleteSilosData") {
        getSilos(clientId);
        this.closeEditSilos();
      }
      if (key === "silosCreate") {
        getSilos(clientId);

        this.setState({
          showAddSilos: false,
          formData: {},
          currentPane: "basic_info",
          idMapping: {}
        });
      }

      toast.success(toastMessageMap[key]);
    }
  };

  /*
    Toggle Add Silos
  */
  toggleAddSilos = () => {
    this.setState({ showAddSilos: !this.state.showAddSilos });
  };

  /*
    Create Silos
  */
  createSilos = () => {
    const { formData } = this.state;
    const { createSilos, userInfo, updateSilos } = this.props;
    let clientId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;
    let assignee = formData.assignee && formData.assignee.split("&");
    let owner = formData.owner && formData.owner.split("&");

    let obj = {
      silosInfo: [
        {
          name: formData.name ? formData.name : null,
          // type: formData.type ? formData.type.toUpperCase : null,
          manualInfo: {
            ownerName: owner && owner.length > 0 ? owner[0] : null,
            ownerEmailId: owner && owner.length > 0 ? owner[1] : null
          },
          dsType: formData.dsType ? formData.dsType : null,
          assignee: {
            name: assignee && assignee.length > 0 ? assignee[0] : null,
            emailId: assignee && assignee.length > 0 ? assignee[1] : null
          },
          userTypes: formData.userType ? formData.userType : [],
          storedDataType: [],
          dataAccess: {
            access: {
              url: formData.access_url ? formData.access_url : null,
              method: formData.access_method ? formData.access_method : null,
              dataSourceType: formData.access_processType
                ? formData.access_processType.toUpperCase()
                : null,
              headers: formData.access_headers
                ? JSON.parse(formData.access_headers)
                : null
            },
            delete: {
              url: formData.delete_url ? formData.delete_url : null,
              method: formData.delete_method ? formData.delete_method : null,
              dataSourceType: formData.delete_processType
                ? formData.delete_processType.toUpperCase()
                : null,
              headers: formData.delete_headers
                ? JSON.parse(formData.delete_headers)
                : null
            }
          },
          idMapping: {
            sameAsAuth: formData.oauthId ? formData.oauthId : false,
            url: formData.idmap_url ? formData.id_url : null,
            method: formData.id_method
              ? formData.id_method.toUpperCase()
              : null,
            headers: formData.idmap_header
              ? JSON.parse(formData.idmap_header)
              : null
          }
        }
      ]
    };

    createSilos(obj, clientId);
  };

  handleChange = (value, name) => {
    let formData_ = { ...this.state.formData };
    if (Array.isArray(value)) {
      let value_arr = value.map(item => {
        return item.value;
      });
      formData_[name] = value_arr;
    } else {
      if (value && value.value) {
        formData_[name] = value.value;
      } else {
        delete formData_[name];
      }
    }
    console.log(formData_);
    this.setState({ formData: formData_ });
  };

  recordInputData = fieldObj => {
    let formData_ = { ...this.state.formData };
    formData_[fieldObj.name] = fieldObj.value;
    this.setState({ formData: formData_ });
  };

  recordCheckboxData = e => {
    let formData = { ...this.state.formData };
    formData[e.target.name] = e.target.checked;
    this.setState({ formData: formData });
  };

  toggleBasicInfo = () => {
    this.setState({ showBasicInfo: !this.state.showBasicInfo });
  };

  /*
    Render Form
  */
  changePane = type => {
    switch (type) {
      case "basic_info":
        this.setState({ currentPane: "basic_info" });
        break;
      case "access_request":
        this.setState({ currentPane: "access_request" });
        break;
      case "delete_request":
        this.setState({ currentPane: "delete_request" });
        break;
      default:
        break;
    }
  };

  /*
    Get Default Select2 Value
  */

  getDefaultValue = (currentValHash, optionList, type) => {
    switch (type) {
      case "owner":
        if (currentValHash && currentValHash.manualInfo) {
          let selectedOption = optionList.filter(item => {
            return (
              item.value ===
              `${currentValHash.manualInfo.ownerName}&${currentValHash.manualInfo.ownerEmailId}`
            );
          });
          return selectedOption.length > 0 && selectedOption[0];
        }
        return false;
      case "userType":
        if (currentValHash.userTypes) {
          let selectedOption = optionList.filter(item => {
            return currentValHash.userTypes.includes(item.value);
          });
          return selectedOption.length > 0 && selectedOption;
        }
        return false;
      case "assignee":
        if (currentValHash && currentValHash.manualInfo) {
          let selectedOption = optionList.filter(item => {
            return (
              item.value ===
              `${currentValHash.assignee.name}&${currentValHash.assignee.emailId}`
            );
          });
          return selectedOption.length > 0 && selectedOption[0];
        }
      default:
        return false;
    }
  };

  updateSilos = () => {
    const { formData } = this.state;
    const { updateSilos, userInfo } = this.props;
    let clientId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;
    let assignee = formData.assignee && formData.assignee.split("&");
    let owner = formData.owner && formData.owner.split("&");

    let obj = {
      silosInfo: [
        {
          name: formData.silosName ? formData.silosName : null,
          // type: formData.type ? formData.type.toUpperCase : null,
          manualInfo: {
            ownerName: formData.ownerEmail ? formData.ownerEmail > 0 : null,
            ownerEmailId: owner && owner.length > 0 ? owner[1] : null
          },
          dsType: formData.dsType ? formData.dsType : null,
          assignee: {
            name: assignee && assignee.length > 0 ? assignee[0] : null,
            emailId: assignee && assignee.length > 0 ? assignee[1] : null
          },
          userTypes: formData.userType ? formData.userType : [],
          storedDataType: [],
          dataAccess: {
            access: {
              url: formData.access_url ? formData.access_url : null,
              method: formData.access_method ? formData.access_method : null,
              dataSourceType: formData.access_processType
                ? formData.access_processType.toUpperCase()
                : null,
              headers: formData.access_headers
                ? JSON.parse(formData.access_headers)
                : null
            },
            delete: {
              url: formData.delete_url ? formData.delete_url : null,
              method: formData.delete_method ? formData.delete_method : null,
              dataSourceType: formData.delete_processType
                ? formData.delete_processType.toUpperCase()
                : null,
              headers: formData.delete_headers
                ? JSON.parse(formData.delete_headers)
                : null
            }
          },
          idMapping: {
            sameAsAuth: formData.oauthId ? formData.oauthId : false,
            url: formData.idmap_url ? formData.id_url : null,
            method: formData.id_method
              ? formData.id_method.toUpperCase()
              : null,
            headers: formData.idmap_header
              ? JSON.parse(formData.idmap_header)
              : null
          },
          silosId: formData.silosId
        }
      ]
    };
    updateSilos(obj, clientId);
  };

  /*
    Render Edit Silos
  */

  renderEditSilos = () => {
    const { editSilo, formData } = this.state;
    const { assignees } = this.props;
    let assigneesList =
      assignees.data && assignees.data.data && assignees.data.data;
    const assigneesList_ =
      assigneesList &&
      assigneesList.map((item, index) => {
        let result = {
          label: `${item.name ? item.name : "-"} (${item.emailId})`,
          value: `${item.name}&${item.emailId}`
        };
        return result;
      });

    return (
      <Modal
        path={"#editSilos"}
        showCloseButton
        onCloseHandler={this.closeEditSilos}
        disableOuterClick
        customStyle={{ width: "768px", height: "600px" }}
      >
        <div className={s.formContainer}>
          <div className={s.inputParent}>
            <span className={s.title}>Edit Silos</span>
            {/* <div className={s.formElemWrapper}>
              <span className={s.label}>Flow Type</span>
              <select
                className={s.select}
                defaultValue={editSilo.type ? editSilo.type.toLowerCase() : ""}
                name={"type"}
                onChange={e =>
                  this.recordInputData({
                    name: e.target.name,
                    value: e.target.value
                  })
                }
              >
                {flowTypeOptions.map((item, index) => {
                  return <option value={item.value}>{item.label}</option>;
                })}
              </select>
            </div> */}
            <div className={s.formElemWrapper}>
              <span className={s.label}>DS Type</span>
              <select
                className={s.select}
                defaultValue={
                  editSilo["dsType"]
                    ? editSilo["dsType"]
                    : dsTypeOptions[0].value
                }
                name={"dsType"}
                onChange={e =>
                  this.recordInputData({
                    name: e.target.name,
                    value: e.target.value
                  })
                }
              >
                {dsTypeOptions.map((item, index) => {
                  return <option value={item.value}>{item.label}</option>;
                })}
              </select>
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>Silos Name</span>
              <input
                type={"text"}
                name={"silosName"}
                onChange={e =>
                  this.recordInputData({
                    name: e.target.name,
                    value: e.target.value
                  })
                }
                defaultValue={editSilo.name ? editSilo.name : null}
              />
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>Owner Email</span>
              <Select
                options={assigneesList_}
                styles={customStyles}
                onChange={e => this.handleChange(e, "owner")}
                defaultValue={this.getDefaultValue(
                  editSilo,
                  assigneesList_,
                  "owner"
                )}
              />
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>User Type</span>
              <Select
                options={userTypeOptions}
                styles={customStyles}
                onChange={e => this.handleChange(e, "userType")}
                isMulti
                defaultValue={this.getDefaultValue(
                  editSilo,
                  userTypeOptions,
                  "userType"
                )}
              />
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>Assignee Email</span>
              <Select
                options={assigneesList_}
                styles={customStyles}
                onChange={e => this.handleChange(e, "assignee")}
                defaultValue={this.getDefaultValue(
                  editSilo,
                  assigneesList_,
                  "assignee"
                )}
              />
            </div>

            <div>
              <span className={s.title}>Access Request</span>
              <div className={s.formElemWrapper}>
                <span className={s.label}>Flow Type</span>
                <select
                  className={s.select}
                  defaultValue={
                    editSilo.dataAccess.access["dataSourceType"]
                      ? editSilo.dataAccess.access["dataSourceType"]
                      : null
                  }
                  name={"access_processType"}
                  onChange={e =>
                    this.recordInputData({
                      name: e.target.name,
                      value: e.target.value
                    })
                  }
                >
                  <option value="">Please Select a Process Type</option>

                  {flowTypeOptions.map((item, index) => {
                    return <option value={item.value}>{item.label}</option>;
                  })}
                </select>
              </div>
              {formData.access_processType === "WEBHOOK" && (
                <>
                  <div className={s.formElemWrapper}>
                    <span className={s.label}>URL</span>
                    <input
                      type={"text"}
                      name={"access_url"}
                      onChange={e =>
                        this.recordInputData({
                          name: e.target.name,
                          value: e.target.value
                        })
                      }
                      defaultValue={
                        editSilo.dataAccess &&
                        editSilo.dataAccess.access &&
                        editSilo.dataAccess.access.url
                          ? editSilo.dataAccess.access.url
                          : null
                      }
                    />
                  </div>
                  <div className={s.formElemWrapper}>
                    <span className={s.label}>Header</span>
                    <textarea
                      name={"access_headers"}
                      onChange={e =>
                        this.recordInputData({
                          name: e.target.name,
                          value: e.target.value
                        })
                      }
                      defaultValue={
                        editSilo.dataAccess &&
                        editSilo.dataAccess.access &&
                        editSilo.dataAccess.access.headers
                          ? JSON.stringify(editSilo.dataAccess.access.headers)
                          : null
                      }
                    />
                  </div>
                  <div className={s.formElemWrapper}>
                    <span className={s.label}>Method</span>
                    <select
                      className={s.select}
                      defaultValue={
                        editSilo.dataAccess.access["method"]
                          ? editSilo.dataAccess.access["method"]
                          : null
                      }
                      name={"access_method"}
                      onChange={e =>
                        this.recordInputData({
                          name: e.target.name,
                          value: e.target.value
                        })
                      }
                    >
                      <option value="">Please Select a Method</option>

                      {methodOptions.map((item, index) => {
                        return <option value={item.value}>{item.label}</option>;
                      })}
                    </select>
                  </div>
                </>
              )}

              <span className={s.title}>Deletion Request</span>
              <div className={s.formElemWrapper}>
                <span className={s.label}>Flow Type</span>
                <select
                  className={s.select}
                  defaultValue={
                    editSilo.dataAccess &&
                    editSilo.dataAccess.delete &&
                    editSilo.dataAccess.delete["dataSourceType"]
                      ? editSilo.dataAccess.delete["dataSourceType"]
                      : null
                  }
                  name={"delete_processType"}
                  onChange={e =>
                    this.recordInputData({
                      name: e.target.name,
                      value: e.target.value
                    })
                  }
                >
                  {flowTypeOptions.map((item, index) => {
                    return <option value={item.value}>{item.label}</option>;
                  })}
                </select>
              </div>
              {formData.delete_processType === "WEBHOOK" && (
                <>
                  <div className={s.formElemWrapper}>
                    <span className={s.label}>URL</span>
                    <input
                      type={"text"}
                      name={"delete_url"}
                      onChange={e =>
                        this.recordInputData({
                          name: e.target.name,
                          value: e.target.value
                        })
                      }
                      defaultValue={
                        editSilo.dataAccess &&
                        editSilo.dataAccess.delete &&
                        editSilo.dataAccess.delete["url"]
                          ? editSilo.dataAccess.delete["url"]
                          : null
                      }
                    />
                  </div>
                  <div className={s.formElemWrapper}>
                    <span className={s.label}>Headers</span>
                    <textarea
                      name={"delete_headers"}
                      onChange={e =>
                        this.recordInputData({
                          name: e.target.name,
                          value: e.target.value
                        })
                      }
                      defaultValue={
                        editSilo.dataAccess &&
                        editSilo.dataAccess.delete &&
                        editSilo.dataAccess.delete["headers"]
                          ? JSON.stringify(
                              editSilo.dataAccess.delete["headers"]
                            )
                          : null
                      }
                    />
                  </div>
                  <div className={s.formElemWrapper}>
                    <span className={s.label}>Method</span>
                    <select
                      className={s.select}
                      defaultValue={
                        editSilo.dataAccess.delete["method"]
                          ? editSilo.dataAccess.delete["method"]
                          : null
                      }
                      name={"delete_method"}
                      onChange={e =>
                        this.recordInputData({
                          name: e.target.name,
                          value: e.target.value
                        })
                      }
                    >
                      <option value="">Please Select a Method</option>

                      {methodOptions.map((item, index) => {
                        return <option value={item.value}>{item.label}</option>;
                      })}
                    </select>
                  </div>
                </>
              )}

              <span className={s.title}>ID Mapping API</span>
              <div className={`${s.formElemWrapper} ${s.checkBoxWrapper}`}>
                <label className={s.checkboxContainer}>
                  <input
                    type="checkbox"
                    name={"oauthId"}
                    onClick={this.recordCheckboxData}
                    defaultChecked={
                      editSilo.idMapping && editSilo.idMapping.sameAsAuth
                        ? true
                        : false
                    }
                  />
                  <span className={s.checkMark}></span>
                </label>
                <span className={`${s.label} ${s.checkboxLabel}`}>
                  Same as O-Auth ID
                </span>
              </div>
              <>
                <div className={s.formElemWrapper}>
                  <span className={s.label}>URL</span>
                  <input
                    type={"text"}
                    name={"idmap_url"}
                    onChange={e =>
                      this.recordInputData({
                        name: e.target.name,
                        value: e.target.value
                      })
                    }
                    defaultValue={
                      editSilo.idMapping && editSilo.idMapping.url
                        ? editSilo.idMapping.url
                        : null
                    }
                  />
                </div>
                <div className={s.formElemWrapper}>
                  <span className={s.label}>Header</span>
                  <textarea
                    name={"idmap_header"}
                    onChange={e =>
                      this.recordInputData({
                        name: e.target.name,
                        value: e.target.value
                      })
                    }
                    defaultValue={
                      editSilo.idMapping && editSilo.idMapping.headers
                        ? JSON.stringify(editSilo.idMapping.headers)
                        : null
                    }
                  />
                </div>
                <div className={s.formElemWrapper}>
                  <span className={s.label}>Method</span>
                  <select
                    className={s.select}
                    name={"id_method"}
                    onChange={e =>
                      this.recordInputData({
                        name: e.target.name,
                        value: e.target.value
                      })
                    }
                    defaultValue={
                      editSilo.idMapping && editSilo.idMapping.method
                        ? editSilo.idMapping.method
                        : null
                    }
                  >
                    <option value="">Please Select a Method</option>
                    {methodOptions.map((item, index) => {
                      return <option value={item.value}>{item.label}</option>;
                    })}
                  </select>
                </div>
                {/* <div className={s.formElemWrapper}>
                      <span className={s.label}>Process Type</span>
                      <Select
                        options={processTypeOptions}
                        styles={customStyles}
                        onChange={e =>
                          this.handleChange("idMapping", e, "processType")
                        }
                      />
                    </div> */}
              </>
            </div>

            {/* <div className={s.formElemWrapper}>
              <span className={s.label}>User Type</span>
              <Select options={options} styles={customStyles} />
            </div> */}
            {/* <div className={s.formElemWrapper}>
              <span className={s.label}>Overall Assignee</span>
              <Select
                options={options}
                styles={customStyles}
                noOptionsMessage={() => this.getNoRecordsFound()}
              />
            </div> */}
            <div>
              {/* <div className={`${s.formElemWrapper} ${s.checkBoxWrapper}`}>
                  <label className={s.checkboxContainer}>
                    <input
                      type="checkbox"
                      name={"createAnother"}
                      onClick={this.recordCheckboxData}
                    />
                    <span className={s.checkMark}></span>
                  </label>
                  <span className={`${s.label} ${s.checkboxLabel}`}>
                    Create Another
                  </span>
                </div> */}
              <div className={s.btnParent}>
                <Button btnText={"Update"} onClickHandler={this.updateSilos} />
                <Button
                  btnText={"Cancel"}
                  type={`transparent`}
                  noBg
                  onClickHandler={this.closeEditSilos}
                />
                <span
                  onClick={() => this.initateDeleteSilos(editSilo)}
                  className={s.trash}
                >
                  <FaTrash />
                </span>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    );
  };

  /*
  Silos Edit
*/
  editSilo = silosData => {
    let formData = {
      dsType: silosData.dsType,
      name: silosData.name && silosData.name,
      owner:
        silosData.manualInfo &&
        `${
          silosData.manualInfo.ownerName ? silosData.manualInfo.ownerName : null
        }&${
          silosData.manualInfo.ownerEmailId
            ? silosData.manualInfo.ownerEmailId
            : null
        }`,
      assignee:
        silosData.manualInfo &&
        `${silosData.assignee.name ? silosData.assignee.name : null}&${
          silosData.assignee.emailId ? silosData.assignee.emailId : null
        }`,
      userType: silosData.userTypes,
      access_url:
        silosData.dataAccess.access && silosData.dataAccess.access.url,
      access_method:
        silosData.dataAccess.access && silosData.dataAccess.access.method,
      access_processType:
        silosData.dataAccess.access &&
        silosData.dataAccess.access.dataSourceType,
      access_headers:
        silosData.dataAccess.access &&
        JSON.stringify(silosData.dataAccess.access.headers),
      delete_url:
        silosData.dataAccess.delete && silosData.dataAccess.delete.url,
      delete_method:
        silosData.dataAccess.delete && silosData.dataAccess.delete.method,
      delete_processType:
        silosData.dataAccess.delete &&
        silosData.dataAccess.delete.dataSourceType,
      delete_headers:
        silosData.dataAccess.delete &&
        JSON.stringify(silosData.dataAccess.delete.headers),
      idmap_url: silosData.idMapping && silosData.idMapping.url,
      idmap_method: silosData.idMapping && silosData.idMapping.method,
      idmap_headers: silosData.idMapping && silosData.idMapping.headers,
      oauthId: silosData.idMapping && silosData.idMapping.sameAsAuth,
      silosId: silosData.silosId
    };
    console.log(formData);
    this.setState({ editSilo: silosData, formData: formData });
  };

  /*
    TODO - Generate all Form Fields from Field Generator
  */
  renderAddSilos = () => {
    const {
      showRequestSubmitForm,
      formData,
      showBasicInfo,
      currentPane
    } = this.state;
    let checkpoints = [
      {
        title: "Basic Info",
        config_key: "basic_info",
        action: () => this.changePane("basic_info")
      },
      {
        title: "Access Request",
        config_key: "access_request",
        action: () => this.changePane("access_request")
      },
      {
        title: "Deletion Request",
        config_key: "delete_request",
        action: () => this.changePane("delete_request")
      }
    ];
    // /*
    //   If Manual then show only basic Info
    // */
    // if (formData["type"] === "manual") {
    //   checkpoints = [checkpoints[0]];
    // }

    return (
      <Modal
        path={"#addSilos"}
        showCloseButton
        onCloseHandler={this.closeAddSilos}
        disableOuterClick
        customStyle={{ width: "768px", height: "600px" }}
      >
        <>
          <div className={s.inputParent}>
            <span className={s.title}>Add Data Source</span>
          </div>
          <StepProgressBar
            checkPoints={checkpoints}
            length={"33%"}
            active={currentPane}
          />
        </>
        {currentPane === "basic_info" && this.renderBasicInfo()}
        {currentPane === "access_request" && this.getAccessRequestMarkup()}
        {currentPane === "delete_request" && this.getDeleteRequestMarkup()}
      </Modal>
    );
  };

  renderBasicInfo = () => {
    const { formData } = this.state;
    const { assignees } = this.props;
    let assigneesList =
      assignees.data && assignees.data.data && assignees.data.data;
    const assigneesList_ =
      assigneesList &&
      assigneesList.map((item, index) => {
        let result = {
          label: `${item.name ? item.name : "-"} (${item.emailId})`,
          value: `${item.name}&${item.emailId}`
        };
        return result;
      });
    return (
      <>
        <div className={s.inputParent}>
          <>
            {/* <div className={s.formElemWrapper}>
              <span className={s.label}>Flow Type</span>
              <select
                className={s.select}
                defaultValue={
                  formData["type"] ? formData["type"] : flowTypeOptions[1].value
                }
                name={"type"}
                onChange={e =>
                  this.recordInputData({
                    name: e.target.name,
                    value: e.target.value
                  })
                }
              >
                {flowTypeOptions.map((item, index) => {
                  return <option value={item.value}>{item.label}</option>;
                })}
              </select>
            </div> */}
            <div className={s.formElemWrapper}>
              <span className={s.label}>DS Type</span>
              <select
                className={s.select}
                defaultValue={
                  formData["dsType"]
                    ? formData["dsType"]
                    : dsTypeOptions[0].value
                }
                name={"dsType"}
                onChange={e =>
                  this.recordInputData({
                    name: e.target.name,
                    value: e.target.value
                  })
                }
              >
                {dsTypeOptions.map((item, index) => {
                  return (
                    <option key={`dstype_option_${index}`} value={item.value}>
                      {item.label}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>Silos Name</span>
              <input
                type={"text"}
                name={"name"}
                onChange={e =>
                  this.recordInputData({
                    name: e.target.name,
                    value: e.target.value
                  })
                }
                value={formData["name"] ? formData["name"] : ""}
              />
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>Owner Email</span>
              <Select
                options={assigneesList_}
                styles={customStyles}
                onChange={e => this.handleChange(e, "owner")}
              />
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>User Type</span>
              <Select
                options={userTypeOptions}
                styles={customStyles}
                onChange={e => this.handleChange(e, "userType")}
                isMulti
              />
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>Assignee Email</span>
              <Select
                options={assigneesList_}
                styles={customStyles}
                onChange={e => this.handleChange(e, "assignee")}
              />
            </div>
          </>
        </div>
        {formData["type"] === "webhook" && this.getIdMappingRequest()}
        <div className={s.inputParent}>
          <div>
            <div className={s.btnParent}>
              <Button
                btnText={"Next"}
                onClickHandler={() => this.changePane("access_request")}
              />
              <Button
                btnText={"Cancel"}
                type={`transparent`}
                noBg
                onClick={this.toggleAddSilos}
              />
            </div>
          </div>
        </div>
      </>
    );
  };

  onBlurHandler = e => {
    const { formData } = this.state;
    let formData_ = { ...formData };
    try {
      formData_[e.target.name] = JSON.stringify(e.target.value, undefined, 2);
      formData_[e.target.name] = JSON.parse(formData_[e.target.name]);
      this.setState({ formData: formData_ });
    } catch {
      console.log("Invalid JSON");
    }
  };

  getIdMappingRequest = () => {
    const { formData } = this.state;
    return (
      <div className={s.inputParent}>
        <span className={s.title}>ID Mapping API</span>
        <div className={`${s.formElemWrapper} ${s.checkBoxWrapper}`}>
          <label className={s.checkboxContainer}>
            <input
              type="checkbox"
              name={"oauthId"}
              onClick={this.recordCheckboxData}
            />
            <span className={s.checkMark}></span>
          </label>
          <span className={`${s.label} ${s.checkboxLabel}`}>
            Same as O-Auth ID
          </span>
        </div>
        {!formData["oauthId"] && (
          <>
            <div className={s.formElemWrapper}>
              <span className={s.label}>URL</span>
              <input
                type={"text"}
                name={"idmap_url"}
                onChange={e =>
                  this.recordInputData({
                    name: e.target.name,
                    value: e.target.value
                  })
                }
                value={formData.idmap_url ? formData.idmap_url : ""}
              />
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>Header</span>
              <textarea
                name={"idmap_header"}
                // onChange={e =>
                //   this.onBlurHandler({
                //     name: e.target.name,
                //     value: e.target.value
                //   })
                // }
                onBlur={this.onBlurHandler}
                defaultValue={
                  formData.idmap_header ? formData.idmap_header : ""
                }
              />
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>Method</span>
              {/* <Select
            options={methodOptions}
            styles={customStyles}
            onChange={e => this.handleChange(e, "id_method")}
          /> */}
              <select
                className={s.select}
                defaultValue={
                  formData["id_method"] ? formData["id_method"] : null
                }
                name={"id_method"}
                onChange={e =>
                  this.recordInputData({
                    name: e.target.name,
                    value: e.target.value
                  })
                }
              >
                <option value="">Please Select a Method</option>
                {methodOptions.map((item, index) => {
                  return (
                    <option
                      key={`id_method_options_${index}`}
                      value={item.value}
                    >
                      {item.label}
                    </option>
                  );
                })}
              </select>
            </div>
          </>
        )}
      </div>
    );
  };

  getDeleteRequestMarkup = () => {
    const { formData } = this.state;
    return (
      <div className={s.inputParent}>
        {/* <span className={s.title}>Delete Request API</span> */}
        <div className={s.formElemWrapper}>
          <span className={s.label}>Process Type</span>
          {/* <Select
            options={processTypeOptions}
            styles={customStyles}
            onChange={e => this.handleChange(e, "delete_processType")}
          /> */}
          <select
            className={s.select}
            defaultValue={
              formData["delete_processType"]
                ? formData["delete_processType"]
                : null
            }
            name={"delete_processType"}
            onChange={e =>
              this.recordInputData({
                name: e.target.name,
                value: e.target.value
              })
            }
          >
            {flowTypeOptions.map((item, index) => {
              return <option value={item.value}>{item.label}</option>;
            })}
          </select>
        </div>
        {formData["delete_processType"] === "WEBHOOK" && (
          <>
            <div className={s.formElemWrapper}>
              <span className={s.label}>URL</span>
              <input
                type={"text"}
                name={"delete_url"}
                onChange={e =>
                  this.recordInputData({
                    name: e.target.name,
                    value: e.target.value
                  })
                }
                value={formData.delete_url ? formData.delete_url : ""}
              />
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>Headers</span>
              <textarea
                name={"delete_headers"}
                // onChange={e =>
                //   this.recordInputData({
                //     name: e.target.name,
                //     value: e.target.value
                //   })
                // }
                onBlur={this.onBlurHandler}
                defaultValue={
                  formData.delete_headers ? formData.delete_headers : ""
                }
              />
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>Method</span>
              {/* <Select
            options={methodOptions}
            styles={customStyles}
            onChange={e => this.handleChange(e, "delete_method")}
          /> */}
              <select
                className={s.select}
                defaultValue={
                  formData["delete_method"] ? formData["delete_method"] : null
                }
                name={"delete_method"}
                onChange={e =>
                  this.recordInputData({
                    name: e.target.name,
                    value: e.target.value
                  })
                }
              >
                <option value="">Please Select a Method</option>

                {methodOptions.map((item, index) => {
                  return <option value={item.value}>{item.label}</option>;
                })}
              </select>
            </div>
          </>
        )}
        <div className={s.inputParent}>
          <div>
            <div className={s.btnParent}>
              <Button btnText={"Create"} onClickHandler={this.createSilos} />
              <Button
                btnText={"Cancel"}
                type={`transparent`}
                noBg
                onClick={this.toggleAddSilos}
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  getAccessRequestMarkup = () => {
    const { formData } = this.state;
    return (
      <div className={s.inputParent}>
        {/* <span className={s.title}>Request Submit API</span> */}
        <div className={s.formElemWrapper}>
          <span className={s.label}>Process Type</span>
          {/* <Select
            options={processTypeOptions}
            styles={customStyles}
            onChange={e => this.handleChange(e, "access_processType")}
          /> */}
          <select
            className={s.select}
            defaultValue={
              formData["access_processType"]
                ? formData["access_processType"]
                : null
            }
            name={"access_processType"}
            onChange={e =>
              this.recordInputData({
                name: e.target.name,
                value: e.target.value
              })
            }
          >
            {flowTypeOptions.map((item, index) => {
              return (
                <option key={`${index}_access_processType`} value={item.value}>
                  {item.label}
                </option>
              );
            })}
          </select>
        </div>
        {/* If ProcessType is Auto then render whole form */}
        {formData["access_processType"] === "WEBHOOK" && (
          <>
            <div className={s.formElemWrapper}>
              <span className={s.label}>URL</span>
              <input
                type={"text"}
                name={"access_url"}
                onChange={e =>
                  this.recordInputData({
                    name: e.target.name,
                    value: e.target.value
                  })
                }
                value={formData.access_url ? formData.access_url : ""}
              />
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>Header</span>
              <textarea
                name={"access_headers"}
                // onChange={e =>
                //   this.recordInputData({
                //     name: e.target.name,
                //     value: e.target.value
                //   })
                // }
                onBlur={this.onBlurHandler}
                defaultValue={
                  formData.access_headers ? formData.access_headers : ""
                }
              />
            </div>
            <div className={s.formElemWrapper}>
              <span className={s.label}>Method</span>
              {/* <Select
            options={methodOptions}
            styles={customStyles}
            onChange={e => this.handleChange(e, "access_method")}
          /> */}
              <select
                className={s.select}
                defaultValue={
                  formData["access_method"] ? formData["access_method"] : null
                }
                name={"access_method"}
                onChange={e =>
                  this.recordInputData({
                    name: e.target.name,
                    value: e.target.value
                  })
                }
              >
                <option value="">Please Select a Method</option>

                {methodOptions.map((item, index) => {
                  return <option value={item.value}>{item.label}</option>;
                })}
              </select>
            </div>
          </>
        )}

        <div className={s.inputParent}>
          <div>
            <div className={s.btnParent}>
              <Button
                btnText={"Next"}
                onClickHandler={() => this.changePane("delete_request")}
              />
              <Button
                btnText={"Cancel"}
                type={`transparent`}
                noBg
                onClick={this.toggleAddSilos}
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  closeAddSilos = () => {
    this.setState({ showAddSilos: false, showBasicInfo: false });
    window.history.back();
  };

  closeEditSilos = () => {
    console.log("tiis");
    this.setState({
      editSilo: false,
      formData: {
        access_processType: "WEBHOOK",
        delete_processType: "WEBHOOK",
        type: "webhook"
      }
    });
  };

  /*
    Open Login Window for External Silos
    @params silo 
  */
  openWindow = silo => {
    let newwindow = window.open(
      silo.loginURL,
      "_blank",
      "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400"
    );
    if (window.focus) {
      newwindow.focus();
    }
    return false;
  };

  initateDeleteSilos = data => {
    const { deleteSilos, userInfo } = this.props;
    console.log(data);
    let clientId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;
    let payload = [data.silosId];
    deleteSilos(clientId, payload);
  };

  render() {
    const { showAddSilos, editSilo } = this.state;
    const { silos } = this.props;
    return (
      <PlatformLayout
        white
        position="relative"
        headerChildren={<PlatformHeaderChildren />}
      >
        <AccessControl fullScreen={true} meta={permissions.VIEW_DATA_SILOS}>
          <div className={s.contentPanel}>
            <div className={s.upperPanel}>
              <span className={s.title}>Data Sources</span>
            </div>
            <div className={s.silosContainer}>
              <div key={`_silosParent`} className={s.silosParent}>
                <span className={s.categoryTitle}>Internal</span>
                <div className={s.silos}>
                  {silos.loading && (
                    <div>
                      <DotLoader />
                    </div>
                  )}
                  {!silos.loading &&
                    silos.data &&
                    silos.data.data &&
                    silos.data.data.silosInfo &&
                    silos.data.data.silosInfo.map((item, index) => {
                      let data = {
                        title: item.name
                      };
                      return (
                        <Fragment key={`frag_internal_${index}`}>
                          <TitleThumb
                            key={`${index}_TitleThumbInstance`}
                            data={data}
                            onClickHandler={() => {
                              return noActionAccessControl(
                                permissions.EDIT_DATA_SILOS,
                                this.editSilo,
                                this.props.userInfo,
                                [item, "sajs"]
                              );
                            }}
                          />
                          {/* <span onClick={()=>this.initateDeleteSilos(item)}>Delete</span> */}
                          {index === silos.data.data.silosInfo.length - 1 && (
                            <TitleThumb
                              key={`${index}_TitleThumbInstance2`}
                              type={"add"}
                              onClickHandler={this.toggleAddSilos}
                              userInfo={this.props.userInfo}
                            />
                          )}
                        </Fragment>
                      );
                    })}
                  {!silos.loading &&
                    silos.data &&
                    ((silos.data.data &&
                      silos.data.data.silosInfo &&
                      silos.data.data.silosInfo.length === 0) ||
                      !silos.data.data) && (
                      <TitleThumb
                        type={"add"}
                        onClickHandler={this.toggleAddSilos}
                        userInfo={this.props.userInfo}
                      />
                    )}
                </div>
              </div>
              <div key={`_external_silosParent`} className={s.silosParent}>
                <span className={s.categoryTitle}>External</span>
                <div className={s.silos}>
                  {!silos.loading &&
                    silos.data &&
                    silos.data.data &&
                    silos.data.data.externalSilosInfo &&
                    silos.data.data.externalSilosInfo.map((item, index) => {
                      let data = {
                        title: item.name
                      };
                      return (
                        <Fragment key={`frag_${index}`}>
                          <TitleThumb
                            key={`${index}_TitleThumbInstance_external`}
                            data={data}
                            onClickHandler={() => {
                              return noActionAccessControl(
                                permissions.EDIT_DATA_SILOS,
                                this.openWindow,
                                this.props.userInfo,
                                [item]
                              );
                            }}
                          />
                        </Fragment>
                      );
                    })}
                </div>
              </div>
              {showAddSilos && this.renderAddSilos()}
              {editSilo && this.renderEditSilos()}
            </div>
          </div>

          {/* */}
        </AccessControl>
      </PlatformLayout>
    );
  }
}

export default Silos;
