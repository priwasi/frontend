import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { createSilos, getSilos, updateSilos, deleteSilos } from "./actions";

import Silos from "./Silos";

import { bindActionCreators } from "redux";

const mapStateToProps = (state, prevProps) => {
  return {
    userInfo: state.app.userInfo,
    silosCreate: state.silosReducer.silosCreate,
    silos: state.silosReducer.silos,
    permMap: state.app.permMap,
    assignees: state.app.assignees,
    updateSilosData: state.silosReducer.updateSilosData,
    deleteSilosData: state.silosReducer.deleteSilosData
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      createSilos,
      getSilos,
      updateSilos,
      deleteSilos
    },
    dispatch
  );
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Silos));
