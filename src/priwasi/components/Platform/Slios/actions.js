/*
  @Module : CredBox
  @Component : Privacy Public
  @Type : Action Creators
  @Description : Privacy
  @Author : Kaustubh Saxena
*/

/*
  Action Type Constants
*/
import {
  SILOS_CREATE_LOADING,
  SILOS_CREATE_SUCCESS,
  SILOS_CREATE_FAILURE,
  SILOS_CREATE_CLEAR,
  SILOS_LOADING,
  SILOS_SUCCESS,
  SILOS_FAILURE,
  SILOS_CLEAR,
  SILOS_UPDATE_LOADING,
  SILOS_UPDATE_SUCCESS,
  SILOS_UPDATE_FAILURE,
  SILOS_UPDATE_CLEAR,
  SILOS_DELETE_LOADING,
  SILOS_DELETE_SUCCESS,
  SILOS_DELETE_FAILURE,
  SILOS_DELETE_CLEAR
} from "../../../constants/actionTypes";

/*
  Fetch Wrapper used to hit API.
*/

import httpFetch from "../../../utils/http";

/*
  Utility Imports
*/
import { getHeaders, getUrl } from "../../../utils/utilities";
import { getCookie } from "../../../storage/cookie";

/*
  @Purpose : To Create Silos
  @System : CBB Backend
*/

export function createSilos(data, clientId) {
  let apiUrl = getUrl("createSilos");
  let headers = getHeaders(true);
  headers["clientid"] = clientId;
  return {
    types: [SILOS_CREATE_LOADING, SILOS_CREATE_SUCCESS, SILOS_CREATE_FAILURE],
    promise: () =>
      httpFetch(apiUrl, {
        headers: headers,
        method: "POST",
        body: JSON.stringify(data)
      })
  };
}

/*
  @Purpose : To Update Silos
  @System : CBB Backend
*/
export function updateSilos(data, clientId) {
  let apiUrl = getUrl("createSilos");
  let headers = getHeaders(true);
  headers["clientid"] = clientId;
  return {
    types: [SILOS_UPDATE_LOADING, SILOS_UPDATE_SUCCESS, SILOS_UPDATE_FAILURE],
    promise: () =>
      httpFetch(apiUrl, {
        headers: headers,
        method: "PUT",
        body: JSON.stringify(data)
      })
  };
}

/*
  @Purpose : To Get Silos List
  @System : CBB Backend
*/

export function getSilos(clientId) {
  let apiUrl = getUrl("createSilos");
  let headers = getHeaders(true);
  headers["clientid"] = clientId;
  return {
    types: [SILOS_LOADING, SILOS_SUCCESS, SILOS_FAILURE],
    promise: () => httpFetch(apiUrl, { headers: headers, method: "GET" })
  };
}

/*
  @Purpose : To Delete Silos
  @System : CBB Backend
*/

export function deleteSilos(clientId, data) {
  let apiUrl = getUrl("createSilos");
  let headers = getHeaders(true);
  headers["clientid"] = clientId;
  return {
    types: [
      SILOS_DELETE_LOADING,
      SILOS_DELETE_SUCCESS,
      SILOS_DELETE_FAILURE,
      SILOS_DELETE_CLEAR
    ],
    promise: () =>
      httpFetch(apiUrl, {
        headers: headers,
        method: "DELETE",
        body: JSON.stringify(data)
      })
  };
}
