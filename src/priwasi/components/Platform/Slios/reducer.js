// Action Type Constants
import {
  SILOS_CREATE_LOADING,
  SILOS_CREATE_SUCCESS,
  SILOS_CREATE_FAILURE,
  SILOS_CREATE_CLEAR,
  SILOS_LOADING,
  SILOS_SUCCESS,
  SILOS_FAILURE,
  SILOS_CLEAR,
  SILOS_UPDATE_LOADING,
  SILOS_UPDATE_SUCCESS,
  SILOS_UPDATE_FAILURE,
  SILOS_UPDATE_CLEAR,
  SILOS_DELETE_LOADING,
  SILOS_DELETE_SUCCESS,
  SILOS_DELETE_FAILURE,
  SILOS_DELETE_CLEAR
} from "../../../constants/actionTypes";

import {
  getLoadingState,
  getSuccessState,
  getErrorState
} from "../../../store/stateSettler";

const initialState = {
  silosCreate: {
    loading: false,
    data: null,
    error: null
  },
  silos: {
    loading: false,
    data: null,
    error: null
  },
  updateSilosData: {
    loading: false,
    data: null,
    error: null
  },
  deleteSilosData: {
    loading: false,
    data: null,
    error: null
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SILOS_CREATE_LOADING:
      return {
        ...state,
        silosCreate: getLoadingState("silosCreate", state)
      };
    case SILOS_CREATE_SUCCESS:
      return {
        ...state,
        silosCreate: getSuccessState("silosCreate", state, action)
      };
    case SILOS_CREATE_FAILURE:
      return {
        ...state,
        silosCreate: getErrorState("silosCreate", state, action)
      };
    case SILOS_CREATE_CLEAR:
      return {
        ...state,
        silosCreate: initialState.silosCreate
      };
    case SILOS_UPDATE_LOADING:
      return {
        ...state,
        updateSilosData: getLoadingState("updateSilosData", state)
      };
    case SILOS_UPDATE_SUCCESS:
      return {
        ...state,
        updateSilosData: getSuccessState("updateSilosData", state, action)
      };
    case SILOS_UPDATE_FAILURE:
      return {
        ...state,
        updateSilosData: getErrorState("updateSilosData", state, action)
      };
    case SILOS_UPDATE_CLEAR:
      return {
        ...state,
        updateSilosData: initialState.updateSilosData
      };
    case SILOS_LOADING:
      return {
        ...state,
        silos: getLoadingState("silos", state)
      };
    case SILOS_SUCCESS:
      return {
        ...state,
        silos: getSuccessState("silos", state, action)
      };
    case SILOS_FAILURE:
      return {
        ...state,
        silos: getErrorState("silos", state, action)
      };
    case SILOS_CLEAR:
      return {
        ...state,
        silos: initialState.silos
      };
    case SILOS_DELETE_LOADING:
      return {
        ...state,
        deleteSilosData: getLoadingState("deleteSilosData", state)
      };
    case SILOS_DELETE_SUCCESS:
      return {
        ...state,
        deleteSilosData: getSuccessState("deleteSilosData", state, action)
      };
    case SILOS_DELETE_FAILURE:
      return {
        ...state,
        deleteSilosData: getErrorState("deleteSilosData", state, action)
      };
    case SILOS_DELETE_CLEAR:
      return {
        ...state,
        deleteSilosData: initialState.deleteSilosData
      };
    default:
      return state;
  }
}
