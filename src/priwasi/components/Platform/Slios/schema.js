export const silosList = () => {
  return [
    {
      title: "Custom",
      config_key: "custom",
      silos: [
        {
          title: "Payments",
          config_key: "payments",
          icon: false
        }
      ]
    },
    {
      title: "Marketing",
      config_key: "marketing",
      silos: [
        {
          title: "Asana",
          config_key: "asana",
          icon: "asana"
        },
        {
          title: "Trello",
          config_key: "hive",
          icon: false
        },
        {
          title: "Hive",
          config_key: "hive",
          icon: false
        },
        {
          title: "Clubhouse",
          config_key: "clubhouse",
          icon: false
        }
      ]
    },
    {
      title: "Analytics",
      config_key: "analytics",
      silos: [
        {
          title: "Google Analytics",
          config_key: "google_analytics",
          icon: false
        },
        {
          title: "Mix Panel",
          config_key: "mix_panel",
          icon: false
        },
        {
          title: "Heap",
          config_key: "heap",
          icon: false
        },
        {
          title: "Smartlook",
          config_key: "smart_look",
          icon: false
        }
      ]
    }
  ];
};
