/*
  @Module : CredBox
  @Component : Request Preferences
  @Type : Screen
  @Description : Request Preferences
  @Author : Kaustubh Saxena 
*/

// React Imports
import React, { useState, Component } from "react";

// Component Imports
import PlatformLayout from "../../../Layout/PlatformLayout/PlatformLayout";
import PlatformHeaderChildren from "../../../shared/PlatformHeaderChildren";
import Modal from "../../../shared/Modal/ModalContainer";
import Button from "../../../shared/Button/Button";
import Checkbox from "../../../shared/Checkbox/Checkbox";
import OptionSelect from "../../../shared/OptionSelect/OptionSelect";
import Input from "../../../shared/Input/Input";

// Stylesheet Imports
import s from "./Users.module.scss";
import "../../../commonstyles/reactMultiselectCheckbox.css";

// Asset & Utility Imports
import email from "../../../public/images/email.svg";
import { preferences } from "./schema";
import { PROFILE, NEW, EXISTING, TEAM } from "../../../utils/enum";
import { permissions, permissions_title } from "../../../constants/permissions";
import { lookup } from "../../../utils/utilities";
import AccessControl, {
  noActionAccessControl
} from "../../../services/AccessControl/AccessControl";

import DotLoader from "../../../shared/DotLoader";

// Node Modules Import
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ReactTooltip from "react-tooltip";

const tabsSchema = [
  {
    title: "Profile",
    config_key: "profile",
    icon: false
  },
  {
    title: "Team",
    config_key: "team",
    icon: false
  }
];
const rolesTabSchema = [
  {
    title: "New",
    config_key: "new",
    icon: false
  },
  {
    title: "Existing",
    config_key: "existing",
    icon: false
  }
];

const bodyHeaderSchema = [
  {
    title: "Name",
    config_key: "name",
    icon: false
  },
  {
    title: "Email",
    config_key: "email",
    icon: false
  },
  {
    title: "Role",
    config_key: "role",
    icon: false
  },
  {
    title: "Status",
    config_key: "status",
    icon: false
  }
];

const toastMessageMap = {
  updateRoleData: "Role Updated Successfully",
  customRoleCreate: "Role Created Successfully",
  inviteUserData: "User Invited Successfully",
  remindInviteData: "Resend Invite Successfully",
  deleteInviteData: "User Deleted From Team Successfully"
};

class Users extends Component {
  state = {
    formData: {
      permissions: []
    },
    showRoles: false,
    currentRolesTab: NEW,
    currentTab: TEAM,
    roleInfo: null,
    currentPermSet: {},
    userHover: false,
    inviteFormData: {}
  };

  componentDidMount = () => {
    const { fetchAllRoles, fetchTeamMembers, userInfo } = this.props;
    let clientId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;
    fetchAllRoles();
    fetchTeamMembers(clientId);
  };

  componentDidUpdate = (prevProps, prevState) => {
    const { roleInfo } = this.state;

    if (prevState.roleInfo !== roleInfo) {
      let currentPermSet = {};
      roleInfo &&
        roleInfo.permission &&
        roleInfo.permission.map((item, index) => {
          currentPermSet[parseInt(item.id)] = true;
        });
      this.setState({ currentPermSet: currentPermSet });
    }

    this.toastTrigger("customRoleCreate", prevProps);
    this.toastTrigger("updateRoleData", prevProps);
    this.toastTrigger("inviteUserData", prevProps);
    this.toastTrigger("remindInviteData", prevProps);
    this.toastTrigger("deleteInviteData", prevProps);
  };

  toastTrigger = (key, prevProps) => {
    const { fetchAllRoles, fetchTeamMembers, userInfo } = this.props;
    let clientId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;
    if (
      prevProps[key].loading &&
      !this.props[key].loading &&
      this.props[key].data
    ) {
      if (key === "customRoleCreate" || key === "updateRoleData") {
        fetchAllRoles();
        this.toggleAddRole();
      }
      if (
        key === "inviteUserData" ||
        key === "remindInviteData" ||
        key === "deleteInviteData"
      ) {
        this.setState({ inviteFormData: {} });
        fetchTeamMembers(clientId);
      }
      toast(toastMessageMap[key]);
    }
  };

  recordCheckboxData = e => {
    let formData = { ...this.state.formData };
    formData.permissions.push(parseInt(e.target.name));
    this.setState({ formData: formData });
  };

  changeExistingRolePerms = e => {
    const { currentPermSet } = this.state;
    let currentPermSet_ = { ...currentPermSet };
    if (!e.target.checked) {
      delete currentPermSet_[parseInt(e.target.name)];
    } else {
      currentPermSet_[parseInt(e.target.name)] = true;
    }
    this.setState({ currentPermSet: currentPermSet_ });
  };

  /*
    Get Tabs
    @params : none
  */
  getTabs = (type = null) => {
    const { currentRolesTab, currentTab } = this.state;
    let tabs = type ? rolesTabSchema : tabsSchema;
    return (
      <ul className={s.tabsHeader}>
        {tabs.map((item, index) => {
          return (
            <li
              key={`${index}_header`}
              className={`${
                currentRolesTab === item.config_key ||
                currentTab === item.config_key
                  ? s.active
                  : ""
              }`}
              onClick={() => this.switchTabs(item.config_key, type)}
            >
              {item.title}
            </li>
          );
        })}
      </ul>
    );
  };

  /*
    Switch Tabs
  */
  switchTabs = (config_key, type) => {
    if (type) {
      this.setState({ currentRolesTab: config_key });
    } else {
      this.setState({ currentTab: config_key });
    }
  };

  /*
    Render Roles Tab
    TODO - Create a Common Tab Component
  */
  renderRolesTab = () => {
    return (
      <Modal
        showCloseButton
        onCloseHandler={this.toggleAddRole}
        disableOuterClick
        customStyle={{ width: "572px", height: "475px", padding: "20px 39px" }}
      >
        <div>
          {this.getTabs("customRoles")}
          {this.getRoleTabBody()}
        </div>
      </Modal>
    );
  };

  /*
    Record Input Data
  */
  recordInputData = callbackFieldObj => {
    const { roles } = this.props;
    let roles_ = roles && roles.data && roles.data.data;
    let formData_ = { ...this.state.formData };
    let roleInfo = null;
    formData_[callbackFieldObj.name] = callbackFieldObj.value;
    if (callbackFieldObj.name === "existingRole") {
      roleInfo = lookup(roles_, "roleId", parseInt(callbackFieldObj.value));
    }
    this.setState({ formData: formData_, roleInfo: roleInfo });
  };
  /*
    Create Custom Role
    @params : none
  */
  createRole = () => {
    const { formData } = this.state;
    const { userInfo, createCustomRole } = this.props;
    let clientId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;
    let data = {};
    if (formData.customRole) {
      data["roleName"] = formData.customRole;
      data["permissionSet"] = formData.permissions;
      createCustomRole(data, clientId);
    }
  };
  /*
    Update Existing Role
    @params : none
  */
  updateRole = () => {
    const { formData, currentPermSet, roleInfo } = this.state;
    const { userInfo, updateRole } = this.props;
    let clientId =
      userInfo.data && userInfo.data.data && userInfo.data.data.userId;
    let data = {};
    if (formData.existingRole) {
      data["roleName"] = roleInfo.roleName;
      data["roleId"] = roleInfo.roleId;
      data["permissionSet"] = Object.keys(currentPermSet).map(item =>
        parseInt(item)
      );
      updateRole(data, clientId);
    }
  };

  /*
    Role Tab
  */
  getRoleTabBody = () => {
    const { currentRolesTab, formData, currentPermSet } = this.state;
    const {
      allPermissions,
      roles,
      updateRoleData,
      customRoleCreate
    } = this.props;
    let allPermissions_ = allPermissions.data && allPermissions.data.data;

    let roles_ = roles && roles.data && roles.data.data;
    switch (currentRolesTab) {
      case NEW:
        if (customRoleCreate.loading) {
          return (
            <div>
              <DotLoader />
            </div>
          );
        }
        return (
          <>
            <AccessControl
              fullScreen={true}
              meta={permissions.ASSIGNING_PERMISSIONS}
            >
              <Input
                onChangeHandler={e =>
                  this.recordInputData({
                    name: e.target.name,
                    value: e.target.value
                  })
                }
                className={s.roleInput}
                placeholder={"Enter Role Name"}
                name={"customRole"}
                value={formData.customRole ? formData.customRole : ""}
              />
              <div className={s.newListParent}>
                {allPermissions_ &&
                  allPermissions_.map((item, index) => {
                    return (
                      <div key={`${index}_permHeader`} className={s.listItem}>
                        <span className={s.title}>
                          {item.permissionName.replace(/_/g, " ").toLowerCase()}
                        </span>
                        <Checkbox
                          name={item.id}
                          handleChange={this.recordCheckboxData}
                          checked={formData.permissions.includes(item.id)}
                        />
                      </div>
                    );
                  })}
              </div>
              <div className={s.btnParent}>
                <Button
                  btnText={"Cancel"}
                  type={`transparent`}
                  noBg
                  onClickHandler={this.toggleAddRole}
                />
                <Button btnText={"Save"} onClickHandler={this.createRole} />
              </div>
            </AccessControl>
          </>
        );
      case EXISTING:
        if (updateRoleData.loading) {
          return (
            <div>
              <DotLoader />
            </div>
          );
        } else {
          return (
            <>
              <AccessControl
                fullScreen={true}
                meta={permissions.CHANGING_PERMISSIONS}
              >
                <select
                  className={s.select}
                  // defaultValue={editSilo.type ? editSilo.type.toLowerCase() : ""}
                  name={"existingRole"}
                  onChange={e =>
                    this.recordInputData({
                      name: e.target.name,
                      value: e.target.value
                    })
                  }
                >
                  <option value="">Select an Existing Role</option>
                  {roles_ &&
                    roles_.map((item, index) => {
                      return (
                        <option
                          key={`role_option_${index}`}
                          value={item.roleId}
                        >
                          {item.roleName}
                        </option>
                      );
                    })}
                </select>
                <div className={s.newListParent}>
                  {allPermissions_ &&
                    allPermissions_.map((item, index) => {
                      return (
                        <div
                          key={`${index}_permHeaderExisting`}
                          className={s.listItem}
                        >
                          <span className={s.title}>
                            {item.permissionName
                              .replace(/_/g, " ")
                              .toLowerCase()}
                          </span>
                          <Checkbox
                            name={item.id}
                            handleChange={this.changeExistingRolePerms}
                            checked={currentPermSet[item.id] ? true : false}
                          />
                        </div>
                      );
                    })}
                </div>
                <div className={s.btnParent}>
                  <Button
                    btnText={"Cancel"}
                    type={`transparent`}
                    noBg
                    onClickHandler={this.toggleAddRole}
                  />
                  <Button btnText={"Save"} onClickHandler={this.updateRole} />
                </div>
              </AccessControl>
            </>
          );
        }

      default:
        return <div>No</div>;
    }
  };

  /*
    Toggle Add Role
  */
  toggleAddRole = () => {
    if (this.state.showRoles) {
      let formData_ = { ...this.state.formData };
      formData_.permissions = [];
      delete formData_.customRole;
      this.setState({ roleInfo: null, formData: formData_ });
    }
    this.setState({ showRoles: !this.state.showRoles });
  };

  /*
    User Row Hover Handler
  */
  handleUserRowHover = (index = false) => {
    this.setState({ userHover: index });
  };

  /*
    Option Select Callback
  */
  optionSelectCallback = (roleId, data) => {
    const { updateUser } = this.props;
    let body = {
      roleIds: [roleId],
      email: data.email,
      name: data.name
    };
    updateUser(data.clientId, data.userId, body);
  };

  /*
    Invite CallBack
  */
  inviteCallback = (roleId = false, data = {}, input = false, e = false) => {
    let inviteFormData = { ...this.state.inviteFormData };
    if (input) {
      inviteFormData[e.target.name] = e.target.value;
    } else {
      inviteFormData[data.name] = roleId;
    }
    this.setState({ inviteFormData: inviteFormData });
  };

  /*
    Trigger Invite
  */
  triggerInvite = (remind = false, callbackData = false) => {
    const { inviteFormData } = this.state;
    const { inviteUser, userInfo } = this.props;
    let data = {};
    if (remind) {
      data = {
        emailIds: [callbackData.email],
        roleId: inviteFormData.inviteRole
      };
    } else {
      let emailIdsArray = inviteFormData["emailIds"].split(",");
      // Trim Every Email String
      let emailIdsArray_ = emailIdsArray.map(item => item.trim());
      data = {
        emailIds: emailIdsArray_,
        roleId: inviteFormData.inviteRole
      };
    }

    let clientId =
      userInfo &&
      userInfo.data &&
      userInfo.data.data &&
      userInfo.data.data.userId;
    inviteUser(clientId, data, remind);
  };

  /*
    Trigger Delete Invite
  */
  triggerDeleteInvite = (userId, email) => {
    const { deleteInvite, userInfo } = this.props;
    let clientId =
      userInfo &&
      userInfo.data &&
      userInfo.data.data &&
      userInfo.data.data.userId;
    let data = {
      emailIds: [email],
      clientId: clientId,
      userId: userId
    };
    deleteInvite(data);
  };

  /*
      Get Tab Body
      @params : none
    */
  getTabBody = () => {
    const {
      team,
      roles,
      userInfo,
      inviteUserData,
      remindInviteData
    } = this.props;
    const { userHover, currentTab } = this.state;
    switch (currentTab) {
      case "profile":
        return <div>Profile Here</div>;
      case "team":
        let teamArr = team && team.data && team.data.data;
        let clientId =
          userInfo.data && userInfo.data.data && userInfo.data.data.userId;
        return (
          <div className={s.tabsBody}>
            <ReactTooltip />

            <div className={s.bodyHeaders}>
              {bodyHeaderSchema.map((item, index) => {
                return (
                  <div
                    key={`${index}_header_container`}
                    className={s.headerContainer}
                  >
                    <span key={`${index}_header`}>{item.title}</span>
                    {item.config_key === "role" && (
                      <Button
                        type={"small"}
                        btnText={"ADD"}
                        onClickHandler={this.toggleAddRole}
                      />
                    )}
                  </div>
                );
              })}
            </div>
            <div className={s.rowWrapper}>
              {teamArr &&
                teamArr.map((item, index) => {
                  let hoverClass = userHover === index ? s.hoverActive : "";
                  let roleName =
                    item.roles &&
                    item.roles.length > 0 &&
                    item.roles[0].roleName &&
                    item.roles[0].roleName;
                  return (
                    <div
                      className={`${s.rowParent} ${hoverClass}`}
                      key={`${index}_tableParent`}
                      onClick={() => {}}
                      onMouseEnter={() => this.handleUserRowHover(index)}
                      onMouseLeave={() => this.handleUserRowHover()}
                    >
                      <div className={s.userRow}>
                        <span>
                          {/* {item.image && <img src={item.image} />} */}
                          {item.name ? item.name : "N/A"}
                        </span>
                        <span data-tip={item.emailId}>
                          {item.emailId
                            ? item.emailId.length > 25
                              ? item.emailId.substring(0, 24) + "..."
                              : item.emailId
                            : "N/A"}
                        </span>
                        {hoverClass === "" && (
                          <span>{roleName ? roleName : "N/A"}</span>
                        )}
                        {hoverClass !== "" && (
                          <span>
                            <AccessControl
                              disabled={true}
                              meta={permissions.CHANGING_ROLES}
                              moduleStyle={s.optionOverlap}
                            >
                              <span className={s.roleSelectorParent}>
                                <OptionSelect
                                  value={roleName}
                                  customStyle={"custom"}
                                  options={roles.data && roles.data.data}
                                  type={"roles"}
                                  callbackMethod={this.optionSelectCallback}
                                  data={{
                                    clientId: clientId,
                                    userId: item.userId,
                                    email: item.emailId,
                                    name: item.name
                                  }}
                                  name={"updateRole"}
                                />
                              </span>
                            </AccessControl>
                          </span>
                        )}
                        <span>{item.status}</span>
                        {item.status === "Pending" && hoverClass !== "" && (
                          <span>
                            <AccessControl
                              disabled={true}
                              meta={permissions.REMINDING_TEAM_MEMBERS}
                              moduleStyle={s.resendInviteOverlap}
                            >
                              <img
                                onClick={() =>
                                  noActionAccessControl(
                                    permissions.REMINDING_TEAM_MEMBERS,
                                    this.triggerInvite,
                                    this.props.userInfo,
                                    [
                                      true,
                                      {
                                        email: item.emailId,
                                        userId: item.userId
                                      }
                                    ]
                                  )
                                }
                                src={email}
                              />
                            </AccessControl>
                          </span>
                        )}
                        {hoverClass !== "" && (
                          <span
                            className={s.deleteBtn}
                            onClick={() =>
                              this.triggerDeleteInvite(
                                item.userId,
                                item.emailId
                              )
                            }
                          >
                            Delete
                          </span>
                        )}
                      </div>
                    </div>
                  );
                })}
            </div>

            <div className={s.tabFooter}>
              <div className={s.searchPlaceholder}>
                <AccessControl
                  disabled={true}
                  meta={permissions.INVITING_TEAM_MEMBERS}
                  moduleStyle={s.inviteOverlap}
                >
                  <input
                    type="text"
                    className={s.search}
                    name={"emailIds"}
                    placeholder={"Enter multiple emails seperated by commas"}
                    onChange={e => this.inviteCallback(false, {}, true, e)}
                  />
                </AccessControl>
              </div>

              <div className={s.roleSelectorParent}>
                <AccessControl
                  disabled={true}
                  meta={permissions.ASSIGNING_ROLES}
                  moduleStyle={s.optionAssignOverlap}
                >
                  <OptionSelect
                    additionalOptions={
                      <span
                        className={s.newRoleBtnParent}
                        onClick={this.toggleAddRole}
                      >
                        <Button type={"small"} btnText={"+ New"} />
                      </span>
                    }
                    options={roles.data && roles.data.data}
                    type={"roles"}
                    callbackMethod={this.inviteCallback}
                    name={"inviteRole"}
                  />
                </AccessControl>
              </div>

              <div className={s.btnParent}>
                {!inviteUserData.loading && (
                  <AccessControl
                    disabled={true}
                    meta={permissions.INVITING_TEAM_MEMBERS}
                    moduleStyle={s.inviteBtnOverlap}
                  >
                    <Button
                      onClickHandler={() => this.triggerInvite(false)}
                      btnText={"+ INVITE"}
                    />
                  </AccessControl>
                )}
                {inviteUserData.loading && (
                  <div className={s.loaderContainer}>
                    <DotLoader />
                  </div>
                )}
              </div>
            </div>
          </div>
        );
      default:
        return false;
    }
  };

  render() {
    const { formData, showRoles } = this.state;
    return (
      <PlatformLayout
        white
        position="relative"
        headerChildren={<PlatformHeaderChildren />}
      >
        <div className={s.contentPanel}>
          <div className={s.upperPanel}>
            <span className={s.title}>Settings</span>
          </div>
          <div className={s.tabsWrapper}>
            {/*-------------------- Tabs Data --------------------*/}
            <div className={s.headerWrapper}>{this.getTabs()}</div>
            {this.getTabBody()}
            {/*-------------------- Tabs Data --------------------*/}
          </div>
        </div>
        {showRoles && this.renderRolesTab()}
      </PlatformLayout>
    );
  }
}

export default Users;
