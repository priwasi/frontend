import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  createCustomRole,
  fetchAllRoles,
  fetchTeamMembers,
  updateRole,
  updateUser,
  inviteUser,
  deleteInvite
} from "./actions";

import Users from "./Users";

import { bindActionCreators } from "redux";

const mapStateToProps = (state, prevProps) => {
  return {
    allPermissions: state.app.allPermissions,
    userInfo: state.app.userInfo,
    roles: state.userReducer.roles,
    team: state.userReducer.team,
    updateRoleData: state.userReducer.updateRoleData,
    customRoleCreate: state.userReducer.customRoleCreate,
    updateUserData: state.userReducer.updateUserData,
    inviteUserData: state.userReducer.inviteUserData,
    deleteInviteData: state.userReducer.deleteInviteData,
    remindInviteData: state.userReducer.remindInviteData
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      createCustomRole,
      fetchAllRoles,
      fetchTeamMembers,
      updateRole,
      updateUser,
      inviteUser,
      deleteInvite
    },
    dispatch
  );
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Users));
