/*
  @Module : CredBox
  @Component : Users Teams
  @Type : Action Creators
  @Description : User
  @Author : Kaustubh Saxena
*/

/*
  Action Type Constants
*/
import {
  CREATE_CUSTOM_ROLE_LOADING,
  CREATE_CUSTOM_ROLE_SUCCESS,
  CREATE_CUSTOM_ROLE_FAILURE,
  FETCH_ALL_ROLES_LOADING,
  FETCH_ALL_ROLES_SUCCESS,
  FETCH_ALL_ROLES_FAILURE,
  FETCH_ALL_USERS_LOADING,
  FETCH_ALL_USERS_SUCCESS,
  FETCH_ALL_USERS_FAILURE,
  UPDATE_ROLE_LOADING,
  UPDATE_ROLE_SUCCESS,
  UPDATE_ROLE_FAILURE,
  UPDATE_USER_LOADING,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILURE,
  UPDATE_USER_CLEAR,
  INVITE_USER_LOADING,
  INVITE_USER_SUCCESS,
  INVITE_USER_FAILURE,
  INVITE_USER_CLEAR,
  DELETE_INVITE_LOADING,
  DELETE_INVITE_SUCCESS,
  DELETE_INVITE_FAILURE,
  DELETE_INVITE_CLEAR,
  REMIND_USER_LOADING,
  REMIND_USER_SUCCESS,
  REMIND_USER_FAILURE,
  REMIND_USER_CLEAR
} from "../../../constants/actionTypes";

/*
  Fetch Wrapper used to hit API.
*/

import httpFetch from "../../../utils/http";

/*
  Utility Imports
*/
import { getHeaders, getUrl } from "../../../utils/utilities";
import { getCookie } from "../../../storage/cookie";

/*
  @Purpose : To Create Custom Role
  @System : CBB Backend
*/

export function createCustomRole(data, clientId) {
  let apiUrl = getUrl("createCustomRole");
  let headers = getHeaders(true, true);
  headers["clientid"] = clientId;
  return {
    types: [
      CREATE_CUSTOM_ROLE_LOADING,
      CREATE_CUSTOM_ROLE_SUCCESS,
      CREATE_CUSTOM_ROLE_FAILURE
    ],
    promise: () =>
      httpFetch(apiUrl, {
        headers: headers,
        method: "POST",
        body: JSON.stringify(data)
      })
  };
}
/*
  @Purpose : To Create Custom Role
  @System : CBB Backend
*/

export function updateRole(data, clientId) {
  let apiUrl = getUrl("updateRole");
  let headers = getHeaders(true, true);
  headers["clientid"] = clientId;
  return {
    types: [UPDATE_ROLE_LOADING, UPDATE_ROLE_SUCCESS, UPDATE_ROLE_FAILURE],
    promise: () =>
      httpFetch(apiUrl, {
        headers: headers,
        method: "PUT",
        body: JSON.stringify(data)
      })
  };
}

/*
  @Purpose : To Fetch All Roles and Permissions List
  @System : CBB Backend
*/

export function fetchAllRoles(data, clientId) {
  let apiUrl = getUrl("fetchAllRoles");
  let headers = getHeaders(true, true);
  headers["clientid"] = clientId;
  return {
    types: [
      FETCH_ALL_ROLES_LOADING,
      FETCH_ALL_ROLES_SUCCESS,
      FETCH_ALL_ROLES_FAILURE
    ],
    promise: () =>
      httpFetch(apiUrl, {
        headers: headers
      })
  };
}

/*
  @Purpose : To Fetch All the Users
  @System : CBB Backend
*/
export function fetchTeamMembers(clientId) {
  let apiUrl = getUrl("allUsers");
  let headers = getHeaders(true, true);
  headers["clientid"] = clientId;
  return {
    types: [
      FETCH_ALL_USERS_LOADING,
      FETCH_ALL_USERS_SUCCESS,
      FETCH_ALL_USERS_FAILURE
    ],
    promise: () =>
      httpFetch(apiUrl, {
        headers: headers
      })
  };
}
/*
  @Purpose : To Update the User
  @System : CBB Backend
*/
export function updateUser(clientId, userId, data) {
  let apiUrl = getUrl("updateUser");
  let headers = getHeaders(true, true);
  headers["clientid"] = clientId;
  headers["userId"] = userId;
  return {
    types: [UPDATE_USER_LOADING, UPDATE_USER_SUCCESS, UPDATE_USER_FAILURE],
    promise: () =>
      httpFetch(apiUrl, {
        headers: headers,
        method: "PUT",
        body: JSON.stringify(data)
      })
  };
}

/*
  @Purpose : To Invite the User
  @System : CBB Backend
*/
export function inviteUser(clientId, data, remind = false) {
  let types = [INVITE_USER_LOADING, INVITE_USER_SUCCESS, INVITE_USER_FAILURE];
  let apiUrl = getUrl("inviteUser");

  if (remind) {
    types = [REMIND_USER_LOADING, REMIND_USER_SUCCESS, REMIND_USER_FAILURE];
    apiUrl = getUrl("remindUser");
  }
  console.log(remind, apiUrl);
  let headers = getHeaders(true, true);
  headers["clientid"] = clientId;
  return {
    types: types,
    promise: () =>
      httpFetch(apiUrl, {
        headers: headers,
        method: "POST",
        body: JSON.stringify(data)
      })
  };
}
/*
  @Purpose : To Delete the User
  @System : CBB Backend
*/
export function deleteInvite(data) {
  let apiUrl = getUrl("deleteInvite");
  let headers = getHeaders(true, true);
  let data_ = { ...data };
  headers["clientid"] = data_.clientId;
  headers["userId"] = data_.userId;
  delete data_.clientId;
  delete data_.userId;
  return {
    types: [
      DELETE_INVITE_LOADING,
      DELETE_INVITE_SUCCESS,
      DELETE_INVITE_FAILURE
    ],
    promise: () =>
      httpFetch(apiUrl, {
        headers: headers,
        method: "DELETE",
        body: JSON.stringify(data_)
      })
  };
}
