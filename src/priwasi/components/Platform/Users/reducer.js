// Action Type Constants
import {
  CREATE_CUSTOM_ROLE_LOADING,
  CREATE_CUSTOM_ROLE_SUCCESS,
  CREATE_CUSTOM_ROLE_FAILURE,
  CREATE_CUSTOM_ROLE_CLEAR,
  FETCH_ALL_ROLES_LOADING,
  FETCH_ALL_ROLES_SUCCESS,
  FETCH_ALL_ROLES_FAILURE,
  FETCH_ALL_ROLES_CLEAR,
  FETCH_ALL_USERS_LOADING,
  FETCH_ALL_USERS_SUCCESS,
  FETCH_ALL_USERS_FAILURE,
  FETCH_ALL_USERS_CLEAR,
  UPDATE_ROLE_LOADING,
  UPDATE_ROLE_SUCCESS,
  UPDATE_ROLE_FAILURE,
  UPDATE_ROLE_CLEAR,
  UPDATE_USER_LOADING,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILURE,
  UPDATE_USER_CLEAR,
  INVITE_USER_LOADING,
  INVITE_USER_SUCCESS,
  INVITE_USER_FAILURE,
  INVITE_USER_CLEAR,
  DELETE_INVITE_LOADING,
  DELETE_INVITE_SUCCESS,
  DELETE_INVITE_FAILURE,
  DELETE_INVITE_CLEAR,
  REMIND_USER_LOADING,
  REMIND_USER_SUCCESS,
  REMIND_USER_FAILURE,
  REMIND_USER_CLEAR
} from "../../../constants/actionTypes";

import {
  getLoadingState,
  getSuccessState,
  getErrorState
} from "../../../store/stateSettler";

const initialState = {
  customRoleCreate: {
    loading: false,
    data: null,
    error: null
  },
  roles: {
    loading: false,
    data: null,
    error: null
  },
  team: {
    loading: false,
    data: null,
    error: null
  },
  updateRoleData: {
    loading: false,
    data: null,
    error: null
  },
  updateUserData: {
    loading: false,
    data: null,
    error: null
  },
  inviteUserData: {
    loading: false,
    data: null,
    error: null
  },
  deleteInviteData: {
    loading: false,
    data: null,
    error: null
  },
  remindInviteData: {
    loading: false,
    data: null,
    error: null
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CREATE_CUSTOM_ROLE_LOADING:
      return {
        ...state,
        customRoleCreate: getLoadingState("customRoleCreate", state)
      };
    case CREATE_CUSTOM_ROLE_SUCCESS:
      return {
        ...state,
        customRoleCreate: getSuccessState("customRoleCreate", state, action)
      };
    case CREATE_CUSTOM_ROLE_FAILURE:
      return {
        ...state,
        customRoleCreate: getErrorState("customRoleCreate", state, action)
      };
    case CREATE_CUSTOM_ROLE_CLEAR:
      return {
        ...state,
        customRoleCreate: initialState.customRoleCreate
      };
    case FETCH_ALL_ROLES_LOADING:
      return {
        ...state,
        roles: getLoadingState("roles", state)
      };
    case FETCH_ALL_ROLES_SUCCESS:
      return {
        ...state,
        roles: getSuccessState("roles", state, action)
      };
    case FETCH_ALL_ROLES_FAILURE:
      return {
        ...state,
        roles: getErrorState("roles", state, action)
      };
    case FETCH_ALL_ROLES_CLEAR:
      return {
        ...state,
        roles: initialState.roles
      };
    case FETCH_ALL_USERS_LOADING:
      return {
        ...state,
        team: getLoadingState("team", state)
      };
    case FETCH_ALL_USERS_SUCCESS:
      return {
        ...state,
        team: getSuccessState("team", state, action)
      };
    case FETCH_ALL_USERS_FAILURE:
      return {
        ...state,
        team: getErrorState("team", state, action)
      };
    case FETCH_ALL_USERS_CLEAR:
      return {
        ...state,
        team: initialState.team
      };
    case UPDATE_ROLE_LOADING:
      return {
        ...state,
        updateRoleData: getLoadingState("updateRoleData", state)
      };
    case UPDATE_ROLE_SUCCESS:
      return {
        ...state,
        updateRoleData: getSuccessState("updateRoleData", state, action)
      };
    case UPDATE_ROLE_FAILURE:
      return {
        ...state,
        updateRoleData: getErrorState("updateRoleData", state, action)
      };
    case UPDATE_ROLE_CLEAR:
      return {
        ...state,
        updateRoleData: initialState.updateRoleData
      };
    case UPDATE_USER_LOADING:
      return {
        ...state,
        updateUserData: getLoadingState("updateUserData", state)
      };
    case UPDATE_USER_SUCCESS:
      return {
        ...state,
        updateUserData: getSuccessState("updateUserData", state, action)
      };
    case UPDATE_USER_FAILURE:
      return {
        ...state,
        updateUserData: getErrorState("updateUserData", state, action)
      };
    case UPDATE_USER_CLEAR:
      return {
        ...state,
        updateUserData: initialState.updateUserData
      };
    case INVITE_USER_LOADING:
      return {
        ...state,
        inviteUserData: getLoadingState("inviteUserData", state)
      };
    case INVITE_USER_SUCCESS:
      return {
        ...state,
        inviteUserData: getSuccessState("inviteUserData", state, action)
      };
    case INVITE_USER_FAILURE:
      return {
        ...state,
        inviteUserData: getErrorState("inviteUserData", state, action)
      };
    case INVITE_USER_CLEAR:
      return {
        ...state,
        inviteUserData: initialState.inviteUserData
      };
    case DELETE_INVITE_LOADING:
      return {
        ...state,
        deleteInviteData: getLoadingState("deleteInviteData", state)
      };
    case DELETE_INVITE_SUCCESS:
      return {
        ...state,
        deleteInviteData: getSuccessState("deleteInviteData", state, action)
      };
    case DELETE_INVITE_FAILURE:
      return {
        ...state,
        deleteInviteData: getErrorState("deleteInviteData", state, action)
      };
    case DELETE_INVITE_CLEAR:
      return {
        ...state,
        deleteInviteData: initialState.deleteInviteData
      };
    case REMIND_USER_LOADING:
      return {
        ...state,
        remindInviteData: getLoadingState("remindInviteData", state)
      };
    case REMIND_USER_SUCCESS:
      return {
        ...state,
        remindInviteData: getSuccessState("remindInviteData", state, action)
      };
    case REMIND_USER_FAILURE:
      return {
        ...state,
        remindInviteData: getErrorState("remindInviteData", state, action)
      };
    case REMIND_USER_CLEAR:
      return {
        ...state,
        remindInviteData: initialState.remindInviteData
      };
    default:
      return state;
  }
}
