/*
  @Module : PaytmKhata
  @Component : Home
  @Type : Screen
  @Description : Entry Screen
  @Author : Kaustubh Saxena 
*/

// React Imports
import React, { useState, Component } from "react";

// Node Modules Import
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

// Component Imports
import SiteLayout from "../../Layout/SiteLayout/SiteLayout";
import HeaderChildren from "../../shared/HeaderChildren";
import Button from "../../shared/Button/Button";
import DotLoader from "../../shared/DotLoader";

// Stylesheet Imports
import s from "./Privacy.module.scss";

// Utility Imports
import { getCookie } from "../../storage/cookie";
import { convertToFormat } from "../../utils/dateUtils";
import { AES128ECBdecrypt, getPassphrase } from "../../utils/CryptoUtils";
import { removeEscapeCharacters } from "../../utils/utilities";
import Paginator from "../../shared/PaginationComponent/Paginator";

var forge = require("node-forge");

const mock = {
  heading_1: [
    {
      id: 1,
      title: "What we store",
      description: "We store your personal data for analytics purpose."
    },
    {
      id: 2,
      title: "Why we store",
      description: "For analytics purpose."
    }
  ]
};

const apiTitles = ["Access Request", "Delete Request"];

class Privacy extends Component {
  state = {
    currentSelected: "heading_1",
    selectedDescription: null,
    currentTitle: null,
    currentTopicUrl: null,
    emailId: null,
    emailError: null
  };

  componentDidMount = () => {
    const { getPrivacyData } = this.props;
    getPrivacyData();
    // this.setState({ selectedDescription: mock.heading_1[0].description });
  };

  componentDidUpdate = (prevProps, prevState) => {
    const {
      topic,
      initiateRequestData,
      getTopicData,
      encData,
      saveTopic
    } = this.props;
    if (prevProps.topic.loading && !topic.loading && topic.data) {
      let topic_ = topic.data;
      let selectedDescription = topic_[this.state.currentTitle];
      this.setState({ selectedDescription: selectedDescription });
    }

    if (
      prevProps.initiateRequestData.loading &&
      !initiateRequestData.loading &&
      initiateRequestData.data
    ) {
      toast.info("Request Initiated Successfully");
      getTopicData(this.state.currentTopicUrl);
      this.setState({ emailError: false });
    }
    if (encData.loading) {
      toast.info("Decypting Data");
    }
    if (prevProps.encData.loading && !encData.loading && encData.data) {
      this.renderFetchedData();
    }
    if (prevState.currentTitle === this.state.currentTitle) {
      saveTopic(this.state.currentTitle);
    }
  };

  /*
    Open Sub Menu
    @params : event = Object
  */
  openSubMenu = e => {
    let id = e.target.id;
    this.setState({ currentSelected: id });
  };

  /*
    Get Description
    @params : item = Object
  */
  getDescription = item => {
    const { getTopicData } = this.props;
    if (item && item.body) {
      const description = item.body;
      this.setState({
        currentTitle: item.title,
        selectedDescription: description
      });
      if (item.url) {
        this.setState({ currentTopicUrl: item.url });
        getTopicData(item.url);
      }
    }
  };

  /*
    Get List Items
    @params : none
  */

  getListItems = () => {
    const { currentSelected } = this.state;
    const { privacy } = this.props;
    let data_ = privacy.data.data ? privacy.data.data : [];
    let data = data_.map(item => {
      if (apiTitles.includes(item.title)) {
        if (item.title === "Access Request") {
          item.url = `${
            process.env.REACT_APP_STAGE === "staging"
              ? `http://cbclient-staging.credbox.io`
              : process.env.REACT_APP_PRODUCTION_BASE_URL
          }/api/sec/request/access`;
        }
        if (item.title === "Delete Request") {
          item.url = `${
            process.env.REACT_APP_STAGE === "staging"
              ? `http://cbclient-staging.credbox.io`
              : process.env.REACT_APP_PRODUCTION_BASE_URL
          }/api/sec/request/delete`;
        }
      }

      return item;
    });

    return (
      <div className={s.listContainer}>
        <ul>
          {data.map((item, index) => {
            return (
              <li
                key={`list_item_${index}`}
                onClick={() => this.getDescription(item)}
              >
                {item.title}
              </li>
            );
          })}
        </ul>
      </div>
    );
  };

  /*
    Initiate Request
    @params : none
  */
  initiateRequest = () => {
    const { currentTitle, emailId } = this.state;
    const { initiate } = this.props;
    let type = currentTitle === "Access Request" ? "ACCESS" : "DELETE";
    let body = {
      type: type,
      description: "test",
      userType: "CUSTOMER",
      emailId: emailId
    };
    if (emailId) {
      initiate(body);
    } else {
      console.log("Email is necessary");
      this.setState({ emailError: "Email is necessary" });
    }
  };

  /*
    Record Email
    @params e:Object
  */
  recordEmail = e => {
    this.setState({ emailId: e.target.value });
  };

  removeLines(str) {
    return str.replace("\n", "");
  }

  base64ToArrayBuffer = b64 => {
    var byteString = atob(b64);
    var byteArray = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
      byteArray[i] = byteString.charCodeAt(i);
    }
    return byteArray;
  };

  pemToArrayBuffer = (pem, privateKey = false) => {
    if (privateKey) {
      var b64Lines = this.removeLines(pem);
      var b64Prefix = b64Lines.replace("-----BEGIN RSA PRIVATE KEY-----", "");
      var b64Final = b64Prefix.replace("-----END RSA PRIVATE KEY-----", "");
      return b64Final.replace(/\s/g, "");
    } else {
      var b64Lines = this.removeLines(pem);
      var b64Prefix = b64Lines.replace("-----BEGIN PUBLIC KEY-----", "");
      var b64Final = b64Prefix.replace("-----END PUBLIC KEY-----", "");
      return b64Final.replace(/\s/g, "");
    }
    // b64Final = this.removeLines(b64Final);

    // this.base64ToArrayBuffer(b64Final);
  };
  /*
    Get Encrypted Data
    @params requestDetail:Object
  */
  getEncryptedData = requestDetail => {
    const { getEncData } = this.props;
    const requestId = requestDetail.id;
    var rsa = forge.pki.rsa;
    var pki = forge.pki;
    var keys = rsa.generateKeyPair(1024);
    let pem_publicKey = pki.publicKeyToPem(keys.publicKey);
    // let pem_privateKey = pki.privateKeyToPem(keys.privateKey);
    let revisedPem = this.pemToArrayBuffer(pem_publicKey);
    getEncData(requestId, revisedPem);
  };

  renderFetchedData = () => {
    const { encData } = this.props;
    if (encData.data && encData.data.data && encData.data.data.silosResponses) {
      if (encData.data.data.silosResponses.length > 0) {
        return encData.data.data.silosResponses.map((item, index) => {
          let object = AES128ECBdecrypt(item.data, atob(getPassphrase()));

          let newBlob = new Blob([object]);
          var url = window.URL.createObjectURL(newBlob, { oneTimeOnly: true });
          //window.open(url, '_blank', '');
          var anchor = document.createElement("a");
          anchor.href = url;
          anchor.target = "_blank";

          anchor.click();
        });
      } else {
        toast.error("No Data Present in data source.");
      }
    }
  };

  render() {
    const { currentSelected, selectedDescription, currentTitle } = this.state;
    const {
      privacy,
      topic,
      initiateRequestData,
      encData,
      getMoreRequest
    } = this.props;
    return (
      <SiteLayout white position="relative" headerChildren={<HeaderChildren />}>
        {!privacy.loading && privacy.data && (
          <>
            <div className={s.contentPanel}>
              <div className={s.listItem}>
                <div className={s.parentItem}>
                  <div className={s.headingParent}>
                    <span id={"heading_1"}>Privacy</span>
                    {currentSelected === "heading_1" && this.getListItems()}
                  </div>
                </div>
              </div>
              <div className={s.contentViewer}>
                {currentTitle !== "Access Request" &&
                  currentTitle !== "Delete Request" && (
                    <div>
                      <span
                        dangerouslySetInnerHTML={{
                          __html: selectedDescription
                        }}
                      ></span>
                    </div>
                  )}
                {topic.loading && <DotLoader />}
                {selectedDescription &&
                  (currentTitle === "Access Request" ||
                    currentTitle === "Delete Request") && (
                    <div>
                      <h1>
                        {selectedDescription.data &&
                          selectedDescription.data.title &&
                          selectedDescription.data.title}
                      </h1>
                      <span>
                        <h2
                          dangerouslySetInnerHTML={{
                            __html:
                              selectedDescription.data &&
                              selectedDescription.data.h1 &&
                              selectedDescription.data.h1
                          }}
                        ></h2>
                      </span>
                      <span
                        dangerouslySetInnerHTML={{
                          __html:
                            selectedDescription.data &&
                            selectedDescription.data.description
                        }}
                      ></span>
                      {
                        <div>
                          <div className={s.btnParent}>
                            <div>
                              <input
                                type={"email"}
                                placeholder={"Email"}
                                onChange={this.recordEmail}
                              />
                              <Button
                                onClickHandler={this.initiateRequest}
                                btnText={
                                  initiateRequestData.loading
                                    ? "Loading"
                                    : currentTitle === "Access Request"
                                    ? "Request Access"
                                    : "Request Deletion"
                                }
                                type={"primary"}
                              />
                            </div>
                            <span>{this.state.emailError}</span>
                          </div>

                          {!topic.loading &&
                            selectedDescription &&
                            selectedDescription.data &&
                            selectedDescription.data.requestHistory &&
                            selectedDescription.data.requestHistory
                              .requests && (
                              <div>
                                <span className={s.requestsHeading}>
                                  {currentTitle === "Access Request"
                                    ? "Your Previous Access Requests"
                                    : "Your Previous Deletion Requests"}
                                </span>
                                <div className={s.table}>
                                  <div className={s.tableRow}>
                                    <span>Request Date</span>
                                    <span>Status</span>
                                    <span></span>
                                  </div>
                                  <Paginator
                                    height={"300px"}
                                    type={"infinite"}
                                    total={
                                      selectedDescription.data &&
                                      selectedDescription.data.requestHistory &&
                                      selectedDescription.data.requestHistory
                                        .totalHits + 1
                                    }
                                    pageSize={10}
                                    action={getMoreRequest}
                                    clientId={1}
                                    currentTab={currentTitle}
                                  >
                                    {selectedDescription.data.requestHistory.requests.map(
                                      (item, index) => {
                                        return (
                                          <div
                                            key={`${index}_key`}
                                            className={s.tableRow}
                                          >
                                            <span>
                                              {convertToFormat(
                                                new Date(item.requestDate),
                                                "yyyy-mm-dd"
                                              )}
                                            </span>
                                            <span>{item.requestStatus}</span>
                                            <span
                                              onClick={() =>
                                                this.getEncryptedData(item)
                                              }
                                            >
                                              <span className={s.dataParent}>
                                                Get Data
                                              </span>
                                            </span>
                                          </div>
                                        );
                                      }
                                    )}
                                  </Paginator>
                                </div>
                              </div>
                            )}
                        </div>
                      }
                    </div>
                  )}
              </div>
            </div>
          </>
        )}
      </SiteLayout>
    );
  }
}

export default Privacy;
