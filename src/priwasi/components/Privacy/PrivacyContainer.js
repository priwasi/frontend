import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  getPrivacyData,
  getTopicData,
  initiate,
  getEncData,
  getMoreRequest,
  saveTopic
} from "./actions";

import Privacy from "./Privacy";

import { bindActionCreators } from "redux";

const mapStateToProps = (state, prevProps) => {
  return {
    privacy: state.privacyReducer.privacy,
    topic: state.privacyReducer.topic,
    initiateRequestData: state.privacyReducer.initiateRequestData,
    encData: state.privacyReducer.encData
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getPrivacyData,
      getTopicData,
      initiate,
      getEncData,
      getMoreRequest,
      saveTopic
    },
    dispatch
  );
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Privacy)
);
