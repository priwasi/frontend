/*
  @Module : CredBox
  @Component : Privacy Public
  @Type : Action Creators
  @Description : Privacy
  @Author : Kaustubh Saxena
*/

/*
  Action Type Constants
*/
import {
  PRIVACY_LOADING,
  PRIVACY_SUCCESS,
  PRIVACY_FAILURE,
  TOPIC_DATA_LOADING,
  TOPIC_DATA_SUCCESS,
  TOPIC_DATA_FAILURE,
  INITATE_REQUEST_LOADING,
  INITATE_REQUEST_SUCCESS,
  INITATE_REQUEST_FAILURE,
  INITATE_REQUEST_CLEAR,
  ENCRYPTED_DATA_LOADING,
  ENCRYPTED_DATA_SUCCESS,
  ENCRYPTED_DATA_FAILURE,
  ENCRYPTED_DATA_CLEAR,
  REQUEST_MORE_DATA_LOADING,
  REQUEST_MORE_DATA_SUCCESS,
  REQUEST_MORE_DATA_FAILURE,
  REQUEST_MORE_DATA_CLEAR,
  SAVE_TOPIC
} from "../../constants/actionTypes";

/*
  Fetch Wrapper used to hit API.
*/

import httpFetch from "../../utils/http";

/*
  Utility Imports
*/
import { getHeaders, getUrl } from "../../utils/utilities";
import { getCookie } from "../../storage/cookie";

/*
  @Purpose : To Fetch all the Requests
  @System : CBB Backend
*/

export function getPrivacyData() {
  let apiUrl = getCookie(process.env.REACT_APP_AUTH_COOKIE_NAME)
    ? getUrl("public_privacy")
    : getUrl("auth_privacy");
  let headers = getHeaders(true);
  headers["clientId"] = 1;
  return {
    types: [PRIVACY_LOADING, PRIVACY_SUCCESS, PRIVACY_FAILURE],
    promise: () => httpFetch(apiUrl, { headers: headers })
  };
}

/*
  @Purpose : To Fetch Topic Data From API
  @System : CBB Backend
*/

export function getTopicData(url) {
  let headers = getHeaders(true);
  headers["clientId"] = 1;
  return {
    types: [TOPIC_DATA_LOADING, TOPIC_DATA_SUCCESS, TOPIC_DATA_FAILURE],
    promise: () => httpFetch(url, { headers: headers })
  };
}

/*
  @Purpose : To Fetch Topic Data From API
  @System : CBB Backend
*/

export function getMoreRequest(currentTopic, clientId, page) {
  console.log(currentTopic, page);
  let url = null;
  if (currentTopic.type === "Access Request") {
    url = `http://cbclient-staging.credbox.io/api/sec/request/access?page=${page}`;
  }
  if (currentTopic.type === "Delete Request") {
    url = `http://cbclient-staging.credbox.io/api/sec/request/delete?page=${page}`;
  }
  let headers = getHeaders(true);
  headers["clientId"] = clientId;
  return {
    types: [
      REQUEST_MORE_DATA_LOADING,
      REQUEST_MORE_DATA_SUCCESS,
      REQUEST_MORE_DATA_FAILURE
    ],
    promise: () => httpFetch(url, { headers: headers })
  };
}

/*
  @Purpose : To Initiate Request
  @System : CBB Backend
*/

export function initiate(body) {
  let headers = getHeaders(true);
  headers["clientId"] = 1;

  return {
    types: [
      INITATE_REQUEST_LOADING,
      INITATE_REQUEST_SUCCESS,
      INITATE_REQUEST_FAILURE
    ],
    promise: () =>
      httpFetch(getUrl("initiateRequest"), {
        method: "POST",
        headers: headers,
        body: JSON.stringify(body)
      })
  };
}

/*
  @Purpose : Get Encrypted Data
  @System : CBB Backend
*/
export function getEncData(id, publicKey) {
  let apiUrl = `${getUrl("encData")}?requestId=${id}`;
  let headers = getHeaders(true);
  headers["key"] = publicKey;
  headers["clientId"] = 1;
  return {
    types: [
      ENCRYPTED_DATA_LOADING,
      ENCRYPTED_DATA_SUCCESS,
      ENCRYPTED_DATA_FAILURE
    ],
    promise: () =>
      httpFetch(apiUrl, {
        headers: headers
      })
  };
}

/*
  Save Topic To Redux
*/
export function saveTopic(type) {
  return {
    type: SAVE_TOPIC,
    result: type
  };
}
