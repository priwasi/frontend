// Action Type Constants
import {
  PRIVACY_LOADING,
  PRIVACY_SUCCESS,
  PRIVACY_FAILURE,
  PRIVACY_CLEAR,
  TOPIC_DATA_LOADING,
  TOPIC_DATA_SUCCESS,
  TOPIC_DATA_FAILURE,
  TOPIC_DATA_CLEAR,
  INITATE_REQUEST_LOADING,
  INITATE_REQUEST_SUCCESS,
  INITATE_REQUEST_FAILURE,
  INITATE_REQUEST_CLEAR,
  ENCRYPTED_DATA_LOADING,
  ENCRYPTED_DATA_SUCCESS,
  ENCRYPTED_DATA_FAILURE,
  ENCRYPTED_DATA_CLEAR,
  REQUEST_MORE_DATA_LOADING,
  REQUEST_MORE_DATA_SUCCESS,
  REQUEST_MORE_DATA_FAILURE,
  REQUEST_MORE_DATA_CLEAR,
  SAVE_TOPIC
} from "../../constants/actionTypes";

import {
  getLoadingState,
  getSuccessState,
  getErrorState
} from "../../store/stateSettler";

const initialState = {
  privacy: {
    loading: false,
    data: null,
    error: null
  },
  topic: {
    loading: false,
    data: null,
    error: null,
    loadMore: false
  },
  initiateRequestData: {
    loading: false,
    data: null,
    error: null
  },
  encData: {
    loading: false,
    data: null,
    error: null
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case PRIVACY_LOADING:
      return {
        ...state,
        privacy: getLoadingState("privacy", state)
      };
    case PRIVACY_SUCCESS:
      return {
        ...state,
        privacy: getSuccessState("privacy", state, action)
      };
    case PRIVACY_FAILURE:
      return {
        ...state,
        privacy: getErrorState("privacy", state, action)
      };
    case PRIVACY_CLEAR:
      return {
        ...state,
        privacy: initialState.privacy
      };
    case INITATE_REQUEST_LOADING:
      return {
        ...state,
        initiateRequestData: getLoadingState("initiateRequestData", state)
      };
    case INITATE_REQUEST_SUCCESS:
      return {
        ...state,
        initiateRequestData: getSuccessState(
          "initiateRequestData",
          state,
          action
        )
      };
    case INITATE_REQUEST_FAILURE:
      return {
        ...state,
        initiateRequestData: getErrorState("initiateRequestData", state, action)
      };
    case INITATE_REQUEST_CLEAR:
      return {
        ...state,
        initiateRequestData: initialState.initiateRequestData
      };
    case TOPIC_DATA_LOADING:
      return {
        ...state,
        topic: getLoadingState("topic", state)
      };
    case TOPIC_DATA_SUCCESS:
      let _data = state.topic && state.topic.data ? state.topic.data : {};
      _data[action.result.data.title] = action.result;

      return {
        ...state,
        topic: {
          loading: false,
          data: _data,
          error: null,
          loadMore: false
        }
      };
    case TOPIC_DATA_FAILURE:
      return {
        ...state,
        topic: getErrorState("topic", state, action)
      };
    case TOPIC_DATA_CLEAR:
      return {
        ...state,
        topic: initialState.topic
      };
    case ENCRYPTED_DATA_LOADING:
      return {
        ...state,
        encData: getLoadingState("encData", state)
      };
    case ENCRYPTED_DATA_SUCCESS:
      return {
        ...state,
        encData: getSuccessState("encData", state, action)
      };
    case ENCRYPTED_DATA_FAILURE:
      return {
        ...state,
        encData: getErrorState("encData", state, action)
      };
    case ENCRYPTED_DATA_CLEAR:
      return {
        ...state,
        encData: initialState.encData
      };
    case REQUEST_MORE_DATA_LOADING:
      return {
        ...state,
        topic: {
          ...state.topic,
          loadMore: true
        }
      };
    case REQUEST_MORE_DATA_SUCCESS:
      console.log(
        state.topic.data[state.savedTopic],
        action.result,
        state.savedTopic
      );
      let currentTopicData = { ...state.topic.data };

      let prevRequest = [
        ...currentTopicData[state.savedTopic].data.requestHistory.requests
      ];
      let nextRequest = [...action.result.data];
      let newRequestArr = prevRequest.concat(nextRequest);
      currentTopicData[
        state.savedTopic
      ].data.requestHistory.requests = newRequestArr;
      console.log(currentTopicData);
      return {
        ...state,
        topic: {
          ...state.topic,
          loadMore: false,
          data: currentTopicData
        }
      };
    case REQUEST_MORE_DATA_FAILURE:
      return {
        ...state,
        topic: {
          ...state.topic,
          error: action.error
        }
      };
    case REQUEST_MORE_DATA_CLEAR:
      return {
        ...state,
        topic: initialState.topic
      };
    case SAVE_TOPIC:
      return {
        ...state,
        savedTopic: action.result
      };
    default:
      return state;
  }
}
