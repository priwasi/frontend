import React from "react";
import "url-search-params-polyfill";
import Landing from "../Landing";

/*
  Redirect URLS
*/
let adminDomain = "http://admin.credbox.io/";
let cbClient = "http://cbclient.credbox.io/";
let loginUrl = "http://cbclient.credbox.io/api/sec/login/dummy";
if (process.env.REACT_APP_STAGE === "staging") {
  adminDomain = "http://admin-staging.credbox.io/";
  cbClient = "http://cbclient-staging.credbox.io/";
  loginUrl = "http://cbclient-staging.credbox.io/api/sec/login/dummy";
}

export default class RouteHandling extends React.PureComponent {
  getSubPartOfDeeplink = () => {
    //It will return subpart of deeplink which will be needed for further proceeding
    console.log("--2");
    const params = new URLSearchParams(window.location.search);
    const deeplinkUrl = params.get("page");

    return deeplinkUrl;
  };

  movetoLanding = () => {
    this.props.history.push("/");
  };

  componentDidMount() {
    const pageRoute = this.getSubPartOfDeeplink();
    console.log("pageRoutepageRoute", pageRoute);
    switch (pageRoute) {
      case "landing":
        return this.movetoLanding();
      default:
        return this.routeRedirector();
    }
  }

  routeRedirector = () => {
    const { history } = this.props;
    switch (window.location.href) {
      case adminDomain:
        history.push("/admin/login");
        break;
      case cbClient:
        window.location.href = loginUrl;
        break;
      default:
        return this.movetoLanding();
    }
  };
  render() {
    const { url } = this.props.match;

    return url === "/" ? <Landing /> : null;
  }
}
