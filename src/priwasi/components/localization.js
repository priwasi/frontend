const defaultLang = {
  //cashsale component
  pk_cash_mila: 'Payment mila (Cash)',
  pk_payment_paytm: 'Payment mila (Paytm)',
  pk_cash_sale: 'Cash sale (Paytm)',
  pk_udhar_diya: 'Udhaar diya',
  pk_kitne_ka_saman: 'Kitne ka saman diya',
  pk_advance_mila: 'Advance mila (Paytm) ',
  pk_bill_details: 'Bill Details',
  pk_view_bill: 'View Bill',
  pk_save_details: 'Save Details',
  pk_description: 'Description (optional)',
  pk_bill_date: 'Bill Date',
  pk_jan: 'Jan',
  pk_feb: 'Feb',
  pk_mar: 'Mar',
  pk_apr: 'Apr',
  pk_may: 'May',
  pk_jun: 'Jun',
  pk_jul: 'Jul',
  pk_aug: 'Aug',
  pk_sep: 'Sep',
  pk_oct: 'Oct',
  pk_nov: 'Nov',
  pk_dec: 'Dec',
  pk_january: 'January',
  pk_february: 'February',
  pk_march: 'March',
  pk_april: 'April',
  pk_june: 'June',
  pk_july: 'July',
  pk_august: 'August',
  pk_september: 'September',
  pk_october: 'October',
  pk_november: 'November',
  pk_december: 'December',
  //CustomerDetails
  pk_business_khata: 'Business Khata',
  pk_aap_ko_milega: 'Aapko Milenge',
  pk_download_report: 'Download Report',
  pk_new_khata_entry: 'New Khata Entry',
  //custonerkhatadetails
  pk_send_reminder: 'Send Payment Reminder',
  pk_udhar_date: 'Udhaar • Due Date',
  pk_flat_no: 'Flat No',
  pk_saman_becha: 'Saman Becha',
  pk_payment: 'Payment Mila',
  pk_balance: 'Balance',
  //error retry
  pk_no_internet: 'NoInternet',
  pk_no_internet_connectivity: 'NoInternetConnectivity',
  pk_try_again: 'tryAgain',
  pk_generic_error: 'GenericError',
  pk_retry: 'retry',
  //khatareport
  pk_receive_payment: 'Send payment reminders and receive payments',
  pk_direct_in_account: 'directly in your bank account',
  pk_notification_setting: 'Notification Settings',
  pk_download_udhar_report: 'Download Udhaar Report',
  pk_download_sales_report: 'Download Sales Report',
  pk_how_to_use: 'How to use Paytm Khata',
  pk_share_feedback: 'Share Feedback',
  pk_daily_khata: 'Daily Khata',
  pk_customer_khata: 'Customer Khata',
  pk_track: 'Paytm Business Khata enables you to easily track',
  pk_udhar_sales: 'daily sales and customer udhaar',
  //newkhata
  pk_add_photo: 'Add Bill Photo',
  pk_today: 'Today',
  pk_proceed: 'Proceed',
  pk_advance: 'Advance',
  pk_udhar: 'Udhaar',
  pk_customer_name: 'Customer Name',
  pk_mobile_no: 'Mobile Number (optional)',
  //notification
  pk_sms: 'SMS',
  pk_automatic_reminders: 'Send automatic reminders to collect udhaar',
  pk_remind: 'Remind Customer to Pay',
  pk_send_notification:
    'Send customer notification for all udhaar and payment entries',
  pk_of_month: 'of every month',
  //report
  pk_udhar_khata_report: 'Udhaar Khata Report',
  pk_close_balance: 'Closing Balance',
  pk_payment_receive: 'Payments Received',
  pk_total_udhar: 'Total Udhaar',
  pk_open_balance: 'Opening Balance',

  //contactsearch

  //saleReport
  pk_date: 'Date',
  pk_paymode: 'Paymode',
  pk_amount: 'Amount',
  pk_sales_report: 'Sales Report',
  pk_sales_udhar: 'Udhaar Sales',
  pk_sales_cash: 'Cash Sales',
  pk_total_sales: 'Total Sales',

  //index
  pk_generic_error_msg: 'Oops, something went wrong! Please try again.',
  //feedback
  pk_share_feedback_suggestion:
    'Share your feedback / suggestion to improve paytm business khata'
};

const constants = {
  en: {
    ...defaultLang
  }
};

export default lang => constants[lang];
