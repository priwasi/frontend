/*
    Private Route Component
    Checks if route is protected or not  
*/
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { withRouter, Redirect } from "react-router-dom";
import { Route } from "react-router-dom";
import LazyloadComponent from "./LazyloadComponent";
import { bindActionCreators } from "redux";
import { getUserInfo, getAllPermissions } from "../store/actions";
import { getCookie, deleteCookie } from "../storage/cookie";
import DotLoader from "../shared/DotLoader";

class PrivateRoute extends React.Component {
  state = {
    userData: false
  };
  componentDidMount = () => {
    const { getUserInfo, userInfo } = this.props;
    if (!userInfo.data) {
      getUserInfo();
    }
  };

  componentDidUpdate = (prevProps, prevState) => {
    const { userInfo, getAllPermissions } = this.props;
    if (
      prevProps.userInfo.loading &&
      userInfo.data &&
      userInfo.data.code === 200
    ) {
      getAllPermissions(userInfo.data.data.cliendId);
    }
    if (prevProps.userInfo.loading && userInfo.error) {
      this.cleanUpAndRedirect();
    }
  };

  /*
    Check if user is logged in and is valid or not
*/
  checkAccess = userData => {
    const { userInfo } = this.props;
    if (userInfo.data) {
      return true;
    }
    return false;
  };

  /*
        Clean up the stale cookie and Redirect
    */
  cleanUpAndRedirect = () => {
    const { history } = this.props;
    if (getCookie(process.env.REACT_APP_AUTH_COOKIE_NAME)) {
      deleteCookie(process.env.REACT_APP_AUTH_COOKIE_NAME);
    }
    history.push("/admin/login");
  };

  render() {
    const { userData } = this.state;
    // Access Flag check

    let access = this.checkAccess(userData);
    return (
      <Route
        path={this.props.path}
        render={props =>
          access ? (
            <LazyloadComponent component={this.props.component} {...props} />
          ) : (
            <DotLoader />
          )
        }
        key={this.props.key}
        exact
      />
    );
  }
}

const mapStateToProps = (state, prevProps) => {
  return {
    userInfo: state.app.userInfo,
    allPermissions: state.app.allPermissions
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getUserInfo,
      getAllPermissions
    },
    dispatch
  );
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(PrivateRoute)
);
