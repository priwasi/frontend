export const protectedRoutes = [
  "request",
  "requestdetails",
  "requestpreferences",
  "users",
  "dashboard",
  "policyInfo",
  "silos",
  "changePassword",
  "connectorinstall"
];
