import * as R from "ramda";

const routes = {
  routeHandling: {
    path: "/",
    name: "routeHandling"
  },
  home: {
    path: "/home",
    name: "home"
  },
  privacy: {
    path: "/privacy",
    name: "privacy"
  },
  request: {
    path: "/platform/request",
    name: "request",
    protected: true
  },
  silos: {
    path: "/platform/silos",
    name: "silos"
  },
  requestdetails: {
    path: "/platform/:id/:type",
    name: "requestdetails"
  },
  requestpreferences: {
    path: "/settings/controlpanel",
    name: "controlpanel"
  },
  policyInfo: {
    path: "/settings/policy",
    name: "policyInfo"
  },
  users: {
    path: "/settings/users",
    name: "users"
  },
  dashboard: {
    path: "/platform",
    name: "dashboard"
  },
  changepassword: {
    path: "/platform/changepassword",
    name: "changepassword"
  },
  adminLogin: {
    path: "/admin/login",
    name: "adminLogin"
  },
  activate: {
    path: "/activate",
    name: "activate"
  },
  forgotpassword: {
    path: "/forgotpassword",
    name: "forgotpassword"
  },
  connectorinstall: {
    path: "/connectors/:name/code/:code",
    name: "connectorinstall"
  }
};
export default routes;
export const getPathByKey = key => routes[key].path;
export const getPathByName = name => {
  const findPathByName = R.compose(
    R.prop("path"),
    R.find(R.propEq("name", name)),
    R.values
  );
  return findPathByName(routes);
};
