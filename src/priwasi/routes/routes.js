import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import { withRouter } from "react-router-dom";
// import Loadable from 'react-loadable';
import { onRouteChangeEvent } from "./onRoutesChange";
import { getPathByName } from "./routerManager";
// import ErrorRetry from "../components/ErrorRetry/ErrorRetry";
import RouteHandlingComponent from "../components/RouteHandling";
import LazyloadComponent from "./LazyloadComponent";
import AsyncPageNotFound from "../components/PageNotFound";
import { protectedRoutes } from "./protectedRoutes";
import PrivateRoute from "../routes/PrivateRoute";

// const LoadingFallback = (props) => {
//   if (props.error) {
//     return <ErrorRetry error retryAction={props.retry.bind(this)} />;
//   }
//   return null;
// };

const AsyncWebsiteLanding = import(
  /* webpackChunkName: "priwasi_landing" */ "../components/Landing/LandingContainer"
);
const AsyncPrivacy = import(
  /* webpackChunkName: "async_privacy" */ "../components/Privacy/PrivacyContainer"
);

const AsyncRequest = import(
  /* webpackChunkName: "credbox_platform_request" */ "../components/Platform/Request/RequestContainer"
);

const AsyncSilos = import(
  /* webpackChunkName: "credbox_platform_silos" */ "../components/Platform/Slios/SilosContainer"
);
const AsyncRequestDetails = import(
  /* webpackChunkName: "credbox_platform_request_details" */ "../components/Platform/RequestDetails/RequestDetailsContainer"
);
const AsyncRequestPreferences = import(
  /* webpackChunkName: "credbox_platform_request_preferences" */ "../components/Platform/RequestPreferences/RequestPreferencesContainer"
);
const AsyncUsers = import(
  /* webpackChunkName: "credbox_platform_users" */ "../components/Platform/Users"
);
const AsyncDashboard = import(
  /* webpackChunkName: "credbox_platform_dashboard" */ "../components/Platform/Dashboard"
);

const AsyncLogin = import(
  /* webpackChunkName: "credbox_platform_login" */ "../components/Login"
);

const AsyncPolicyInfo = import(
  /* webpackChunkName: "credbox_platform_policy_info" */ "../components/Platform/PolicyInfo"
);

const AsyncActivate = import(
  /* webpackChunkName: "credbox_platform_activate" */ "../components/Platform/Activate"
);

const AsyncChangePassword = import(
  /* webpackChunkName: "credbox_platform_change_password" */ "../components/Platform/ChangePassword"
);

const AsyncForgotPassword = import(
  /* webpackChunkName: "credbox_platform_forgot_password" */ "../components/ForgotPassword"
);
const AsyncConnectRedirect = import(
  /* webpackChunkName: "credbox_platform_connector_redirect" */ "../components/Platform/ConnectorInstall/ConnectorInstallContainer"
);

const RouteHandling = {
  path: getPathByName("routeHandling"),
  component: RouteHandlingComponent,
  exact: true
};

/*
  Check if given route is Protected or Not
  @params : routeName = String;
*/
const getProtectedRoutes = routeName => {
  return protectedRoutes.includes(routeName) ? true : false;
};

export const routes = [
  {
    path: getPathByName("home"),
    component: AsyncWebsiteLanding,
    exact: true,
    protected: getProtectedRoutes("home")
  },
  {
    path: getPathByName("privacy"),
    component: AsyncPrivacy,
    exact: true,
    protected: getProtectedRoutes("privacy")
  },
  {
    path: getPathByName("request"),
    component: AsyncRequest,
    exact: true,
    protected: getProtectedRoutes("request")
  },
  {
    path: getPathByName("silos"),
    component: AsyncSilos,
    exact: true,
    protected: getProtectedRoutes("silos")
  },
  {
    path: getPathByName("requestdetails"),
    component: AsyncRequestDetails,
    exact: true,
    protected: getProtectedRoutes("requestdetails")
  },
  {
    path: getPathByName("controlpanel"),
    component: AsyncRequestPreferences,
    exact: true,
    protected: getProtectedRoutes("requestpreferences")
  },
  {
    path: getPathByName("users"),
    component: AsyncUsers,
    exact: true,
    protected: getProtectedRoutes("users")
  },
  {
    path: getPathByName("dashboard"),
    component: AsyncDashboard,
    exact: true,
    protected: getProtectedRoutes("dashboard")
  },
  {
    path: getPathByName("adminLogin"),
    component: AsyncLogin,
    exact: true,
    protected: getProtectedRoutes("adminLogin")
  },
  {
    path: getPathByName("policyInfo"),
    component: AsyncPolicyInfo,
    exact: true,
    protected: getProtectedRoutes("policyInfo")
  },
  {
    path: getPathByName("activate"),
    component: AsyncActivate,
    exact: true,
    protected: getProtectedRoutes("activate")
  },
  {
    path: getPathByName("changepassword"),
    component: AsyncChangePassword,
    exact: true,
    protected: getProtectedRoutes("changepassword")
  },
  {
    path: getPathByName("forgotpassword"),
    component: AsyncForgotPassword,
    exact: true,
    protected: getProtectedRoutes("forgotpassword")
  },
  {
    path: getPathByName("connectorinstall"),
    component: AsyncConnectRedirect,
    exact: true,
    protected: getProtectedRoutes("connectorinstall")
  }
];

class Router extends Component {
  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      onRouteChangeEvent("data from route changes");
    }
  }

  render() {
    return (
      <React.Fragment>
        <Switch>
          <Route
            path={RouteHandling.path}
            component={RouteHandling.component}
            exact={RouteHandling.exact}
          />
          {routes.map((r, i) => {
            if (r.protected) {
              return (
                <PrivateRoute
                  path={r.path}
                  component={r.component}
                  key={`${i}_privateRoute`}
                  exact
                  {...this.props}
                />
              );
            }
            return (
              <Route
                path={r.path}
                render={props => (
                  <LazyloadComponent component={r.component} {...props} />
                )}
                key={i}
                exact
              />
            );
          })}
          <Route component={AsyncPageNotFound} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default withRouter(Router);
