/*
  @Module : CredBox
  @Component : Access Control
  @Type : Service
  @Description : Access Control
  @Author : Kaustubh Saxena 
*/

// React Imports
import React, { Component } from "react";
import { connect } from "react-redux";

import { permissions } from "../../constants/permissions";
import AccessDenied from "./AccessDenied/AccessDenied";

// Component Imports

// Stylesheet Imports
import s from "./AccessControl.module.scss";

// Asset & Utility Imports

// Node Modules Import
import ReactTooltip from "react-tooltip";

/*
    Check For Permissions
    @params metaKey = String, currentPermissions=Array
*/
const checkForPermissions = (metaKey, currentPermissions) => {
  const found = currentPermissions.includes(metaKey);

  return found;
};

/*
    Render Denied
    @params props=Object
*/

const renderDenied = props => {
  if (props.fullScreen) {
    return <AccessDenied />;
  }
  if (props.noAction) {
    return false;
  }
  if (props.disabled) {
    return (
      <>
        <ReactTooltip />
        <span
          data-tip={`You don’t currently have permission to add requests.
                <br />
                Please contact your administrator to get permission`}
          data-place={"right"}
          data-multiline={true}
          className={`${s.overlappedDiv} ${
            props.moduleStyle ? props.moduleStyle : s.dimensions
          }`}
        ></span>
        {props.children}
      </>
    );
  }
};

const AccessControl = props => {
  const { userInfo } = props;
  if (
    !userInfo.loading &&
    userInfo.data &&
    userInfo.data.data &&
    userInfo.data.data.permissions
  ) {
    let permissions = userInfo.data.data.permissions;
    if (checkForPermissions(props.meta, permissions)) {
      return props.children;
    } else {
      return renderDenied(props);
    }
  }
  return null;
};

/*
    For Applying Access Control on Method Action Invocation.
    This Method Restricts the Access of any action to be
    performed if user dont have permission.
    Uses Same Inner Functionality API as <AccessControl />

    @params = meta:String, method:Function
*/
export const noActionAccessControl = (meta, method, userInfo, args) => {
  if (
    !userInfo.loading &&
    userInfo.data &&
    userInfo.data.data &&
    userInfo.data.data.permissions
  ) {
    let permissions = userInfo.data.data.permissions;
    if (checkForPermissions(meta, permissions)) {
      return args ? method(...args) : method();
    } else {
      let config = {
        noAction: true
      };
      return renderDenied(config);
    }
  }
  return null;
};

const mapStateToProps = state => ({
  userInfo: state.app.userInfo
});

export default connect(mapStateToProps)(AccessControl);
