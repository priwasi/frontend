/*
  @Module : CredBox
  @Component : Access Denied
  @Type : Component
  @Description : Access Denied
  @Author : Kaustubh Saxena 
*/

// React Imports
import React, { Component } from "react";

// Component Imports

// Stylesheet Imports

// Asset & Utility Imports

// Node Modules Imports

const AccessDenied = props => {
  return <div>Restricted Access</div>;
};

export default AccessDenied;
