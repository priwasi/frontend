// React Imports
import React from 'react';

// Node Modules Imports
import PropTypes from 'prop-types';

// Style Imports
import s from './BaseListItem.module.scss';

// Utility Imports
import cx from '../../utils/classNames';
import { getImage } from '../../utils/AppUtils';

const BaseListItem = props => {
  return (
    <div
      onClick={props.onClick}
      className={cx(s.baseListItem, props.textClass, s[props.extraPadding])}
    >
      <img
        className={`${s.rightArrow} ${s.leftIcon}`}
        src={getImage(props.leftIconType)}
        alt="leftIcon"
      />
      <span className={s.headingText}>{props.title}</span>
      <img className={s.rightArrow} src={getImage(props.iconType)} alt=">" />
    </div>
  );
};
BaseListItem.propTypes = {
  title: PropTypes.string,
  iconType: PropTypes.string,
  onClick: PropTypes.func,
  textClass: PropTypes.string
};

BaseListItem.defaultProps = {
  title: '',
  textClass: '',
  iconType: '',
  onClick: () => {}
};

export default BaseListItem;
