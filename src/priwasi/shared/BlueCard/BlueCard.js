// React Imports
import React from 'react';

// Node Modules Imports
// import PropTypes from 'prop-types';
/*
blue colour header component
*/

// Style Imports
import s from './BlueCard.module.scss';

const BlueCard = props => {
  return <div className={s.bluecard}>{props.children}</div>;
};

export default BlueCard;
