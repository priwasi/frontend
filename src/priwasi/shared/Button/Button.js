import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import s from './Button.module.scss';

export default class Button extends PureComponent {
  getButtonStatus = () => {
    const { buttonStatus } = this.props;
    if (buttonStatus) {
      switch (buttonStatus) {
        case '_DEBIT':
          return `${s.orangeBtn}`;
        case '_CREDIT':
          return `${s.greenBtn}`;
        default:
          return false;
      }
    }
  };

  render() {
    const {
      onClickHandler,
      isDisabled,
      btnText,
      classNamesStr,
      type,
      size,
      btnType,
      opaqueFactor,
    } = this.props;
    const typeClass = `is-${type}`;
    const sizeClass = `is-${size}`;
    const catClassNamesStr = `${s.btn} ${classNamesStr} ${s[typeClass]} ${s[sizeClass]}`;
    let cssClasses = isDisabled
      ? `${s['is-disabled']} ${catClassNamesStr}`
      : `${catClassNamesStr} ${this.getButtonStatus()}`;
    
    return (
      <button
        onClick={onClickHandler}
        disabled={isDisabled}
        className={cssClasses}
        type={btnType}
        opacity={opaqueFactor}
      >
        {btnText}
      </button>
    );
  }
}

Button.propTypes = {
  btnText: PropTypes.string.isRequired
};

Button.defaultProps = {
  classNamesStr: '', // BY default, Button will have primary btn css
  isDisabled: false,
  type: 'primary',
  size: 'large',
  btnType: 'button'
};
