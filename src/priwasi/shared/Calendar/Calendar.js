/*
    Generic Calendar Component Library
    Can be Hooked up to any react component
    Completely customizable
    Originally written for P4B Insurance Integration

*/

import React, { Component } from 'react';
import './Calendar.scss';
import {
  DAYS_ARRAY_MON,
  MONTHS_ARRAY_SHORT
} from '../../constants/dateConstants';
import classNames from '../../utils/classNames';
import { daysBetween } from '../../utils/dateUtils';
import { formatDate } from '../../utils/dateUtils';

class Calendar extends Component {
  state = {
    currentDate: null,
    selectedDate: null,
    selectedStartDate: null,
    selectedEndDate: null,
    dateRangeError: null,
    selectStartDateBlock: true,
    selectEndDateBlock: false,
    //
    yearsView: false,
    currentYear: false,
    years: false
  };

  componentDidMount = () => {
    const { prevSelectedStart, prevSelectedEnd, range } = this.props;
    var elmnt = document.getElementById('currentMonth');
    if (elmnt) {
      elmnt.scrollIntoView(true);
    }

    if (range && prevSelectedStart) {
      this.setState({ selectedStartDate: prevSelectedStart });
    }
    if (range && prevSelectedEnd) {
      this.setState({ selectedEndDate: prevSelectedEnd });
    }
  };
  isValidDate = d => {
    return d instanceof Date && !isNaN(d);
  };
  /*
        Method only for invalid date format from backend,
        (D-M-Y) to ISO 8601 compatible format.
    */

  _formatDate = (date, type) => {
    switch (type) {
      case 'timeStamp':
        let timestampDateArr = date.split('+');
        let _newDate = timestampDateArr[0].replace('T', ' ');
        return _newDate;
      case 'timeStamp_Safari':
        var timeStampSafariArr = date.split(/\D/);
        return new Date(
          Date.UTC(
            timeStampSafariArr[0],
            --timeStampSafariArr[1],
            timeStampSafariArr[2],
            timeStampSafariArr[3],
            timeStampSafariArr[4],
            timeStampSafariArr[5]
          )
        );
      default:
        let dateArr = date.split('-');
        dateArr.reverse();
        let newDate = '';
        dateArr.map((item, index) => {
          if (index !== dateArr.length - 1) {
            newDate += `${item}-`;
          } else {
            newDate += `${item}`;
          }
          return null;
        });
        return newDate;
    }
  };

  _getCurrentCalendar = () => {
    const { currentYear } = this.state;
    const { pastLimit, max, lowLimitStart } = this.props;

    var date = new Date();
    var now = new Date(new Date().getFullYear(), 0, 1);
    // var duedate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    var duedate = new Date();
    if (max && !lowLimitStart) {
      let maxLimit = max;
      if (!this.isValidDate(max)) {
        maxLimit = this._formatDate(max, 'timeStamp');
      }
      let _limitDate = new Date(maxLimit);
      let _starting = new Date(_limitDate.getFullYear(), 0, 1);
      now = _starting;
      duedate = _limitDate;
    }
    if (pastLimit && lowLimitStart) {
      let maxLimit = pastLimit;
      if (!this.isValidDate(pastLimit)) {
        maxLimit = this._formatDate(pastLimit, 'timeStamp_Safari');
      }

      let _starting = new Date(maxLimit);
      let _limitDate = new Date(_starting.getFullYear(), 11, 31);

      now = _starting;
      duedate = _limitDate;
    }

    if (currentYear) {
      now = new Date(currentYear, 0, 1);

      if (new Date().getFullYear() === currentYear) {
        let currentMonth = new Date().getMonth();
        let currentDay = new Date().getDate();
        duedate = new Date(currentYear, currentMonth, currentDay);
      } else {
        /*
                    Logic For Max Calendar Capping 
                    When Max Date is Given
                */

        duedate = new Date(currentYear, 12, 0);
        let max_capping = this.props.max;
        if (!this.isValidDate(max)) {
          max_capping = new Date(this._formatDate(max, 'timeStamp'));
        }

        /*
                    Comparing Current Year and Max Capping Year
                */
        if (currentYear === max_capping.getFullYear()) {
          duedate = max_capping;
        }
      }
    }

    const minDate = now;
    const maxDate = duedate;
    const noOfDays = daysBetween(minDate, maxDate);
    let i = 0,
      monthCalendar = [],
      pastMonthCalendar = [];
    date = new Date(minDate.getTime());
    const todayDate = new Date();
    const monthDiff = todayDate.getMonth() - date.getMonth();

    while (i <= noOfDays) {
      let unqIndex =
        date.getMonth() +
        12 * (date.getFullYear() - todayDate.getFullYear()) -
        todayDate.getMonth() +
        monthDiff;
      if (unqIndex < 0) {
        let pastUnqIndex = unqIndex * -1;

        if (!pastMonthCalendar[pastUnqIndex]) {
          pastMonthCalendar[pastUnqIndex] = [];
        } else {
          pastMonthCalendar[pastUnqIndex].push(new Date(date));
          date.setDate(date.getDate() + 1);
          i++;
        }
      }
      if (unqIndex >= 0) {
        if (!monthCalendar[unqIndex]) {
          monthCalendar[unqIndex] = [];
        } else {
          monthCalendar[unqIndex].push(new Date(date));
          date.setDate(date.getDate() + 1);
          i++;
        }
      }
    }

    pastMonthCalendar.reverse();

    let pastMonthsView = pastMonthCalendar.map((month, index) => {
      return this.createMonth(month, index);
    });
    let monthsView = monthCalendar.map((month, index) => {
      var currentdate = new Date();
      var thisMonth = currentdate.getMonth();
      /* Needs to Be checked and Optimised */
      let currentMonth = index === thisMonth ? true : false;

      return this.createMonth(month, index, currentMonth);
    });

    return (
      <div className="month-views">
        <div className="container">
          {pastMonthsView}
          {monthsView}
        </div>
      </div>
    );
  };

  showAllYears = () => {
    const { pastLimit, max } = this.props;
    let years = [];

    let pastYearLimit = new Date(pastLimit).getFullYear();
    if (!pastLimit) {
      pastYearLimit = new Date().getFullYear();
    }
    let currentYear = new Date().getFullYear();
    if (max) {
      currentYear = new Date(max).getFullYear();
    }
    // if(max){
    //     currentYear = new Date(max).getFullYear();
    // }
    for (var i = pastYearLimit; i <= currentYear; i++) {
      years.push(i);
    }
    this.setState({ yearsView: true, years: years });
  };

  createMonth(monthArray, index, currentMonth = false) {
    const fullMonth = monthArray;

    if (fullMonth.length > 0) {
      let emptyDates;
      const counter = [0, 1, 2, 3, 4, 5, 6];
      let start_day = (fullMonth[0].getDay() + 6) % 7; //to start day with Mon instead of Sunday
      if (start_day !== 0) {
        emptyDates = counter.map((count, index) => {
          if (count < start_day) {
            return (
              <div className="cal-day" key={index}>
                <p></p>
              </div>
            );
          }
          return null;
        });
      }

      const fullMonthView = fullMonth.map((date, index) => {
        return this.createDay({ date: date }, index);
      });
      const month = fullMonth[0];
      const monthId =
        MONTHS_ARRAY_SHORT[month.getMonth()] + month.getFullYear();

      return (
        <div className="fromSecond-monthViews" key={index}>
          <hr className="divider" />
          <div
            className={classNames('cal-monthHead', 'row')}
            onClick={this.showAllYears}
            id={`${currentMonth ? 'currentMonth' : ''}`}
          >
            {`${MONTHS_ARRAY_SHORT[month.getMonth()]} ${month.getFullYear()}`}
            {/* <span className="cal-monthName">{`${MONTHS_ARRAY_SHORT[month.getMonth()]} `}</span>
                        <span className="cal-yaerName">{month.getFullYear()}</span> */}
          </div>
          <div
            className={classNames('cal-monthView', 'row')}
            id={monthId}
            ref={c => {
              this[monthId] = c;
            }}
          >
            <div className="cal-mv-weekRow">
              {emptyDates}
              {fullMonthView}
            </div>
          </div>
        </div>
      );
    }
    return null;
  }
  onDateClick = date => {
    const { range } = this.props;
    const {
      selectedStartDate,
      selectStartDateBlock,
      selectEndDateBlock
    } = this.state;
    this.setState({ dateRangeError: false });
    if (range) {
      if (selectStartDateBlock) {
        this.setState({
          selectedStartDate: date,
          selectedDate: date,
          selectEndDateBlock: true,
          selectStartDateBlock: false
        });
      }
      if (selectEndDateBlock) {
        if (
          selectedStartDate &&
          (this.compareDates(date, selectedStartDate) === 1 ||
            this.compareDates(date, selectedStartDate) === 0)
        ) {
          this.setState({ selectedEndDate: date, selectedDate: date });
        } else {
          if (!selectStartDateBlock && !selectedStartDate) {
            this.setState({ dateRangeError: 'Please Select Start Date' });
          } else {
            this.setState({
              dateRangeError: 'End Date Should be greater than Start Date'
            });
          }
        }
      }
    } else {
      this.setState({ selectedDate: date });
    }
  };
  createDay(config, index) {
    let checkInClass = '',
      checkOutClass = '',
      inBetweenClass = '',
      currentDateClass = '';
    const { selectedStartDate, selectedEndDate } = this.state;

    if (
      this.props.currentSelectedDate &&
      this.compareDates(
        new Date(this.props.currentSelectedDate),
        config.date
      ) === 0
    ) {
      currentDateClass = 'prevSelection';
    }
    if (
      this.state.selectedDate &&
      this.compareDates(this.state.selectedDate, config.date) === 0
    ) {
      currentDateClass = 'currentDate';
    }

    if (
      selectedStartDate &&
      selectedEndDate &&
      this.compareDates(config.date, selectedStartDate) === 0
    ) {
      inBetweenClass = 'checkinDate';
    }
    if (
      selectedStartDate &&
      selectedEndDate &&
      this.compareDates(config.date, selectedStartDate) === 1 &&
      this.compareDates(config.date, selectedEndDate) === -1
    ) {
      inBetweenClass = 'inbetweenDate';
    } else if (
      selectedEndDate &&
      this.compareDates(selectedEndDate, config.date) === 0
    ) {
      inBetweenClass = 'checkoutDate';
      if (
        selectedStartDate &&
        this.compareDates(selectedEndDate, selectedStartDate) === 0
      ) {
        inBetweenClass = 'sameRangeDate';
      }
    }

    let dateClasses = classNames(
      'cal-day',
      currentDateClass,
      checkInClass,
      checkOutClass,
      inBetweenClass
    );
    return (
      <div
        onClick={() => {
          this.onDateClick(config.date);
        }}
        className={dateClasses}
        key={index}
      >
        <p>
          <span>{config.date.getDate()}</span>
        </p>
      </div>
    );
  }

  compareDates(dateFirst, dateSecond) {
    dateFirst.setHours(0, 0, 0, 0);
    dateSecond.setHours(0, 0, 0, 0);
    if (dateFirst.getTime() === dateSecond.getTime()) {
      return 0;
    } else if (dateFirst < dateSecond) {
      return -1;
    } else if (dateFirst > dateSecond) {
      return 1;
    }
  }

  submitDate = () => {
    const { selectedDate, selectedEndDate, selectedStartDate } = this.state;
    const { config_key, manageCalendarCallback, range } = this.props;

    let calendarValue = {};
    if (range) {
      if (selectedEndDate) {
        let finalStartDate = selectedStartDate;
        let finalEndDate = selectedEndDate;
        /*
                    If Start Date and End Date Are same,
                    then the Range will be from Starting of the day to Ending of the day
                */
        if (this.compareDates(selectedEndDate, selectedStartDate) === 0) {
          let _endOfDay = new Date(
            selectedEndDate.setHours(selectedEndDate.getHours() + 23)
          );
          _endOfDay = new Date(
            _endOfDay.setMinutes(_endOfDay.getMinutes() + 59)
          );
          finalEndDate = _endOfDay;
        }

        calendarValue[config_key] = {
          start_date: finalStartDate ? finalStartDate : new Date(),
          end_date: finalEndDate
        };
        manageCalendarCallback(calendarValue);
      } else {
        this.setState({ dateRangeError: 'Please Select End Date' });
      }
    } else {
      if (selectedDate) {
        calendarValue[config_key] = selectedDate;
        manageCalendarCallback(calendarValue);
      } else {
        this.setState({ dateRangeError: 'Please Select A Date' });
      }
    }
  };
  handleValidation = date => {
    if (!date) {
      return false;
    } else {
      return true;
    }
  };

  handleStartDate = () => {
    this.clearDate();
    this.setState({ selectStartDateBlock: true, selectEndDateBlock: false });
  };

  handleEndDate = () => {
    this.setState({ selectStartDateBlock: false, selectEndDateBlock: true });
  };

  clearDate = () => {
    this.setState({ selectedEndDate: null, selectedStartDate: null });
  };

  onYearSelect = year => {
    this.setState({ yearsView: false, currentYear: year });
  };

  getYearsView = () => {
    const { years } = this.state;
    let yearsDom = [];
    years.map((year, index) => {
      yearsDom.push(
        <span onClick={() => this.onYearSelect(year)} key={`${index}_${year}`}>
          {year}
        </span>
      );
      return null;
    });
    return yearsDom;
  };

  render() {
    const {
      selectedStartDate,
      selectedEndDate,
      dateRangeError,
      selectStartDateBlock,
      selectEndDateBlock
    } = this.state;
    const { range, onCloseHandler } = this.props;
    const { yearsView } = this.state;
    return (
      <div className="pos-relative calendar calendar-container">
        <div className={'closeIconDiv'}>
          {/* <i className={'cross closeIcon'} onClick={onCloseHandler}> */}
          <span onClick={onCloseHandler}>X</span>
          {/* </i> */}
        </div>

        <div className="addValiditySection">
          {range && (
            <div className="dateBlock">
              <div
                className={`startDateBlock ${
                  selectStartDateBlock ? 'highlightBorder' : null
                }`}
                onClick={this.handleStartDate}
              >
                <span>
                  Start Date -{' '}
                  {selectedStartDate
                    ? formatDate(new Date(selectedStartDate))
                    : formatDate(new Date())}
                </span>
              </div>
              <div
                className={`endDateBlock ${
                  selectEndDateBlock ? 'highlightBorder' : null
                }`}
                onClick={this.handleEndDate}
              >
                <span>
                  End Date -{' '}
                  {selectedEndDate
                    ? formatDate(selectedEndDate)
                    : formatDate(new Date())}
                </span>
              </div>
            </div>
          )}

          {dateRangeError && (
            <span className="dateErrorMsg">{dateRangeError}</span>
          )}
        </div>

        <div
          ref={c => {
            this.outerContainer = c;
          }}
        >
          {yearsView && (
            <div className="fullCalender yearsView">{this.getYearsView()}</div>
          )}

          {!yearsView && (
            <>
              <div className={classNames('cal-dayHead', 'row')}>
                {DAYS_ARRAY_MON.map((day, i) => (
                  <div className="cal-dayName" key={i}>
                    <p>{day}</p>
                  </div>
                ))}
              </div>
              <div
                className="fullCalender"
                ref={c => {
                  this.fullCalender = c;
                }}
              >
                {this._getCurrentCalendar()}
              </div>
              <div className="floatingButton">
                <button onClick={this.submitDate} className={`button`}>
                  {' '}
                  <span>&#8250;</span>
                </button>
              </div>
            </>
          )}
        </div>
        {/* {
          this.state.checkOutDateError &&
          this.createErrorMessage()
        } */}
      </div>
    );
  }
}

export default Calendar;
