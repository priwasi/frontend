// React Import
import React from 'react';

// Styles and Asset Import
import s from './Card.module.scss';
import voucher from '../../public/images/qrcodeVoucher.svg';

// Utility Imports
import { getCurrencySymbol } from '../../utils/helper';

const Card = props => {
  const { amount, expiry, text, currency } = props;
  return (
    <div className={s.CardContainer}>
      <div className={s.CardInnerContainer}>
        <div className={s.title}>Voucher Worth</div>
        <div className={s.amountParent}>
          <span>{`${getCurrencySymbol(currency)}${amount}`}</span>
          <span>{`${text}`}</span>
          <span>{`Valid for ${expiry}`}</span>
        </div>
      </div>
      <div>
        <img src={voucher} alt={'Voucher'} />
      </div>
    </div>
  );
};
export default Card;
