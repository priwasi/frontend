// React Import
import React from "react";

// Styles and Asset Import
import s from "./Checkbox.module.scss";

const Checkbox = props => {
  return (
    <div className={`${s.formElemWrapper} ${s.checkBoxWrapper}`}>
      <label className={s.checkboxContainer}>
        <input
          type="checkbox"
          name={props.name}
          onChange={props.handleChange}
          checked={props.checked}
        />
        <span className={s.checkMark}></span>
      </label>
      <span className={`${s.label} ${s.checkboxLabel}`}>
        {props.label && props.label}
      </span>
    </div>
  );
};
export default Checkbox;

Checkbox.defaultProps = {
  name: "",
  label: "",
  handleChange: () => {}
};
