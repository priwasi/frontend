import React from "react";
import s from "./DotLoader.module.scss";
import loader from "../../public/priwasi/loader.svg";

export default class DotLoader extends React.PureComponent {
  render() {
    return (
      <div className={s.wrapper}>
        <div className={s.spinner}>
          <div className={s.bounce1}></div>
          <div className={s.bounce2}></div>
          <div className={s.bounce3}></div>
          <div className={s.bounce4}></div>
          <div className={s.bounce5}></div>
        </div>
      </div>
    );
  }
}
