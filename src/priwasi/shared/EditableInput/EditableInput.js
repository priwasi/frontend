/*
  @Module : CredBox
  @Component : Editable Input
  @Type : Shared
  @Description : Editable Input
  @Author : Kaustubh Saxena
*/

// React Imports
import React, { useState } from "react";

// Styles Imports
import s from "./EditableInput.module.scss";

const handleChange = (e, stateObject) => {
  stateObject.setInputValue(e.target.value);
};

const EditableInput = props => {
  const { value, index } = props;
  const [inputValue, setInputValue] = useState(value);
  return (
    <div className={s.container}>
      <input
        type={"text"}
        name={"editInput"}
        // value={inputValue ? inputValue : value}
        defaultValue={inputValue}
        onChange={e => handleChange(e, { inputValue, setInputValue })}
        autoFocus
        onBlur={() => props.closeEditInput(index, inputValue)}
      />
      {/* <div className={s.actionButtons}>
          <span>&#10004;</span>
        </div> */}
    </div>
  );
};

export default EditableInput;
