/*
  @Module : CredBox
  @Component : WYSIWYG Editor
  @Type : Shared
  @Description : Rich Text Editor
  @Author : Kaustubh Saxena 
*/

// React Imports
import React, { Component } from "react";

// Node Module Imports
import ReactQuill from "react-quill";

// Styles Imports
import s from "./EditorComponent.module.scss";
import "react-quill/dist/quill.snow.css";
import "./editor-overrides.scss";

const EditorComponent = props => {
  const { value, index } = props;
  return (
    <div className={s.container}>
      <span className={s.close} onClick={() => props.closeEditor(index, value)}>
        Done
      </span>
      <ReactQuill
        style={{ resize: "vertical" }}
        value={value}
        onChange={value => props.onChangeHandler(value, props.index)}
      />
    </div>
  );
};

export default EditorComponent;
