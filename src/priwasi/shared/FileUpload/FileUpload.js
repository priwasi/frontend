// React Import
import React from "react";

// Styles and Asset Import
import s from "./FileUpload.module.scss";

const FileUpload = props => {
  const { name, onChangeHandler, data } = props;
  return (
    <div className={s.container}>
      <div className={s.fileUploadWrapper}>
        <button type={"button"} className={s.btn}>
          Upload
        </button>
        <input
          type="file"
          name={name}
          onChange={e => onChangeHandler(e, data)}
        />
      </div>
    </div>
  );
};
export default FileUpload;
