// React Import
import React from 'react';

// Styles and Asset Import
import s from './FilterPanel.module.scss';

// Component Imports
import Pill from '../Pill';

const FilterPanel = props => {
  const { options, currentFilter, white, border } = props;
  let classNameStr = `${s.filterPanelContainer} ${border ? s.border : ''} ${
    white ? s.white : ''
  }`;
  return (
    <div className={classNameStr} style={props.styleConfig}>
      {options &&
        options.map((item, index) => {
          return (
            <Pill
              key={`${index}_pill`}
              text={item.name}
              config_key={item.config_key}
              index={`${index}_pill_instance`}
              active={currentFilter === item.config_key && true}
              white={true}
              onClickHandler={props.onClickAction}
            />
          );
        })}

      {props.children && props.children}
    </div>
  );
};
export default FilterPanel;
