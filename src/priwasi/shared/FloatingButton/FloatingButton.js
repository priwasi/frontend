// React Imports
import React from 'react';

// Node Modules Imports
import PropTypes from 'prop-types';

// Style Imports and Asset Import
import s from './FloatingButton.module.scss';
import add from '../../public/priwasi/ic-add.svg';

// Utility Imports
// import cx from '../../../utils/classNames';

// Component Import
// import FilterButton from '../FilterButton/FilterButton'

const FloatingButton = props => {
  return (
    <div className={s.floatingButton}>
      <button
        onClick={props.onClick}
        className={`${s.button} ${props.classNamesStr}`}
      >
        {' '}
        <img className={s.addImg} src={add} alt="+" />
        <span>{props.text}</span>
      </button>
    </div>
  );
};

export default FloatingButton;

FloatingButton.propTypes = {
  onClick: PropTypes.func,
  text: PropTypes.string
};

FloatingButton.defaultProps = {
  status: 'inactive',
  onClickHnadler: () => {}
};
