import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import s from './FloatingFormField.module.scss';
import cx from './../../utils/classNames';
import dropdownicon from '../../public/svgfonts/ic_chevron_right_grey.svg';

//import calendar from '../../public/svgfonts/ic_today.svg';

export default class FloatingFormField extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      count: this.props.strCount
    };
  }

  handleChange = event => {
    this.props.handleChange({
      name: event.target.name,
      value: event.target.value
    });
  };

  handleCountChange = event => {
    this.setState({
      count: event.target.value.length
    });
    this.props.handleChange({
      name: event.target.name,
      value: event.target.value
    });
  };

  openCalendar = () => {
    this.props.openCalendar();
  };

  handleBlur = event => {
    this.props.handleBlur && this.props.handleBlur(event.target.name);
    this.props.handleBlurValue &&
      this.props.handleBlurValue(event.target.value);
  };

  getFormField = () => {
    const {
      type,
      showCalendar,
      name,
      showLengthCount,
      readOnly,
      maxLength,
      value,
      inputGroup,
      options,
      data
    } = this.props;
    let errorClass = this.props.validationError ? s.errorClass : '';
    switch (type) {
      case 'textarea':
        return (
          <textarea
            rows="4"
            placeholder=" "
            className={cx(s.floatingInput, s.normalTextArea, {
              [s.floatingTextAreaInput]: showLengthCount
            })}
            name={name}
            value={value}
            maxLength={maxLength}
            onChange={
              showLengthCount ? this.handleCountChange : this.handleChange
            }
            onBlur={!showCalendar ? this.handleBlur : undefined}
            onFocus={this.props.onFocus}
          ></textarea>
        );
      case 'select':
        return (
          <>
            <select
              className={s.floatingSelect}
              name={name}
              onChange={this.handleChange}
              value={this.props.value}
              onFocus={this.props.onFocus}
            >
              {Object.keys(this.props.options).map(key => (
                <option
                  key={key}
                  value={this.props.options[key]}
                  onClick={this.props.handleChange}
                >
                  {this.props.options[key]}
                </option>
              ))}
            </select>
            {this.props.showDropdownArrow && (
              <img
                className={s.ddImgIcon}
                src={dropdownicon}
                alt="dropdownicon"
              />
            )}
          </>
        );

      case 'number':
        return (
          <React.Fragment>
            <input
              id={this.props.id}
              className={`${s.floatingInput} ${errorClass}`}
              type="number"
              placeholder=" "
              name={name}
              value={value}
              maxLength={maxLength}
              readOnly={readOnly ? true : false}
              onChange={
                showLengthCount ? this.handleCountChange : this.handleChange
              }
              onKeyUp={showCalendar ? this.openCalendar : undefined}
              onClick={showCalendar ? this.openCalendar : undefined}
              onBlur={!showCalendar ? this.handleBlur : undefined}
              defaultValue={this.props.defaultValue}
              onFocus={this.props.onFocus}
            />
            {inputGroup && inputGroup.cta && (
              <span onClick={inputGroup.action} className={s.inputGroup}>
                {inputGroup.cta}
              </span>
            )}
            {showCalendar && (
              <i
                className={cx('calender', s.calenderIcon)}
                onClick={this.openCalendar}
              ></i>
            )}
          </React.Fragment>
        );
      case 'share':
        return (
          <textarea
            rows="4"
            placeholder=" "
            className={cx(
              s.floatingInput,
              s.normalTextArea,
              {
                [s.floatingTextAreaInput]: showLengthCount
              },
              s.shareTextArea
            )}
            name={name}
            value={value}
            maxLength={maxLength}
            onChange={
              showLengthCount ? this.handleCountChange : this.handleChange
            }
            onBlur={!showCalendar ? this.handleBlur : undefined}
            onFocus={this.props.onFocus}
          ></textarea>
        );
      /*
          To Do : Optimise and merge into one type.
          Currently for plan its a different type radio button 
          and for normal radio button its different
      */
      case 'radio':
        return (
          <div key={`_parent_radio`} className={s.floatingRadio}>
            {data &&
              data.map((item, index) => {
                return (
                  <label
                    key={`_parent_options_${index}`}
                    htmlFor={item.title}
                    className={s.radioLabelParent}
                  >
                    <input
                      key={`_parent_index_${index}`}
                      id={item.value}
                      type="radio"
                      name={name}
                      onClick={e => this.handleChange(e, true)}
                    />
                    <div className={s.subTextRadio}>
                      <span>{item.title}</span>
                    </div>
                  </label>
                );
              })}
          </div>
        );

      default:
        return (
          <React.Fragment>
            <input
              id={this.props.id}
              className={`${s.floatingInput} ${errorClass}`}
              type="text"
              placeholder=" "
              name={name}
              value={value}
              maxLength={maxLength}
              readOnly={readOnly ? true : false}
              onChange={
                showLengthCount ? this.handleCountChange : this.handleChange
              }
              onKeyUp={showCalendar ? this.openCalendar : undefined}
              onClick={showCalendar ? this.openCalendar : undefined}
              onBlur={!showCalendar ? this.handleBlur : undefined}
              defaultValue={this.props.defaultValue}
              onFocus={this.props.onFocus}
            />
            {inputGroup && inputGroup.cta && (
              <span onClick={inputGroup.action} className={s.inputGroup}>
                {inputGroup.cta}
              </span>
            )}
            {showCalendar && (
              <i
                className={cx('calender', s.calenderIcon)}
                onClick={this.openCalendar}
              ></i>
            )}
          </React.Fragment>
        );
    }
  };

  render() {
    const {
      title,
      mandatory,
      underlineText,
      showLengthCount,
      maxLength,
      errMsg,
      validationError
    } = this.props;
    const { count } = this.state;
    return (
      <React.Fragment>
        <div className={s.floatingForm}>
          <div className={s.floatingLabel}>
            {this.getFormField()}
            <span className={s.highlight} />
            <label>{title + '' + (mandatory ? '*' : '')}</label>
          </div>
        </div>
        {validationError && (
          <div className={cx(s.inputText, s.valErrMsg)}>
            {validationError}
            {showLengthCount && (
              <span>
                {count}/{maxLength}
              </span>
            )}
          </div>
        )}
        {underlineText && !errMsg && (
          <div className={s.inputText}>
            {underlineText}
            {showLengthCount && (
              <span>
                {count}/{maxLength}
              </span>
            )}
          </div>
        )}
      </React.Fragment>
    );
  }
}

FloatingFormField.propTypes = {
  title: PropTypes.string.isRequired
};

FloatingFormField.defaultProps = {
  type: 'input',
  mandatory: false,
  showCalendar: false,
  showLengthCount: false,
  focusable: false,
  readOnly: false,
  onFocus: () => {}
};
