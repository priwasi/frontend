import React from 'react';
import s from './Footer.module.scss';

export default class Footer extends React.PureComponent {
  render() {
    return (
      <div className={s.footerContainer}>
        <div className={this.props.className}>{this.props.children}</div>
      </div>
    );
  }
}
