import React, { Component } from "react";
import s from "./Header.module.scss";
import { getCookie, deleteCookie } from "../../storage/cookie";
import { UI_COOKIE_NAME } from "../../utils/enum";
import { withRouter } from "react-router-dom";
/* eslint-disable */

const logout = () => {
  deleteCookie(process.env.REACT_APP_AUTH_COOKIE_NAME);
};

const headerOptions = [
  {
    title: "Login",
    id: "login",
    icon: false,
    link:
      process.env.REACT_APP_STAGE === "staging"
        ? process.env.REACT_APP_LOGIN_STAGING_URL
        : process.env.REACT_APP_LOGIN_URL
  },
  {
    title: "Free Sign up",
    id: "signup",
    icon: false,
    link: false,
    className: s.btn
  },
  {
    title: "Logout",
    id: "logout",
    icon: false,
    link: logout,
    className: s.btn
  }
];

class HeaderChildren extends Component {
  gotoLink = link => {
    const { history } = this.props;
    if (typeof link === "string") {
      window.location.href = link;
    } else {
      link();
      window.location.reload();
    }
  };
  render() {
    return (
      <div className={s.headerChildrenParent}>
        <span>
          <ul>
            {headerOptions.map((item, index) => {
              if (!getCookie(UI_COOKIE_NAME)) {
                let className_ = item.className ? item.className : "";
                if (item.id !== "logout") {
                  return (
                    <li
                      key={index}
                      className={className_}
                      onClick={() => this.gotoLink(item.link)}
                    >
                      <span>{item.title}</span>
                    </li>
                  );
                }
              } else {
                if (item.id !== "login" && item.id !== "signup") {
                  let className_ = item.className ? item.className : "";
                  return (
                    <li
                      key={index}
                      className={className_}
                      onClick={() => this.gotoLink(item.link)}
                    >
                      <span>{item.title}</span>
                    </li>
                  );
                }
              }
            })}
          </ul>
        </span>
      </div>
    );
  }
}

export default withRouter(HeaderChildren);
