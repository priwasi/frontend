// React Import
import React from 'react';

// Styles and Asset Import
import s from './InfoPanel.module.scss';
import success from '../../public/images/ic-check-cirlce.svg';
import error from '../../public/images/ic_clear.svg';

// Component Import
import ProgressLoaderButton from '../ProgressLoaderButton';
import Button from '../Button/Button';

const InfoPanel = props => {
  return (
    <>
      <div className={`${s.container} ${!props.noFlex ? s.flexClass : null}`}>
        <div className={s.titleContainer}>
          <span className={s.title}>{props.title}</span>
        </div>

        {props.loading && (
          <div className={s.loaderParent}>
            <ProgressLoaderButton
              btnText="Done"
              isDisabled={false}
              isShowLoaderDots={true}
              onClickHandler={() => {}}
            />
          </div>
        )}
        {props.success && (
          <div className={s.successImg}>
            <img src={success} alt={'success_check'} />
          </div>
        )}
        {props.error && (
          <div className={s.successImg}>
            <img className={s.errorImg} src={error} alt={'error_check'} />
          </div>
        )}
      </div>
      {props.button && (
        <div className={s.ButtonParent}>
          <Button
            classNamesStr={s.proceedBtn}
            btnText={props.buttonText}
            type={'primary'}
            size={'medium'}
            btnType={'submit'}
            onClickHandler={props.onClickHandler}
          />
        </div>
      )}
    </>
  );
};
InfoPanel.defaultProps = {
  title: '',
  loading: false,
  success: false,
  button: false,
  buttonText: null,
  noFlex: false,
  error: false
};
export default InfoPanel;
