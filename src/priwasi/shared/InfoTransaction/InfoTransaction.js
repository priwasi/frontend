// React Import
import React from 'react';
/*
 Transaction information  component depending on type
*/
// Styles and Asset Import
import s from './InfoTransaction.module.scss';
//image import
import darkchevron from '../../public/svgfonts/ic_chevron_rightblack.svg';
import { getImage } from '../../utils/AppUtils';
import { getCurrencySymbol } from '../../utils/helper';

/*
  Build Info Pane Body for Info Transaction
  @params : props = Object
*/
const buildInfoPane = props => {
  switch (props.type) {
    case 'udhaar':
      return getUdharLayout(props);
    case 'advance':
      return getAdvanceLayout(props);
    case 'customerDetails':
      return getCustomerInfoDetails(props);
    case 'udharreport':
      return getUdharreportLayout(props);
    case 'salereport':
      return getSalereportLayout(props);
    case 'udhaarCustomerKhata':
      return getUdharCustomerLayout(props);
    case 'advanceCustomerKhata':
      return getAdvanceCustomerLayout(props);
    case 'cashCustomerKhata':
      return getCashCustomerLayout(props);
    default:
      return getDefaultLayout(props);
  }
};
/*
Build salereport layout
*/
const getSalereportLayout = props => {
  return (
    <div className={s.divcontainer}>
      <div className={s.transaction}>
        <span className={s.date}>01/10</span>
        <span className={s.cash}>Paytm</span>
        <span className={s.amount}>&#x20b9; 100</span>
      </div>
      <div className={s.transaction}>
        <span className={s.name}>Ashish Gupta</span>
        <span></span>
        <span className={props.udhar === 'udhar' ? s.udhar : s.advance}>
          &#x20b9; 100 {props.udhar === 'udhar' ? 'udhar' : 'advance'}
        </span>
      </div>
    </div>
  );
};

/*
Build udharreport layout
*/
const getUdharreportLayout = props => {
  return (
    <div className={s.divcontainer}>
      <div className={s.transaction}>
        <span>01/10</span>
        <span className={s.udhar1}>&#x20b9; 1,000</span>
        <span className={s.advance1}>&#x20b9; 200</span>
      </div>
      <div className={s.transaction}>
        <span className={s.name}>Ashish Gupta</span>
        <span></span>
        <span className={s.mode}>Paytm</span>
      </div>
    </div>
  );
};

/*
  Build Udhar Layout 
  @params : props = Object
*/

const getUdharLayout = props => {
  return (
    <div className={s.container}>
      <div className={s.columncontainer}>
        <div className={s.namecontainer}>
          <div className={s.name}>
            {props.serial === '' ? props.data.name : props.serial}
          </div>
          <div className={s.amountDefault}>
            {props.currency}
            {props.data.sale}
          </div>
        </div>
        <div className={s.mobilecontainer}>
          <div className={s.mobile}>{props.data.mobileNo}</div>
          <div className={`${s.type} ${s.amountorange}`}>{`${
            props.currency
          }${props.data.amount * -1} udhaar`}</div>
        </div>
      </div>
      <div className={s.imagecontainer}>
        <img className={s.image} src={darkchevron} alt={'logo'} />
      </div>
    </div>
  );
};
/*
  Build Advance Layout 
  @params : props = Object
*/
const getAdvanceLayout = props => {
  return (
    <div className={s.container}>
      <div className={s.columncontainer}>
        <div className={s.namecontainer}>
          <div className={s.name}>
            {props.serial === '' ? props.data.name : props.serial}
          </div>
          <div className={s.amountDefault}>
            {props.currency}
            {props.data.sale}
          </div>
        </div>
        <div className={s.mobilecontainer}>
          <div className={s.mobile}>{props.data.mobileNo}</div>
          <div
            className={`${s.type} ${s.amountgreen}`}
          >{`${props.currency}${props.data.amount} advance`}</div>
        </div>
      </div>
      <div className={s.imagecontainer}>
        <img className={s.image} src={darkchevron} alt={'logo'} />
      </div>
    </div>
  );
};
/*
  Build Customer Transaction Details Layout
  @params : props = Object
*/
const getCustomerInfoDetails = props => {
  let ty =
    props.data.payment > props.data.sale
      ? 'Advance'
      : props.data.payment == props.data.sale
      ? 'Cash Sale'
      : 'Udhar';
  return (
    <div className={s.container}>
      <div className={s.customerInfoDetails}>
        <div className={s.saleTab}>
          <span>
            {props.currency}
            {props.data.sale}
          </span>
          <span className={s.dateHolder}>11 Oct, 4:12 PM</span>
        </div>
        <span className={s.payment}>
          {props.currency}
          {props.data.payment}
        </span>
        <div className={s.customerResultant}>
          <div>
            <span>
              {props.currency}
              {props.data.amount}
            </span>
            <span className={ty === 'Advance' ? s.green : s.orange}>{ty}</span>
          </div>
          <img src={getImage('rightarrow')} alt={'right'} width={24} />
        </div>
      </div>
    </div>
  );
};

/*
  Build Default Layout 
  @params : props = Object
*/
const getDefaultLayout = props => {
  return (
    <div className={s.container}>
      <div className={s.columncontainer}>
        <div className={s.namecontainer}>
          <div className={s.name}>
            {props.serial === '' ? props.data.name : props.serial}
          </div>
          <div className={s.amountDefault}>
            {props.currency}
            {props.data.sale}{' '}
          </div>
        </div>
        <div className={s.mobilecontainer}>
          <div className={s.mobile}>
            {props.data.mobileNo} • {props.data.name}
          </div>
          <div className={s.type}>Cash Sale</div>
        </div>
      </div>
      <div className={s.imagecontainer}>
        <img className={s.image} src={darkchevron} alt={'logo'} />
      </div>
    </div>
  );
};

/*
  Build Udhar Layout  for customer khata
  @params : props = Object
*/

const getUdharCustomerLayout = props => {
  return (
    <div className={s.container}>
      <div className={s.columncontainer}>
        <div className={s.namecontainer}>
          <div className={s.name}>{props.data.name}</div>
          <div className={s.orange}>
            {props.currency}
            {props.data.amount * -1}
          </div>
        </div>
        <div className={s.mobilecontainer}>
          <div className={s.mobile}>{props.data.mobileNo}</div>
          <div className={s.amountDefault}>Due on </div>
        </div>
      </div>
      <div className={s.imagecontainer}>
        <img className={s.image} src={darkchevron} alt={'logo'} />
      </div>
    </div>
  );
};

/*
  Build Advance Layout  for customer khata
  @params : props = Object
*/

const getAdvanceCustomerLayout = props => {
  return (
    <div className={s.container}>
      <div className={s.columncontainer}>
        <div className={s.namecontainer}>
          <div className={s.name}>{props.data.name}</div>
          <div className={s.green}>
            {props.currency}
            {props.data.amount}
          </div>
        </div>
        <div className={s.mobilecontainer}>
          <div className={s.mobile}>{props.data.mobileNo}</div>
          <div className={s.amountDefault}>Advance</div>
        </div>
      </div>
      <div className={s.imagecontainer}>
        <img className={s.image} src={darkchevron} alt={'logo'} />
      </div>
    </div>
  );
};

/*
  Build cash Layout  for customer khata
  @params : props = Object
*/

const getCashCustomerLayout = props => {
  return (
    <div className={s.container}>
      <div className={s.columncontainer}>
        <div className={s.namecontainer}>
          <div className={s.name}>{props.data.name}</div>
          <div className={s.amountDefault}>
            {props.currency}
            {props.data.amount}
          </div>
        </div>
        <div className={s.mobilecontainer}>
          <div className={s.mobile}>{props.data.mobileNo}</div>
          <div className={s.amountDefault}>Due</div>
        </div>
      </div>
      <div className={s.imagecontainer}>
        <img className={s.image} src={darkchevron} alt={'logo'} />
      </div>
    </div>
  );
};

const InfoTransaction = props => {
  return buildInfoPane(props);
};

InfoTransaction.defaultProps = {
  name: '',
  type: 'Udhaar',
  amount: '0',
  mobile: '',
  currency: getCurrencySymbol()
};
export default InfoTransaction;
