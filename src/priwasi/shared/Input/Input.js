// React Import
import React from "react";

// Styles and Asset Import
import s from "./Input.module.scss";

const Input = props => {
  return (
    <div className={s.container}>
      <label className={s.wrapLabel}>{props.label ? props.label : ""}</label>
      <input
        placeholder={`${props.placeholder}`}
        className={props.className ? props.className : s.input}
        value={props.value}
        onChange={props.onChangeHandler}
        onClick={props.onClickHandler}
        readOnly={props.readOnly}
        name={props.name}
        type={props.type ? props.type : "text"}
      />
      {/* <i className='gvFontIcons icon-back' ></i> */}
    </div>
  );
};
export default Input;
