import React from 'react';
import s from './ListItem.module.scss';
/* feeder function to modal for list item*/
export default class ListItem extends React.PureComponent {
  render() {
    return (
      <div className={s.morecontainer}>
        <img className={s.image} src={this.props.image} alt="warning" />
        <div className={s.text}>{this.props.header}</div>
      </div>
    );
  }
}
