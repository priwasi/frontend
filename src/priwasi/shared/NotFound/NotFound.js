// React Import
import React, { useState } from "react";

// Styles and Asset Import
import s from "./NotFound.module.scss";

const NotFound = props => {
  return <div className={s.container}>{props.text}</div>;
};
export default NotFound;
