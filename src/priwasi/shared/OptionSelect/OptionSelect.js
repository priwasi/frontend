// React Import
import React, { useState } from "react";

// Styles and Asset Import
import s from "./OptionSelect.module.scss";

const showOptionsHandler = visibilityStateObject => {
  visibilityStateObject.setVisibility(!visibilityStateObject.showOptions);
};

const recordCurrentValue = (
  valueStateObject,
  visibilityStateObject,
  value,
  callbackValue = null,
  callbackMethod = () => {},
  data = {},
  props
) => {
  console.log(callbackValue);
  valueStateObject.setValue(value);
  showOptionsHandler(visibilityStateObject);
  data.name = props.name;
  callbackMethod(callbackValue, data);
};

const getOptions = (props, visibilityStateObject, valueStateObject) => {
  switch (props.type) {
    case "roles":
      return props.options.map((item, index) => {
        return (
          <span
            onClick={() =>
              recordCurrentValue(
                valueStateObject,
                visibilityStateObject,
                item.roleName,
                item.roleId,
                props.callbackMethod,
                props.data,
                props
              )
            }
            key={`${index}_role`}
          >
            {item.roleName}
          </span>
        );
      });
    default:
      return <span>-</span>;
  }
};

const OptionSelect = props => {
  const [showOptions, setVisibility] = useState(false);
  const [selectedValue, setValue] = useState(props.value);
  let containerClass = props.customStyle
    ? s[`optionSelect_${props.customStyle}`]
    : s.container;

  return (
    <div className={containerClass}>
      <div className={s.optionSelector}>
        <input
          type="text"
          value={selectedValue ? selectedValue : "Select Role"}
          readOnly
          onClick={() => showOptionsHandler({ showOptions, setVisibility })}
        />
      </div>
      {showOptions && (
        <div className={s.optionHolder}>
          {props.additionalOptions && props.additionalOptions}
          {getOptions(
            props,
            { showOptions, setVisibility },
            { selectedValue, setValue }
          )}
        </div>
      )}
    </div>
  );
};
export default OptionSelect;
