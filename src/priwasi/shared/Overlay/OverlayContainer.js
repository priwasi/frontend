import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Overlay from './Overlay';

export default withRouter(connect()(Overlay));
