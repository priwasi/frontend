import React from "react";
import s from "./Paginator.module.scss";
import { client } from "../../utils/webviewUtils";
/* feeder function to modal for list item*/
export default class Paginator extends React.PureComponent {
  state = {
    currentPage: 1
  };

  trackScroll = () => {
    const {
      total,
      pageSize,
      action,
      clientId,
      currentFilters,
      currentTab
    } = this.props;
    const { currentPage } = this.state;

    let filters = { ...currentFilters };
    let elem = document.getElementById("parent");
    if (elem && elem.scrollTop === elem.scrollHeight - elem.offsetHeight) {
      let totalPages = Math.ceil(parseInt(total) / parseInt(pageSize));
      if (currentPage < totalPages) {
        let newPage = currentPage + 1;
        this.setState({ currentPage: newPage });
        if (currentTab) {
          filters["type"] = currentTab;
        }
        action(filters, clientId, newPage);
      }
    }
  };

  renderChildren = () => {
    const { type, height } = this.props;
    switch (type) {
      case "legacy":
        return <div></div>;
        break;
      default:
        return (
          <div
            id={"parent"}
            className={s.parent}
            style={{ height: height ? height : "500px" }}
            onScroll={this.trackScroll}
          >
            {this.props.children}
          </div>
        );
    }
  };
  render() {
    const { action } = this.props;
    return <div>{this.renderChildren()}</div>;
  }
}

Paginator.defaultProps = {
  pageSize: 10
};
