/*
  @Module : PaytmKhata
  @Component : Pill
  @Type : Shared
  @Description : Pill Shaped UI Element
  @Author : Kaustubh Saxena
*/

// React Imports
import React, { PureComponent } from 'react';

// Node Modules Imports
import PropTypes from 'prop-types';

// Stylesheet Imports
import s from './Pill.module.scss';

const getClassName = props => {
  switch (props.type) {
    case 'reportpill':
      if (!props.active) {
        return `${s.reportpill}`;
      } else {
        return `${s.selectedreportpill}`;
      }

    default:
      return `${s.pill}`;
  }
};

const Pill = props => {
  const { text, active, onClickHandler, config_key, index, type } = props;
  console.log(active);
  let classNameStr_ = `${active ? s.active : ''} ${props.white &&
    !props.active &&
    s.white}`;
  classNameStr_ = `${classNameStr_} ${getClassName(props)}`;

  return (
    <div
      className={`${classNameStr_}`}
      key={`${index}_parent`}
      onClick={() => onClickHandler(config_key)}
    >
      <span className={s.text} key={`${index}_textKey`}>
        {text}
      </span>
    </div>
  );
};
export default Pill;

Pill.propTypes = {
  text: PropTypes.string.isRequired
};
Pill.defaultProps = {
  onClickHandler: () => {}
};
