import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import s from "./PlatformHeaderChildren.module.scss";
import { deleteCookie } from "../../storage/cookie";
import profile from "../../public/images/human.svg";

// Node Modules Import
import { FaSignOutAlt } from "react-icons/fa";

/* eslint-disable */
import { bindActionCreators } from "redux";

class PlatformHeaderChildren extends Component {
  state = {
    showDropdown: false
  };

  logout = () => {
    const { history } = this.props;
    deleteCookie(process.env.REACT_APP_ADMIN_COOKIE_NAME);
    history.push("/admin/login");
  };

  toggleDropdown = () => {
    this.setState({ showDropdown: !this.state.showDropdown });
  };

  gotoLink = meta_key => {
    const { history } = this.props;
    switch (meta_key) {
      case "team":
        history.push("/settings/users");
        break;
      case "changePassword":
        history.push("/platform/changePassword");
      default:
        break;
    }
  };

  render() {
    const { showDropdown } = this.state;
    return (
      <div className={s.headerChildrenParent}>
        {/* <span className={s.logout} onClick={this.logout}>
          <span>Logout</span>
          <span>
            <FaSignOutAlt />
          </span>
        </span> */}
        <span className={s.logout} onClick={this.toggleDropdown}>
          <img src={profile} alt="profile" />
          <span>
            {this.props.userInfo.data &&
              this.props.userInfo.data.data &&
              this.props.userInfo.data.data.name}
          </span>
        </span>
        {showDropdown && (
          <div className={s.dropDownParent}>
            <ul>
              {/* <li>Profile</li> */}
              {/* <li>Settings</li> */}
              <li onClick={() => this.gotoLink("team")}>Team</li>
              <li onClick={() => this.gotoLink("changePassword")}>
                Change Password
              </li>
              <li onClick={this.logout}>Logout</li>
            </ul>
          </div>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state, prevProps) => {
  return {
    allPermissions: state.app.allPermissions,
    userInfo: state.app.userInfo
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({}, dispatch);
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(PlatformHeaderChildren)
);
