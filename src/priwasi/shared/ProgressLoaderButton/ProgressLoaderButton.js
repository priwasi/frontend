import React, { PureComponent } from 'react';
import s from './ProgressLoaderButton.module.scss';
import cx from '../../utils/classNames';

export default class ProgressLoaderButton extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isDisabled: props.isDisabled
    };
  }

  render() {
    return (
      <div className={s.btnSpinnerWrapper}>
        <div
          className={cx(
            { [s.showSpinner]: this.props.isShowLoaderDots },
            s.spinner
          )}
        >
          <div className={s.bounce1}></div>
          <div className={s.bounce2}></div>
          <div className={s.bounce3}></div>
          <div className={s.bounce4}></div>
          <div className={s.bounce5}></div>
        </div>
      </div>
    );
  }
}

ProgressLoaderButton.defaultProps = {
  isDisabled: false,
  onClickHandler: null,
  btnText: null,
  isShowLoaderDots: false
};
