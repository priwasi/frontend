// React Import
import React from 'react';

// Styles and Asset Import
import s from './SectionTitle.module.scss';

const SectionTitle = props => {
  return (
    <div className={s.sectionTitleContainer} style={props.styleConfig}>
      {props.title && (
        <span className={`${props.className ? props.className : s.title}`}>
          {props.title}
        </span>
      )}
      {props.children}
    </div>
  );
};
export default SectionTitle;
