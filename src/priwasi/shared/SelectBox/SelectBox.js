// React Import
import React from 'react';

// Styles and Asset Import
import s from './SelectBox.module.scss';

// Utility Imports
import { getCurrencySymbol } from '../../utils/helper';

const getDefaultValue = props => {
  let defaultValue = null;
  props.options.map(item => {
    if (item.selected) {
      defaultValue = item.value;
    }
    return null;
  });
  return defaultValue;
};

const handleChange = (event, props) => {
  let obj = {
    name: event.target.name,
    value: event.target.value
  };
  props.onChangeHandler(obj);
};

const SelectBox = props => {
  return (
    <div className={s.container}>
      <label className={s.wrapLabel}>{props.label ? props.label : ''}</label>
      <select
        name={props.name}
        onChange={e => handleChange(e, props)}
        className={s.selectBox}
        defaultValue={getDefaultValue(props)}
      >
        {props.options &&
          props.options.map((item, index) => {
            return (
              <option key={index} value={item.value}>{`${
                props.currency ? getCurrencySymbol(item.currency) : ''
              }${item.value}`}</option>
            );
          })}
      </select>
    </div>
  );
};
export default SelectBox;
