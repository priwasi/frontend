import React, { PureComponent } from 'react';
import s from './Shimmer.module.scss';

export default class Shimmer extends PureComponent {
  render() {
    return (
      <div className={s.shimmerSkeleton}>
        <div className={s.shimmerBox}></div>
        <div className={s.shimmerTabs}>
          <div className={s.tab1}></div>
          <div className={s.tab1}></div>
          <div className={s.tab1}></div>
        </div>
        <div className={s.shimmerLine}></div>
        <div className={s.shimmerLines}>
          <div className={s.shimmerLines1}></div>
          <div className={s.shimmerLines2}></div>
        </div>
        <div className={s.shimmerGame}>
          <div className={s.imageBox}></div>
          <div className={s.detailBox}>
            <div className={s.detail1}></div>
            <div className={s.detail2}></div>
          </div>
          <div className={s.playBox}></div>
        </div>
        <div className={s.shimmerGame}>
          <div className={s.imageBox}></div>
          <div className={s.detailBox}>
            <div className={s.detail1}></div>
            <div className={s.detail2}></div>
          </div>
          <div className={s.playBox}></div>
        </div>
      </div>
    );
  }
}
