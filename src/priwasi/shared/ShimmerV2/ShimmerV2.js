import React, { PureComponent } from 'react';
import s from './ShimmerV2.module.scss';

export default class Shimmer extends PureComponent {
  render() {
    return (
      <div className={s.shimmerSkeleton}>
        {this.props.children}
        {this.props.type === 'line' && (
          <div
            className={s.shimmerLine}
            style={{
              width: this.props.width,
              float: this.props.float,
              marginTop: this.props.marginTop,
              marginLeft: this.props.marginLeft
            }}
          ></div>
        )}
        {this.props.type === 'box' && (
          <div
            className={s.shimmerBox}
            style={{
              width: this.props.width,
              height: this.props.height,
              float: this.props.float
            }}
          ></div>
        )}
      </div>
    );
  }
}

Shimmer.defaultProps = {
  width: '100%',
  type: 'line',
  height: '100px',
  float: 'unset',
  marginTop: '10px',
  marginLeft: 0
};
