// React Import
import React from "react";

// Styles and Asset Import
import s from "./StatCard.module.scss";

const StatCard = props => {
  return (
    <div className={s.container}>
      <div className={s.header}>{props.title}</div>
      <div className={s.body}>{props.content}</div>
    </div>
  );
};
export default StatCard;
