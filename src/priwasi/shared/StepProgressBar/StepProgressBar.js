import React, { useState, Fragment } from "react";
// Style Imports
import s from "./StepProgressBar.module.scss";

const setPopupVisibility = (item, stateObject) => {
  stateObject.setTitleVisibility(item.config_key);
};
const removePopupVisibility = stateObject => {
  stateObject.setTitleVisibility(false);
};

const StepProgressBar = props => {
  const { checkPoints, length, active } = props;
  const [showTitle, setTitleVisibility] = useState(false);
  let lengthStyle = length ? `${length}` : "50px";
  return (
    <div className={s.progressBar}>
      {checkPoints.map((item, index) => {
        return (
          <Fragment key={`${index}_stepProgressBar`}>
            {active === item.config_key && (
              <span className={s.titleHolder}>{item.title}</span>
            )}
            <span
              className={`${s.checkPoints} ${
                active === item.config_key ? s.active : ""
              }`}
              //   onMouseOver={() =>
              //     setPopupVisibility(item, {
              //       showTitle,
              //       setTitleVisibility
              //     })
              //   }
              //   onMouseOut={() =>
              //     removePopupVisibility({ showTitle, setTitleVisibility })
              //   }
              onClick={item.action}
            ></span>
            {index < checkPoints.length - 1 && (
              <div className={s.separator} style={{ width: lengthStyle }}></div>
            )}
          </Fragment>
        );
      })}
    </div>
  );
};
export default StepProgressBar;
