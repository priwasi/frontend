import React from 'react';
import s from './TabChanger.module.scss';
/*
 Button component for header
*/

export default class TabChanger extends React.PureComponent {
  render() {
    if (this.props.config === this.props.active) {
      return (
        <div onClick={this.props.onClick} className={s.clicked}>
          <div className={s.highlighted}>
            <span className={s.amount}>{this.props.amount}</span>
            <span className={s.text}>{this.props.text}</span>
          </div>
          <span className={s.clickicon}></span>
        </div>
      );
    } else {
      return (
        <div onClick={this.props.onClick} className={s.click}>
          <div className={s.highlight}>
            <span className={s.amount}>{this.props.amount}</span>
            <span className={s.text}>{this.props.text}</span>
          </div>
          <span className={s.clickic}></span>
        </div>
      );
    }
  }
}
