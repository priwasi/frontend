// React Import
import React from "react";
import { withRouter } from "react-router-dom";

// Styles and Asset Import
import s from "./TableRow.module.scss";
import FileUpload from "../FileUpload/FileUpload";

const TableRow = props => {
  const { data, formatData, type } = props;
  let className = "";
  switch (type) {
    case "requestMap":
      return (
        <div className={s.container}>
          <span key={`request_silosId`}>{data.name}</span>
          <span key={`request_status`}>{data.status}</span>
          <span key={`request_owner`}>{data.owner}</span>
          <span key={`request_source`}>{data.source}</span>
          <span key={`request_flowType`}>{data.flowType}</span>
          <span key={`request_identifier`}>{data.identifier}</span>
          <span key={`request_fileName`}>{getDefaultOptions(data, props)}</span>
        </div>
      );
    default:
      return (
        <div className={s.container}>
          {Object.keys(data).map((item, index) => {
            if (formatData && item === formatData) {
              return (
                <span key={`${index}_object`}>
                  <span className={getClassName(data[item], item)}>
                    {data[item]}
                  </span>
                </span>
              );
            }
            return <span key={`${index}_object`}>{data[item]}</span>;
          })}
        </div>
      );
  }
};

const getDefaultOptions = (data, props) => {
  const { params } = props.match;
  let data_ = { ...data, id: params.id };
  if (data.flowType && data.flowType === "Manual") {
    if (data.fileName) {
      return <span className={s.fileName}>{data.fileName}</span>;
    }
    if (!data.fileName && !data.uploaded) {
      return (
        <span>
          <FileUpload
            name={data.dsId}
            data={data_}
            onChangeHandler={props.onUpload}
          />
        </span>
      );
    }
  }
  return "";
};

const getClassName = (data, key) => {
  switch (key) {
    case "status":
      // Not initiated, Under approval, To do, in-progress, Completed, Failed, Rejected
      if (data && data.toLowerCase() === "in-progress") {
        return s.statusOngoing;
      }
      if (data && data.toLowerCase() === "under approval") {
        return s.statusPending;
      }
      if (data && data.toLowerCase() === "completed") {
        return s.statusCompleted;
      }

    default:
      return "";
  }
};

export default withRouter(TableRow);
