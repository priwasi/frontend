// React Import
import React from "react";

// Styles and Asset Import
import s from "./Tabs.module.scss";

class Tabs extends React.Component {
  state = {
    activeTab: null
  };

  componentDidMount = () => {
    const { defaultActive } = this.props;
    if (defaultActive) {
      this.setState({ activeTab: 0 });
    }
  };

  /*
        Change Tab Action
    */
  changeTab = index => {
    this.setState({ activeTab: index });
  };

  render() {
    const { activeTab } = this.state;
    const { data } = this.props;
    console.log(data);
    return (
      <div className={s.container}>
        <ul className={s.tabsHeader}>
          {data.map((item, index) => (
            <li
              className={activeTab === index ? s.active : ""}
              key={`${index}_${item.config_key}`}
              onClick={() => this.changeTab(index)}
            >
              {item.title}
            </li>
          ))}
        </ul>
        <div key={`${activeTab}`}>
          {data[activeTab] && data[activeTab].content}
        </div>
      </div>
    );
  }
}

export default Tabs;
