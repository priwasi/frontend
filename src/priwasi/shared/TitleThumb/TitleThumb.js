// React Import
import React from "react";
/*
 Transaction information  component depending on type
*/
// Styles and Asset Import
import s from "./TitleThumb.module.scss";
//image import
import darkchevron from "../../public/svgfonts/ic_chevron_rightblack.svg";
import { getImage } from "../../utils/AppUtils";
import { getCurrencySymbol } from "../../utils/helper";
import sample from "../../public/images/sampleLogo.svg";
import AccessControl, {
  noActionAccessControl
} from "../../services/AccessControl/AccessControl";
import { permissions } from "../../constants/permissions";
import { FaTrash } from "react-icons/fa";

const buildThumb = props => {
  let data = props.data && props.data;
  let shortName = data && data.title && data.title.slice(0, 2);

  switch (props.type) {
    case "add":
      return (
        <AccessControl
          disabled={true}
          moduleStyle={s.disabled}
          meta={permissions.ADD_DATA_SILOS}
        >
          <div
            className={s.container}
            onClick={() => {
              {
                return noActionAccessControl(
                  permissions.ADD_DATA_SILOS,
                  props.onClickHandler,
                  props.userInfo
                );
              }
            }}
          >
            <div className={s.thumb}>
              <span className={s.add}>+</span>
            </div>
          </div>
        </AccessControl>
      );
    default:
      return (
        <div className={s.container} onClick={props.onClickHandler}>
          <div className={s.thumb}>
            {!data.icon && <span className={s.custom_short}>{shortName}</span>}
            {data.icon && <img src={sample} />}
            <span className={s.title}>{data.title}</span>
          </div>
        </div>
      );
  }
};

const TitleThumb = props => {
  return buildThumb(props);
};

TitleThumb.defaultProps = {
  type: null
};
export default TitleThumb;
