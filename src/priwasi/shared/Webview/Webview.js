import React, { Component } from 'react';
import s from './Webview.module.scss';
// import crossIcon from "../../public/images/crossIcon.svg";
import { disableBodyScrolling } from '../../utils/domUtils';
// import backIcon from "../../public/svgfonts/back.svg";

export default class Webview extends Component {
  componentDidMount() {
    disableBodyScrolling(true);
  }

  componentWillUnmount() {
    disableBodyScrolling(false);
  }

  render() {
    return (
      <div className={s.loanOfferingTncPage}>
        <section className={s.topContainer}>
          <i
            className="gvFontIcons icon-back"
            onClick={this.props.onClickHandler}
          />
          <h3>{this.props.headerTitle}</h3>
          {/* <img className={s.loanImageContainer} src={backIcon} onClick={this.props.onClickHandler} alt="back" /> */}
        </section>
        <iframe
          src={this.props.webviewUrl}
          className={s.iframeContainer}
          title="web View"
        />
      </div>
    );
  }
}

Webview.defaultProps = {
  headerTitle: null,
  webviewUrl: null
};
