import {
  GET_USER_LOADING,
  GET_USER_SUCCESS,
  GET_USER_FAILURE,
  GET_USER_CLEAR,
  GET_PERMISSIONS_LOADING,
  GET_PERMISSIONS_SUCCESS,
  GET_PERMISSIONS_FAILURE,
  GET_PERMISSIONS_CLEAR,
  ASSIGNEE_LOADING,
  ASSIGNEE_SUCCESS,
  ASSIGNEE_FAILURE,
  ASSIGNEE_CLEAR
} from "../constants/actionTypes";

import httpFetch from "../utils/http";

/*
    Utility Imports
*/
import { getCookie } from "../storage/cookie";
import { getHeaders, getUrl } from "../utils/utilities";

export function getUserInfo(userType = null) {
  let headers = userType ? getHeaders(true, true) : getHeaders(true);

  return {
    types: [GET_USER_LOADING, GET_USER_SUCCESS, GET_USER_FAILURE],
    promise: () =>
      httpFetch(getUrl("userInfo"), {
        headers: headers
      })
  };
}

export function getAllPermissions(clientId) {
  let headers = getHeaders(true, true);
  headers["clientId"] = clientId;
  return {
    types: [
      GET_PERMISSIONS_LOADING,
      GET_PERMISSIONS_SUCCESS,
      GET_PERMISSIONS_FAILURE
    ],
    promise: () =>
      httpFetch(getUrl("allPermissions"), {
        headers: headers
      })
  };
}

export function getAssignee(clientId) {
  let headers = getHeaders(true, true);
  headers["clientId"] = clientId;
  return {
    types: [ASSIGNEE_LOADING, ASSIGNEE_SUCCESS, ASSIGNEE_FAILURE],
    promise: () =>
      httpFetch(getUrl("assignees"), {
        headers: headers
      })
  };
}
