// Action Type Constants
import {
  GET_USER_LOADING,
  GET_USER_SUCCESS,
  GET_USER_FAILURE,
  GET_USER_CLEAR,
  GET_PERMISSIONS_LOADING,
  GET_PERMISSIONS_SUCCESS,
  GET_PERMISSIONS_FAILURE,
  GET_PERMISSIONS_CLEAR,
  SAVE_PERMISSION_MAP,
  ASSIGNEE_LOADING,
  ASSIGNEE_SUCCESS,
  ASSIGNEE_FAILURE,
  ASSIGNEE_CLEAR
} from "../constants/actionTypes";

import {
  getLoadingState,
  getSuccessState,
  getErrorState
} from "./stateSettler";

const initialState = {
  userInfo: {
    loading: false,
    data: null,
    error: null
  },
  allPermissions: {
    loading: false,
    data: null,
    error: null
  },
  permMap: null,
  assignees: {
    loading: false,
    data: null,
    error: null
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_USER_LOADING:
      return {
        ...state,
        userInfo: getLoadingState("userInfo", state)
      };
    case GET_USER_SUCCESS:
      return {
        ...state,
        // userInfo: getSuccessState("userInfo", state, action)
        userInfo: {
          data: {
            ...action.result,
            data: {
              ...action.result.data,
              permissions: [
                "VIEW_DATA_SILOS",
                "EDIT_DATA_SILOS",
                "ADD_DATA_SILOS",
                "VIEW_ALL_REQUESTS",
                "CHANGE_REQUEST_STATUS",
                "UPLOAD_ACCESS_DOCUMENT",
                "DELETE_ACCESS_DOCUMENT",
                "CHANGING_ASSIGNEE_PER_SILOS_REQUEST",
                "ADDING_ASSIGNEE_PER_SILOS_REQUEST",
                "EDIT_DEFAULT_ASSIGNEE_FOR_REQUEST",
                "APPROVING_REQUEST",
                "REMINDING_DATA_SOURCE_ASSIGNEE_FOR_PARTICULAR_REQUEST",
                "INVITING_TEAM_MEMBERS",
                "REMINDING_TEAM_MEMBERS",
                "CHANGING_ROLES",
                "ASSIGNING_ROLES",
                "ASSIGNING_PERMISSIONS",
                "CHANGING_PERMISSIONS",
                "CHANGING_CONTROL_PANEL_CHECKLIST",
                "CHANGE_SILOS_STATUS_FOR_REQUEST"
              ]
            }
          },
          loading: false,
          error: null
        }
      };
    case GET_USER_FAILURE:
      return {
        ...state,
        userInfo: getErrorState("userInfo", state, action)
      };
    case GET_USER_CLEAR:
      return {
        ...state,
        userInfo: initialState.userInfo
      };
    case GET_PERMISSIONS_LOADING:
      return {
        ...state,
        allPermissions: getLoadingState("allPermissions", state)
      };
    case GET_PERMISSIONS_SUCCESS:
      return {
        ...state,
        allPermissions: getSuccessState("allPermissions", state, action)
      };
    case GET_PERMISSIONS_FAILURE:
      return {
        ...state,
        allPermissions: getErrorState("allPermissions", state, action)
      };
    case GET_PERMISSIONS_CLEAR:
      return {
        ...state,
        allPermissions: initialState.allPermissions
      };
    case SAVE_PERMISSION_MAP:
      return {
        ...state,
        permMap: action.payload
      };
    case ASSIGNEE_LOADING:
      return {
        ...state,
        assignees: getLoadingState("assignees", state)
      };
    case ASSIGNEE_SUCCESS:
      return {
        ...state,
        assignees: getSuccessState("assignees", state, action)
      };
    case ASSIGNEE_FAILURE:
      return {
        ...state,
        assignees: getErrorState("assignees", state, action)
      };
    case ASSIGNEE_CLEAR:
      return {
        ...state,
        assignees: initialState.assignees
      };
    default:
      return state;
  }
}
