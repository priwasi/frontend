import { createStore, combineReducers, compose, applyMiddleware } from "redux";

import ReduxThunk from "redux-thunk";

/////---------------------- Common Reducer --------------------------------//////////
import app from "./appReducer";

import privacyReducer from "../components/Privacy/reducer";
import websiteReducer from "../components/Landing/reducer";

//---------------------------------- Platform Reducers--------------------------------------//
import loginReducer from "../components/Login/reducer";
import requestReducer from "../components/Platform/Request/reducer";
import requestDetailsReducer from "../components/Platform/RequestDetails/reducer";
import silosReducer from "../components/Platform/Slios/reducer";
import userReducer from "../components/Platform/Users/reducer";
import dashboardReducer from "../components/Platform/Dashboard/reducer";
import activateReducer from "../components/Platform/Activate/reducer";
import policyReducer from "../components/Platform/PolicyInfo/reducer";
import changePasswordReducer from "../components/Platform/ChangePassword/reducer";
import forgotPasswordReducer from "../components/ForgotPassword/reducer";
import connectorReducer from "../components/Platform/ConnectorInstall/reducer";
import controlPanelReducer from "../components/Platform/RequestPreferences/reducer";
//----------- Middleware Import ------------//
import { augmentorMiddleware } from "./storeMiddleware";

// if you're also using redux-thunk, add it as a middleware
const createStoreWithMiddleware = compose(
  applyMiddleware(augmentorMiddleware(), ReduxThunk)
)(createStore);

/*
  TO DO - Move Uneccessary or small reducers to app
*/

const rootReducer = combineReducers({
  app,
  requestReducer,
  privacyReducer,
  websiteReducer,
  requestDetailsReducer,
  loginReducer,
  silosReducer,
  userReducer,
  dashboardReducer,
  activateReducer,
  policyReducer,
  changePasswordReducer,
  forgotPasswordReducer,
  connectorReducer,
  controlPanelReducer
});

export default function configureStore(initialState = {}) {
  return createStoreWithMiddleware(rootReducer, initialState);
}
