import submitted from '../public/utils/ic-warning.svg';
import success from '../public/utils/ic-check-cirlce.svg';
import download from '../public/utils/ic-file-download.svg';
import cancelled from '../public/utils/ic-cancel.svg';
import rightArrow from '../public/utils/ic-chevron-right.svg';
import rightArrowCommission from '../public/utils/ic-chevron-right-commission.svg';
import whatsapp from '../public/utils/whatsapp.svg';
import chat from '../public/utils/ic-chat.svg';
import bell from '../public/svgfonts/ic_warning_copy_5.svg';
import help from '../public/svgfonts/ic_help.svg';
import download2 from '../public/svgfonts/ic_file_download.svg';

export const roughSizeOfObject = object => {
  let objectList = [];
  let stack = [object];
  let bytes = 0;

  while (stack.length) {
    var value = stack.pop();

    if (typeof value === 'boolean') {
      bytes += 4;
    } else if (typeof value === 'string') {
      bytes += value.length * 2;
    } else if (typeof value === 'number') {
      bytes += 8;
    } else if (typeof value === 'object' && objectList.indexOf(value) === -1) {
      objectList.push(value);

      for (var i in value) {
        stack.push(value[i]);
      }
    }
  }
  return bytes;
};

export function getImage(name) {
  switch (name.toLowerCase()) {
    case 'submitted':
      return submitted;
    case 'pending':
      return submitted;
    case 'success':
      return success;
    case 'cancelled':
      return cancelled;
    case 'rightarrow':
      return rightArrow;
    case 'download':
      return download;
    case 'rightarrowcommission':
      return rightArrowCommission;
    case 'whatsapp':
      return whatsapp;
    case 'sms':
      return chat;
    case 'bell':
      return bell;
    case 'help':
      return help;
    case 'chat':
      return chat;
    case 'download2':
      return download2;
    default:
      return '';
  }
}
