import CryptoJS from "crypto-js";

const AES128ECBdecrypt = (message = "", key = "") => {
  let message_ = message.replace(/"/g, "");
  let keyUtf8 = CryptoJS.enc.Utf8.parse(key);
  let decrypt = CryptoJS.AES.decrypt(message_, keyUtf8, {
    mode: CryptoJS.mode.ECB,
    keySize: 128
  });
  let final = CryptoJS.enc.Utf8.stringify(decrypt).toString();
  return final;
};

const getPassphrase = () => {
  return btoa("strongpasswordss");
};
export { AES128ECBdecrypt, getPassphrase };
