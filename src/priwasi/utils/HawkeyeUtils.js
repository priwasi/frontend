import { pushHawkeyeEvent } from './webviewUtils';
import { roughSizeOfObject } from './AppUtils';

const NO_NETWORK_MSG = 'No Network Connection';

const EVENT_TYPE = {
  ApiLog: 'apiLog',
  ApiError: 'apiError',
  ApiFailure: 'apiFailure',
  LocalError: 'localError'
};

export const pushNormalHawkeyeEvent = (response, request, responseTime) => {
  const payload = {};
  payload.uri = request.url;
  payload.screenName = window.location.toString();
  payload.responseTime = responseTime.toString();
  payload.responseCode = response.status.toString();

  if (response.body) {
    payload.responseSize = roughSizeOfObject(response).toString();
  }
  if (request.body) {
    payload.requestSize = roughSizeOfObject(request).toString();
  }
  if (response.status >= 200 && response.status < 400) {
    payload.eventType = EVENT_TYPE.ApiLog;
  } else {
    payload.eventType = EVENT_TYPE.ApiError;
    payload.errorCode = response.status.toString();
    payload.errorMsg = response.statusText ? response.statusText : '';
  }
  payload.verticalName = 'MerchantGV';
  pushHawkeyeEvent(payload);
};
export const pushNoNetworkHawkeyeEvent = request => {
  const payload = {};
  payload.screenName = window.location.toString();
  payload.uri = request.url;
  payload.eventType = EVENT_TYPE.LocalError;
  payload.customMessage = NO_NETWORK_MSG;
  if (request.body) {
    payload.requestSize = roughSizeOfObject(request).toString();
  }
  payload.verticalName = 'MerchantGV';
  pushHawkeyeEvent(payload);
};
export const pushTimeOutHawkeyeEvent = (request, timeoutTime) => {
  const payload = {};
  payload.screenName = window.location.toString();
  payload.uri = request.url;
  payload.responseTime = timeoutTime;
  payload.eventType = EVENT_TYPE.ApiFailure;
  if (request.body) {
    payload.requestSize = roughSizeOfObject(request).toString();
  }
  payload.verticalName = 'MerchantGV';
  pushHawkeyeEvent(payload);
};
