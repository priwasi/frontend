export const DELETION = "delete";
export const ACCESS = "access";
export const UI_COOKIE_NAME = "UISESSIONID";
export const PROFILE = "profile";
export const NEW = "new";
export const EXISTING = "existing";
export const TEAM = "team";
