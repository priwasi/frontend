export const getCurrencySymbol = currency => {
  switch (currency) {
    case "INR":
      return "₹";
    case "USD":
      return "$";

    default:
      return "₹";
  }
};

export const getDashboardChartOptions = () => {
  const chartOptions = {
    legend: {
      display: true,
      position: "right"
    },
    cutoutPercentage: 70,
    maintainAspectRatio: false,
    responsive: false,
    centertext: "100"
  };
  return chartOptions;
};
