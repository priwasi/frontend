import { getCookie } from "../../storage/cookie";
import * as R from "ramda";

const getPostHeaders = headers => {
  if (!headers) {
    headers = {};
  }

  if (headers["Content-Type"] === "multipart/form-data") {
    delete headers["Content-Type"];
  } else {
    headers["Content-Type"] = "application/json";
  }

  headers["Cache-Control"] = "no-cache";

  return { headers: Object.assign(headers) };
};

const checkRequest = (url, options) => {
  const modifiedUrl = url;

  let params = R.clone(options);
  const headers = getPostHeaders(params && params.headers);
  console.log(headers);
  const safeStringify = R.ifElse(R.is(Object), R.toString, R.identity);
  params.body =
    params.headers["Content-Type"] !== "application/json"
      ? params.body
      : safeStringify(params.body);
  params.timeout = 5000; //set timeout
  params = { ...headers, ...params };

  return {
    url: modifiedUrl,
    params: params
  };
};

export default checkRequest;
