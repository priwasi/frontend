import fetch from "node-fetch";
import checkResponse from "./httpResponseInterceptor";
import checkRequest from "./httpRequestInterceptor";
import { showToast, getAppLanguage } from "../webviewUtils";
import constants from "../../constants/appconstants";
import {
  pushNormalHawkeyeEvent,
  pushNoNetworkHawkeyeEvent,
  pushTimeOutHawkeyeEvent
} from "../HawkeyeUtils";

const timer = 30000;

export const httpFetch = (url, opts = {}, handle500) => {
  let requestObj = checkRequest(url, opts);
  console.log(requestObj);
  return new Promise(function(resolve, reject) {
    const timeout = setTimeout(function() {
      reject(new Error(constants.REQUEST_TIME_OUT));
    }, timer);
    const requestTime = new Date();
    let request = null;
    if (!window.navigator.onLine) {
      request = {
        msg: constants.NO_NETWORK,
        url: requestObj.url,
        params: requestObj.params
      };
      reject(request);
    } else {
      fetch(requestObj.url, requestObj.params)
        .then(response => {
          return response;
        })
        .then(checkResponse)
        .then(r => {
          clearTimeout(timeout);
          const contentType = r.headers.get("content-type");
          if (contentType) {
            if (contentType.indexOf("text/plain") !== -1) {
              return resolve(r.text());
            } else {
              return resolve(r.json());
            }
          } else {
            let result = {
              msg: "NULL_BODY"
            };
            return resolve(result);
          }
        })
        .catch(e => {
          clearTimeout(timeout);
          console.log("Not able to fetch response for ", url, e);
          if (!handle500) {
            const languageConstants = localStorage
              ? JSON.parse(localStorage.getItem("pk_languageConstants"))
              : null;
            showToast(
              languageConstants &&
                languageConstants[getAppLanguage()].constants
                  .pk_generic_error_msg
            );
          }
          if (e.toString() === "TypeError: Failed to fetch") {
            request = {
              msg: constants.REQUEST_TIME_OUT,
              url: requestObj.url,
              params: requestObj.params
            };
            reject(request);
          } else {
            reject(e);
          }
        });
    }
  });
};

export default httpFetch;
