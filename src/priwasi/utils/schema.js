export const paymentModeSchema = [
  {
    name: "Cash",
    config_key: "cash"
  },
  {
    name: "Paytm",
    config_key: "paytm"
  },
  {
    name: "Card",
    config_key: "card"
  },
  {
    name: "Other",
    config_key: "other"
  }
];

export const formValidation = [
  {
    config_key: "companyName",
    regex: /^[\s\t\r\n]*\S+/,
    regex_err_msg: "Please Enter Company Name"
  },
  {
    config_key: "fullName",
    regex: /^[\s\t\r\n]*\S+/,
    regex_err_msg: "Please Enter Your Full Name"
  },
  {
    config_key: "phone",
    regex: /^[\s\t\r\n]*\S+/,
    regex_err_msg: "Please Enter Phone Number"
  },
  {
    config_key: "email",
    regex: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
    regex_err_msg: "Please Enter a valid Email"
  },
  {
    config_key: "inviteEmail",
    regex: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
    regex_err_msg: "Invalid Value"
  }
];
