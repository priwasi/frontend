import queryString from "query-string";
import { getCookie } from "../storage/cookie";

export const getHeaders = (auth = false, admin = false) => {
  let headers = {
    "Content-Type": "application/json"
  };
  if (auth) {
    let cookieName = admin
      ? process.env.REACT_APP_ADMIN_COOKIE_NAME
      : process.env.REACT_APP_AUTH_COOKIE_NAME;
    headers["sessionToken"] = getCookie(cookieName);
  }
  return headers;
};

export const getBaseUrl = () => {
  if (process.env.REACT_APP_STAGE === "staging") {
    return process.env.REACT_APP_STAGING_BASE_URL;
  } else {
    return process.env.REACT_APP_PRODUCTION_BASE_URL;
  }
};
export const getClientBaseUrl = () => {
  if (process.env.REACT_APP_STAGE === "staging") {
    return `http://cbclient-staging.credbox.io`;
  } else {
    return `http://cbclient.credbox.io`;
  }
};
export const getAdminBaseUrl = () => {
  if (process.env.REACT_APP_STAGE === "staging") {
    return `http://admin-staging.credbox.io`;
  } else {
    return `http://admin.credbox.io`;
  }
};

export const getUrl = key => {
  if (key) {
    return urlConfig[key] ? urlConfig[key] : null;
  }
  return null;
};

const urlConfig = {
  requestAll: `${getAdminBaseUrl()}/api/admin/request/admin/all`,
  addRequest: `${getAdminBaseUrl()}/api/admin/request/admin`,
  auth_privacy: `${getClientBaseUrl()}/api/privacy/info`,
  public_privacy: `${getClientBaseUrl()}/api/privacy/info`,
  encData: `${getClientBaseUrl()}/api/sec/silos/response`,
  email: `${getBaseUrl()}/api/pub/verify`,
  requestDetails: `${getAdminBaseUrl()}/api/admin/request/detail`,
  scheduledemo: `${getBaseUrl()}/api/pub/scheduledemo`,
  adminLogin: `${getAdminBaseUrl()}/api/pub/admin/login`,
  initiateRequest: `${getClientBaseUrl()}/api/sec/request`,
  userInfo: `${getAdminBaseUrl()}/api/admin/detail`,
  createSilos: `${getAdminBaseUrl()}/api/admin/silos`,
  allPermissions: `${getAdminBaseUrl()}/api/admin/client/permissions`,
  createCustomRole: `${getAdminBaseUrl()}/api/admin/client/create_custom_role`,
  updateRole: `${getAdminBaseUrl()}/api/admin/client/custom_role`,
  fetchAllRoles: `${getAdminBaseUrl()}/api/admin/client/role_and_permission`,
  dashboard: `${getAdminBaseUrl()}/api/admin/dashboard`,
  assignees: `${getAdminBaseUrl()}/api/admin/assignees`,
  allUsers: `${getAdminBaseUrl()}/api/admin/user/all`,
  updateUser: `${getAdminBaseUrl()}/api/admin/user/update`,
  inviteUser: `${getAdminBaseUrl()}/api/admin/user/invite`,
  deleteInvite: `${getAdminBaseUrl()}/api/admin/user/invite/delete`,
  remindUser: `${getAdminBaseUrl()}/api/admin/user/invite/remind`,
  confirmToken: `${getAdminBaseUrl()}/api/pub/confirm`,
  createPassword: `${getAdminBaseUrl()}/api/pub/user/create/password`,
  policyList: `${getAdminBaseUrl()}/api/admin/privacy`,
  createUpdatePolicy: `${getAdminBaseUrl()}/api/admin/privacy/create_update`,
  changePassword: `${getAdminBaseUrl()}/api/admin/user/change/password`,
  forgotPassword: `${getAdminBaseUrl()}/api/pub/user/forgot/password`,
  moreRequests: `${getAdminBaseUrl()}/api/admin/dashboard/requests`,
  uploadFile: `http://el-staging.credbox.io/api/internal/upload`,
  connectorInstall: `https://credbox.io/api/pub/intercom/oauth2/code`,
  controlPanel: `${getAdminBaseUrl()}/api/admin/control-panel/ds-fields`
};

export const pushFilterToQuery = (
  location,
  browserHistory,
  parsedSearch,
  name,
  newValue
) => {
  parsedSearch[name] = newValue;
  let finalString = queryString.stringify(parsedSearch);
  location.search = finalString;
  browserHistory.push(`?${location.search}`);
};

export const deleteFilterFromQuery = (
  location,
  browserHistory,
  parsedSearch,
  name
) => {
  delete parsedSearch[name];
  let finalString = queryString.stringify(parsedSearch);
  location.search = finalString;
  browserHistory.push(`?${location.search}`);
};

export const lookup = (arr, key, searchVal, index_ = false) => {
  return arr.find((o, i) => {
    if (o[key] === searchVal) {
      if (index_) {
        return i;
      }
      return arr[i]; // stop searching
    }
    return false;
  });
};

export const removeEscapeCharacters = json => {
  let json_ = Object.keys(json).map(item => {
    return decodeURIComponent(json[item]);
  });
  console.log(json_);
};
