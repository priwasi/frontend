const Validator = (formData, validationRules) => {
  var validationError = {};
  Object.keys(validationRules).map(key => {
    if (
      !validateRegex(validationRules[key], formData[key]) ||
      !validateIsMandatory(validationRules[key], formData[key])
    ) {
      validationError[key] = getMessage(validationRules[key]);
    } else {
      validationError[key] = false;
    }
    return null;
  });
  return validationError;
};
const SoloValidator = (field, validationRule, name) => {
  var validationError = {};
  Object.keys(validationRule).map(key => {
    if (!validateRegex(validationRule, field)) {
      validationError[name] = getMessage(validationRule);
    } else {
      validationError[name] = false;
    }
    return null;
  });
  return validationError;
};

const getMessage = validationRulesObj => {
  return validationRulesObj.message;
};

const validateRegex = (obj, formData) => {
  if (obj.regex !== "") {
    let regex = new RegExp(obj.regex);
    if (typeof formData === "string") {
      formData = formData.trim();
    }
    if (!regex.test(formData)) {
      return false;
    } else {
      return true;
    }
  } else {
    return true;
  }
};

const validateIsMandatory = (obj, formData) => {
  if (obj.isMandatory && !formData) {
    return false;
  } else {
    return true;
  }
};

/*
    Check given Validation Rules and Add to Local State Object on Component Mount.
*/
const addValidationRules = (form, productDetails = false) => {
  if (form) {
    let validationRules = {};

    form.map((item, index) => {
      validationRules[item.config_key] = {};
      validationRules[item.config_key]["regex"] = item.regex;
      validationRules[item.config_key]["isRequired"] = item.isRequired
        ? item.isRequired
        : false;
      validationRules[item.config_key]["isMandatory"] = item.isMandatory
        ? item.isMandatory
        : false;
      validationRules[item.config_key]["message"] = item.regex_err_msg;
      return null;
    });
    return validationRules;
  }
};

/* 
    Check If Given Validation State Object data has no validation error
*/
const checkForValidationError = (validation, count = null) => {
  let error = false;
  if (
    Object.keys(validation).length === 0 ||
    (count && Object.keys(validation).length < count)
  ) {
    error = true;
  }
  Object.keys(validation).map(key => {
    if (validation[key] || validation[key] === "") {
      error = true;
    }
    return null;
  });
  return error;
};

export {
  Validator,
  addValidationRules,
  checkForValidationError,
  SoloValidator
};
