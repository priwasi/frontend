/* eslint-disable */

export const rootContractId = {
  value: null,
  get() {
    return this.value;
  },
  set(val) {
    this.value = val;
  }
};

export const checkAppName = () => {
  return typeof Android !== 'undefined'
    ? 'android'
    : typeof Ios !== 'undefined'
    ? 'ios'
    : 'web';
};

//Enum which will contain all native method which needs to calling from JS
const NATIVEFUNCTION_TYPE = {
  getNativeData: 'getNativeData',
  getAllGTMUrls: 'getAllGTMUrls',
  onBackPressed: 'onBackPressed',
  sharePost: 'sharePost',
  showErrorToast: 'showErrorToast',
  getGAEvents: 'getGAEvents',
  sessionExpiryEvent: 'sessionExpiryEvent',
  startCameraForImageUpload: 'startCameraForImageUpload',
  startGalleryForImageUpload: 'startGalleryForImageUpload',
  openApplicationStore: 'openApplicationStore',
  pushHawkeyeEvent: 'pushHawkeyeEvent',
  hideLoadingScreen: 'hideLoadingScreen',
  getContacts: 'getContacts',
  whatsAppShare: 'whatsAppShare',
  smsShare: 'smsShare',
  moreShare: 'moreShare'
};

//Enum which will contain all JS method which needs to calling from Native
const JSFUNCTION_TYPE = {
  nativeData: 'nativeData',
  allGTMUrls: 'allGTMUrls',
  getContacts: 'getContacts'
};

const Web = {
  onBackPressed: () => {
    console.log('Load inside webview');
  },
  showErrorToast: val => {
    console.log('Toast:', val);
  },
  sessionExpiryEvent: () => {
    console.log('Session expiry called');
  },
  getGtmUrl: val => {
    return handleUrlSwitch(val);
  },
  sharePost: url => {
    console.log('Load inside webview', url);
  },
  startCameraForImageUpload: val => {
    console.log('Load inside webview', val);
  },
  startGalleryForImageUpload: val => {
    console.log('Load inside webview', val);
  },
  openCustomGallery: () => {
    console.log('Load inside webview');
  },
  setNumberOfImagesToBeUploaded: () => {
    console.log('load inside webview');
  },
  getGAEvents: (cat, value) => {
    console.log('GA:', cat, value);
  },
  getAuthData: () => {
    return '{"token":"d20f85d6-f619-4d80-9a3e-9b7baaea5800","mid":"TVhmdu92708061714544","merchant_name":"Chanshu Arora","is_supported":true, "language":"en-IN"}';
  },
  getIPAddress: () => {
    return '10.10.10.10';
  },
  showLocationPrompt: () => {
    console.log('load inside webview');
  },
  getGASreenEvents: screen => {
    console.log('GAScreen: ', screen);
  },
  openNativeWebview: url => {
    console.log('Loading the following url in Native ', url);
  },
  sendDeepLink: deeplink => {
    console.log(deeplink);
  }
};

const myInterface =
  typeof Android !== 'undefined'
    ? Android
    : typeof Ios !== 'undefined'
    ? Ios
    : Web;

let gtmData = {};
let contacts = {};
export function getAuthData() {
  return JSON.parse(myInterface.getAuthData());
}
export const APP_VERSION = getAuthData().app_version
  ? getAuthData().app_version
  : '2.5.5';

export function getIPAddress() {
  return myInterface.getIPAddress();
}
export function closeWebView() {
  myInterface.onBackPressed();
}

export function sendDeepLink(deeplink) {
  myInterface.sendDeepLink(deeplink);
}

export function sharePost(val) {
  gaTrack('Channels-details', 'share_offer');
  myInterface.sharePost(val);
}

export function showToast(val) {
  myInterface.showErrorToast(val);
}

export function getApiUrl(val) {
  return myInterface.getGtmUrl(val);
}

export function showLocationPrompt() {
  myInterface.showLocationPrompt();
}

export function gaTrack(category, action) {
  myInterface.getGAEvents(category, action, null);
}

export function gaScreenEventTrack(screen) {
  myInterface.getGASreenEvents(screen);
}

export function sessionExpiryEvent() {
  myInterface.sessionExpiryEvent();
}

export function startCameraForImageUpload(val) {
  myInterface.startCameraForImageUpload(val);
}

export function openCustomGallery(num) {
  myInterface.openCustomGallery(num);
}

export function startGalleryForImageUpload(val) {
  myInterface.startGalleryForImageUpload(val);
}
export function setNumberOfImagesToBeUploaded(val) {
  myInterface.setNumberOfImagesToBeUploaded(val);
}
export function showNativeLoader() {
  myInterface.showNativeLoader();
}
export function hideNativeLoader() {
  myInterface.hideNativeLoader();
}
export function openNativeWebview(url) {
  return myInterface.openNativeWebview(url);
}

export function pushHawkeyeEvent(payload) {
  if (myInterface.pushHawkeyeEvent) {
    myInterface.pushHawkeyeEvent(JSON.stringify(payload));
  }
}

// let baseUrl = "https://digitalproxy.paytm.com/chlbwp";
// let baseUrl = "https://digitalproxy-staging.paytm.com/chlbwp";
// let baseUrl = '//localhost:3000';
//let baseUrl = 'http://localhost:3000';
let baseUrl = 'https://ump2-ite3.paytm.in';
// baseUrl = 'http://10.0.143.34:8080/';
function handleUrlSwitch(val) {
  switch (val) {
    case 'gvActivationStatus':
      return baseUrl + '/api/v1/voucher/status';
    case 'gvActivateApi':
      return baseUrl + '/api/v1/voucher/activate';
    case 'getTncApi':
      return baseUrl + '/api/v1/voucher/tnc';
    case 'getTemplateApi':
      return baseUrl + '/api/v1/voucher/template';
    case 'voucherList':
      return baseUrl + '/api/v1/voucher';
    case 'languageApi':
      return 'https://digitalproxy-staging.paytm.com/localisation/v1/getMessages?service=';
    case 'dailyKhata':
      return `${baseUrl}/ledger/api/v1/report/sale`;
    case 'createNewEntry':
      return `${baseUrl}/ledger/api/v1/`;
    case 'customerKhataList':
      return `${baseUrl}/ledger/api/v1/sale`;
    case 'customerKhataDetail':
      return `${baseUrl}/ledger/api/ledger/v1/customerdetail`;
    default:
      return null;
  }
}

export function hardwareBackPressed(routerHistory) {
  if (window.location.hash === '#noNetwork') {
    return closeWebView();
  }
  // window.location.pathname = /staging-merchant.paytm.com/v1/index.html/review" --- for staging
  // window.location.pathname = /v1/index.html/review" --- Production
  let pathArr = window.location.pathname.split('/');
  let customPathname = '/' + pathArr[pathArr.length - 1];
  //back press on edit offer should goto view offer
  switch (customPathname) {
    case '/landing':
      return closeWebView();
    case '/profile':
      return routerHistory.push('/');
    case '/storeTiming':
      return routerHistory.push('/profile');
    case '/timeline':
      return closeWebView();
    case '/vouchers':
      return closeWebView();
    default:
      return window.history.back();
  }
}

export function trimCharacter(str, character) {
  let ans = '';
  if (str) {
    for (let i = 0; i < str.length; i++) {
      if (str[i] !== character) ans = ans + str[i];
    }
  }
  return ans;
}

export function getNativeData() {
  const methodBody = {
    functionName: NATIVEFUNCTION_TYPE.getNativeData
  };
  try {
    callNativeMethod(methodBody);
  } catch (error) {
    console.log('Using WEB as interface');
  }
}

export function getContacts() {
  const methodBody = {
    functionName: NATIVEFUNCTION_TYPE.getContacts
  };
  try {
    callNativeMethod(methodBody);
  } catch (error) {
    let contacts = getContactsWeb();
    return contacts;
    console.log('Using Web as Interface');
  }
}
const getContactsWeb = () => {
  return {
    contacts: [
      {
        name: 'Rohan',
        phoneNumbers: ['981272121', '891211232']
      },
      {
        name: 'John Wayne',
        phoneNumbers: ['981272120']
      },
      {
        name: 'Price',
        phoneNumbers: ['891211242']
      },
      {
        name: 'Shivam',
        phoneNumbers: ['181272121']
      },
      {
        name: 'Shaun',
        phoneNumbers: ['961272121']
      },
      {
        name: 'Jack',
        phoneNumbers: ['801211232']
      },
      {
        name: 'Yuri',
        phoneNumbers: ['941272121']
      },
      {
        name: 'Soap',
        phoneNumbers: ['801211232']
      },
      {
        name: 'Pagan',
        phoneNumbers: ['981272121']
      }
    ]
  };
};
const nativeMethodCalling = getCommonMethod(); //common variable which will be use for native method calling
function getCommonMethod() {
  if (window.webkit && window.webkit.messageHandlers) {
    //It will come in iOS
    return window.webkit.messageHandlers.CommunicationWindow;
  } else if (window.CommunicationWindow) {
    //It will come in Android
    return window.CommunicationWindow;
  } else {
    return new Error('asdgkjbsdkjgb');
  }
}

function callNativeMethod(data) {
  nativeMethodCalling.postMessage(JSON.stringify(data));
}

function hideLoadingScreen() {
  try {
  } catch (err) {}
}

// Generic utility that will  be called from ios and android to handle different cases
window.onMessageReceivePaytmKhata = (messageName, data) => {
  switch (messageName) {
    case JSFUNCTION_TYPE.nativeData:
      authData = data ? JSON.parse(data) : {};
      setNativeValues(authData);
      break;
    case JSFUNCTION_TYPE.allGTMUrls:
      gtmData = data ? JSON.parse(data) : {};
      break;
    case JSFUNCTION_TYPE.getContacts:
      contacts = data ? JSON.parse(data) : {};

      break;
    default:
      break;
  }
};

export const merchantName = getAuthData().merchant_name;

export const pgMid = getAuthData().mid;

export const session_token = getAuthData().token;
export const osVersion = getAuthData().os_version
  ? getAuthData().os_version
  : '';
export const appVersion = getAuthData().app_version
  ? getAuthData().app_version
  : '';
export const client = getAuthData().client ? getAuthData().client : '';
export const deviceIdentifier = getAuthData().device_identifier
  ? getAuthData().device_identifier
  : '';

window.onMessageReceivePaytmKhata = (messageName, data) => {
  switch (messageName) {
    case JSFUNCTION_TYPE.nativeData:
      window.localStorage.setItem('authdata', JSON.stringify(data));
      authData = data ? JSON.parse(data) : {};
      setNativeValues(authData);
      break;
    case JSFUNCTION_TYPE.allGTMUrls:
      gtmData = data ? JSON.parse(data) : {};
      break;
    case JSFUNCTION_TYPE.nativeIPAddress:
      resolvePromise(NATIVEFUNCTION_TYPE.getIPAddress, data);
      break;
    default:
      break;
  }
};
export function getAppLanguage() {
  return getAuthData().language;
}
