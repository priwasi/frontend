import React from 'react';
import s from './Languagesplash.module.scss';
import paytmlogo from '../../public/png/logoBusiness.png';
import arrow from '../../public/png/icArrowBack.png';

function update() {
  var element = document.getElementById('progressid');
  var width = 1;
  var identity = setInterval(scene, 5);
  function scene() {
    if (width >= 100) {
      clearInterval(identity);
    } else {
      width++;
      element.style.width = width + '%';
    }
  }
}

export default class Languagesplash extends React.PureComponent {
  componentDidMount() {
    update();
  }
  render() {
    return (
      <div>
        <div className={s.comman}>
          <img className={s.new_image} src={arrow} alt="arrow back"></img>
          <img className={s.logo_image} src={paytmlogo} alt="logo"></img>
        </div>

        <div>
          <div className={s.space}></div>
          <div className={s.display}>
            <span className={s.para_header}>{this.props.title}</span>
          </div>
          <div className={s.display}>
            <div className={s.progress_bar}>
              <div id="progressid" className={s.bar}></div>
            </div>
          </div>
          <div className={s.display}>
            <p className={s.para}>
              Please wait. We are setting up {this.props.title} for you
            </p>
          </div>
        </div>
      </div>
    );
  }
}
